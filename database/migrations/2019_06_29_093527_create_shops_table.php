<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->unique();
            $table->string('shop_name');
            $table->string('slug')->unique();
            $table->string('phone_number');
            $table->string('trade_license')->unique();
            $table->time('open_at')->nullable();
            $table->time('close_at')->nullable();
            $table->integer('city_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->string('location');
            $table->double('lat')->nullable();
            $table->double('long')->nullable();
            $table->string('image')->default('default.png');
            $table->string('icon')->default('default.jpg');
            $table->boolean('is_approved')->default(false);
            $table->boolean('is_blocked')->default(false);
            $table->boolean('is_open')->default(true);
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
