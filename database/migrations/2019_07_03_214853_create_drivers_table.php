<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('social_security')->unique();
            $table->integer('city_id')->unsigned();
            $table->integer('area_id');
            $table->integer('vehicle_id')->unsigned();
            $table->string('vehicle_model')->nullable();
            $table->string('vehicle_number')->nullable();
            $table->boolean('is_your_vehicle')->default(false);
            $table->boolean('is_have_license')->default(false);
            $table->integer('working_type_id')->unsigned();
            $table->integer('workinghour_id')->unsigned();
            $table->integer('smartphone_id')->unsigned();
            $table->double('lat')->nullable();
            $table->double('long')->nullable();
            $table->string('gcm_token')->nullable();
            $table->boolean('is_approved')->default(false);
            $table->boolean('is_active')->default(true);
            $table->boolean('is_busy')->default(false);
            $table->boolean('is_blocked')->default(false);
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('city_id')
                ->references('id')->on('cities');
            $table->foreign('vehicle_id')
                ->references('id')->on('vehicles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
