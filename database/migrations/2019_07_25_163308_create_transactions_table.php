<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();//this is customer id
            $table->integer('shop_id')->unsigned();
            $table->integer('tax')->default(30);
            $table->text('item_list');
            $table->integer('total_quantity');
            $table->float('total_price');
            $table->float('delivery_charge');
            $table->integer('payment_id')->unsigned();//stripe or paypal
            $table->string('pay_id')->unique();//which is provide by bank authority after transaction
            $table->string('transaction_or_payer_id')->unique();//which is provide by bank authority after transaction
            $table->string('receipt_url')->nullable();//which is provide by bank authority after transaction
            $table->integer('shippingaddress_id');
            $table->integer('driver_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('shop_id')
                ->references('id')->on('shops')
                ->onDelete('cascade');
            $table->foreign('payment_id')
                ->references('id')->on('payment_roles');
            $table->foreign('driver_id')
                ->references('id')->on('drivers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
