<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();//this is customer id
            $table->integer('shop_id')->unsigned();
            $table->text('item_list');
            $table->integer('total_quantity');
            $table->float('total_price');
            $table->double('delivery_charge')->default(0);
            $table->integer('payment_id')->unsigned();//stripe or paypal
            $table->string('pay_id')->unique();//which is provide by bank authority after transaction
            $table->string('transaction_or_payer_id')->unique();//which is provide by bank authority after transaction
            $table->string('receipt_url')->nullable();//which is provide by bank authority after transaction
            $table->integer('shippingaddress_id');
            $table->integer('driver_id')->unsigned()->nullable();
            $table->boolean('is_approved')->default(0);
            $table->integer('is_cooked')->default(0);//0 for not cooked 1 for cooked and 2 for deliver to driver hand
            $table->boolean('is_delivered')->default(0);
            $table->boolean('is_reviewed')->default(0);
            $table->boolean('is_view_admin')->default(0);
            $table->boolean('is_view_shop')->default(0);
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('shop_id')
                ->references('id')->on('shops')
                ->onDelete('cascade');
            $table->foreign('payment_id')
                ->references('id')->on('payment_roles')
                ->onDelete('cascade');
            $table->foreign('driver_id')
                ->references('id')->on('drivers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
