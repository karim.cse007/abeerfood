<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SmartphonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('smartphones')->insert([
            'name' => 'Android',
        ]);
        DB::table('smartphones')->insert([
            'name' => 'iOS',
        ]);
    }
}
