<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactiontypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_types')->insert([
            'name' => 'Cashin',
        ]);
        DB::table('transaction_types')->insert([
            'name' => 'Cashout',
        ]);
    }
}
