<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkinghourTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workinghours')->insert([
            'name' => '1-10 hours',
        ]);
        DB::table('workinghours')->insert([
            'name' => '10-20 hours',
        ]);
        DB::table('workinghours')->insert([
            'name' => '20+ hours',
        ]);
    }
}
