<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicles')->insert([
            'name' => 'Car',
        ]);
        DB::table('vehicles')->insert([
            'name' => 'Scooter',
        ]);
        DB::table('vehicles')->insert([
            'name' => 'Bicycle',
        ]);
    }
}
