<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(PaymentrolesTableSeeder::class);
        $this->call(TransactiontypesTableSeeder::class);
        $this->call(VehiclesTableSeeder::class);
        $this->call(SmartphonesTableSeeder::class);
        $this->call(WorkinghourTableSeeder::class);
        $this->call(WorkingTypeTableSeeder::class);
    }
}
