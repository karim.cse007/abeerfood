<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'md admin',
            'username' => 'admin',
            'email' => 'admin@demo.com',
            'phone_number' => '123456',
            'password' => bcrypt('rootadmin'),
        ]);
        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'md Author',
            'username' => 'author',
            'email' => 'author@demo.com',
            'phone_number' => '1234567',
            'password' => bcrypt('rootauthor'),
        ]);
        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'md driver',
            'username' => 'driver',
            'email' => 'driver@demo.com',
            'phone_number' => '12345678',
            'password' => bcrypt('rootdriver'),
        ]);
        DB::table('users')->insert([
            'role_id' => '4',
            'name' => 'md Customer',
            'username' => 'customer',
            'email' => 'customer@demo.com',
            'phone_number' => '123456789',
            'password' => bcrypt('rootcustomer'),
        ]);
    }
}
