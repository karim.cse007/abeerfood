<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentrolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_roles')->insert([
            'name' => 'Paypal',
        ]);
        DB::table('payment_roles')->insert([
            'name' => 'Stripe',
        ]);
    }
}
