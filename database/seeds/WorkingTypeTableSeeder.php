<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('working_types')->insert([
            'name' => 'Weekdays',
        ]);
        DB::table('working_types')->insert([
            'name' => 'Weekends',
        ]);
        DB::table('working_types')->insert([
            'name' => 'Both',
        ]);
    }
}
