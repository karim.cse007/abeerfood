<div class="generator-bg rounded shadow-sm mb-4 p-4 osahan-cart-item">
    <div class="d-flex mb-4 osahan-cart-item-profile">
        <img class="img-fluid mr-3 rounded-pill" alt="osahan" src="{{ asset('public/storage/shops/'.$shop->image) }}">
        <div class="d-flex flex-column">
            <h6 class="mb-1 text-black">{{ $shop->shop_name }}
            </h6>
            <p class="mb-0 text-black"><i class="icofont-location-pin"></i> {{ $shop->location }}</p>
        </div>
    </div>
    <?php $i = 0; $flag =0 ;?>
    @foreach( $cartCollections as $cartCollection)
        <?php $flag =1 ; ?>
        <div class="bg-white rounded shadow-sm mb-2">
            <div class="gold-members p-2 border-bottom">
                <p class="text-gray mb-0 float-right ml-2">{{ $cartCollection->price * $cartCollection->quantity }} kr.</p>
                <span class="count-number float-right">
                   <button class="btn btn-outline-secondary  btn-sm left dec" onclick="document.getElementById('decrement-form-{{ $cartCollection->id }}').submit();"> <i class="icofont-minus"></i> </button>
                   <input class="count-number-input" type="text" value="{{ $cartCollection->quantity }}" readonly="">
                   <button class="btn btn-outline-secondary btn-sm right inc" onclick="document.getElementById('increment-form-{{ $cartCollection->id }}').submit();"> <i class="icofont-plus"></i> </button>
                    <form id="decrement-form-{{ $cartCollection->id }}" method="POST" action="{{ route('customer.cart.update') }}" style="display: none;">
                        @csrf
                        <input type="hidden" name="pid" value="1">
                        <input type="hidden" name="cartId" value="{{ $cartCollection->id }}">
                    </form>
                    <form id="increment-form-{{ $cartCollection->id }}" method="POST" action="{{ route('customer.cart.update') }}" style="display: none;">
                        @csrf
                        <input type="hidden" name="pid" value="2">
                        <input type="hidden" name="cartId" value="{{ $cartCollection->id }}">
                    </form>
               </span>
                <div class="media">
                    <div class="mr-2"><i class="icofont-ui-press text-danger food-item"></i></div>
                    <div class="media-body">
                        <p class="mt-1 mb-0 text-black">{{ $cartCollection->name }}</p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    @if ( $flag == 0)
        <p>Cart is empty buy now and enjoy eating!!!</p>
    @endif

    <div class="mb-2 bg-white rounded p-2 clearfix">
        <div class="input-group input-group-sm mb-2">
            <input type="text" class="form-control" placeholder="Enter promo code">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button" id="button-addon2"><i class="icofont-sale-discount"></i> APPLY</button>
            </div>
        </div>
    </div>
    <div class="mb-2 bg-white rounded p-2 clearfix">
        <p class="mb-1">Item Total <span class="float-right text-dark">{{ $total }} kr.</span></p>
        <p class="mb-1">Delivery Fee <span class="text-info" data-toggle="tooltip" data-placement="top" title="Total discount breakup">
           <i class="icofont-info-circle"></i>
           </span> <span class="float-right text-dark">{{ $charge }} kr.</span>
        </p>
        <p class="mb-1 text-success">Total Discount
            <span class="float-right text-success">0 kr.</span>
        </p>
        <hr />
        <h6 class="font-weight-bold mb-0">TO PAY  <span class="float-right">{{ $total + $charge }} kr.</span></h6>
    </div>
</div>
</div>
