@extends('layouts.frontend.app')

@section('title','Login')

@push('css')
    <link href="{{ asset('assets/frontend/css/style1.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">
    <style>
        .bg-image {
            background-image: url({{ asset('assets/frontend/images/homepage/home2.jpg') }});
        }
    </style>
@endpush

@section('content')

    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
            <div class="col-md-8 col-lg-6">
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-lg-8 mx-auto pl-5 pr-5">
                                <h3 style="text-align: center; border-bottom: 2px solid #000;padding: 3px; margin-top: -5px;" class="login-heading mb-4">Log In here</h3>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <fieldset class="form-group">
                                        <label>Email</label>
                                        <div class="input-group">
                                        <span class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="fa fa-envelope"></i>
                                          </span>
                                            </span>
                                            <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="example@email.com" autofocus>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label>Password</label>
                                        <div class="input-group">
                                        <span class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="fa fa-key"></i>
                                          </span>
                                            </span>
                                            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <label
                                            for="remember {{ old('remember') ? 'checked' : '' }}"></label>
                                        <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="remember">Remember password</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block btn-lg btn-gradient
                              text-uppercase font-weight-bold mb-2">
                                        {{ __('Login') }}
                                    </button>
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                    <div class="text-center pt-3">
                                        Don’t have an account? <a class="font-weight-bold" href="{{ route('register') }}">Sign Up</a>
                                    </div>
                                </form>
                                <hr class="my-4">
                                <p class="text-center">LOGIN WITH</p>
                                <div class="row">
                                    <div class="col pr-2">
                                        <a href="{{ route('login.google','google') }}" class="btn pl-1 pr-1 btn-lg btn-google font-weight-normal text-white btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Google</a>
                                    </div>
                                    <div class="col pl-2">
                                        <a href="{{ route('login.facebook','facebook') }}" class="btn pl-1 pr-1 btn-lg btn-facebook font-weight-normal text-white btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Facebook</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
