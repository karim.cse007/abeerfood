@extends('layouts.frontend.app')

@section('title','Checkout')

@push('css')
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">
    <style>
        .StripeElement {
            box-sizing: border-box;

            height: 40px;

            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
        div#card-element {
            width: 100%;
        }
    </style>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <style>
        .cross-area-icon {
            position: absolute;
            right: 12px;
            bottom: -28px;
            font-size: 14px;
            cursor: pointer;
        }
        #geomap{
            height: 230px;
        }
    </style>
@endpush

@section('content')
    <section class="offer-dedicated-body mt-4 mb-4 pt-2 pb-2">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    <div class="offer-dedicated-body-left">
                        <!--------address section start--------->

                        <div class="bg-white rounded shadow-sm p-4 mb-4">
                            <h4 class="mb-1">Choose a delivery address</h4>
                            <h6 class="mb-3 text-black-50"></h6>
                            <form method="POST" action="{{ route('shipping.address.store') }}">
                                @csrf
                                @method('POST')
                                @if(session()->get('shipping') == false)
                                    <div class="form-group">
                                        <div id="geomap" class="form-control"></div>
                                    </div>
                                @endif
                                <fieldset class="form-group">
                                    <label>GPS location</label>
                                    <div class="input-group">
                                    <span class="input-group-prepend">
                                      <span class="input-group-text">
                                        <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                      </span>
                                        </span>
                                        <input type="text" value="@if(session()->get('shipping') == true) {{ $shipaddress->location?$shipaddress->location:'' }} @endif" class="form-control @error('location') is-invalid @enderror" id="location" autocomplete="off" name="location" placeholder="gps location" required>
                                        <div class="icon-btn">
                                            <div class="cross-area-icon" id="my-location">
                                                <i class="fas fa-crosshairs"></i>
                                            </div>
                                        </div>
                                        @error('location')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <input type="hidden" name="lat" value="{{ old('lat') }}" id="lat">
                                        <input type="hidden" name="long" value="{{ old('long') }}" id="long">
                                    </div>
                                </fieldset>
                                <div class="form-row">
                                    <fieldset class="form-group col-md-6">
                                        <label>Name</label>
                                        <div class="input-group">
                                        <span class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="fa fa-user-circle"></i>
                                          </span>
                                            </span>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="name" value="{{ isset($shipaddress->name)?$shipaddress->name:'' }}" required>
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-md-6">
                                        <label>Phone Number</label>
                                        <div class="input-group">
                                        <span class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="fa fa-phone-square"></i>
                                          </span>
                                            </span>
                                            <input type="text" class="form-control @error('phone') is-invalid @enderror" value="{{ isset($shipaddress->phone_number)?$shipaddress->phone_number:'' }}" id="phone" name="phone" placeholder="1234" required>
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                </div>
                                <fieldset class="form-group">
                                    <label>Road Name/No</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="fa fa-road" aria-hidden="true"></i>
                                          </span>
                                            </span>
                                        <input type="text" class="form-control @error('road_name') is-invalid @enderror" value="{{ isset($shipaddress->road_name_or_number)?$shipaddress->road_name_or_number:'' }}" id="road_name" name="road_name" placeholder="road name" required>
                                        @error('road_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </fieldset>
                                <div class="form-row">
                                    <fieldset class="form-group col-md-4">
                                        <label>House No</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-home" aria-hidden="true"></i>
                                              </span>
                                            </span>
                                            <input type="text" class="form-control @error('house_no') is-invalid @enderror" value="{{ isset($shipaddress->house_no)?:old('house_no') }}" id="house_no" name="house_no" placeholder="House number" required>
                                            @error('house_no')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-md-4">
                                        <label>Door Code</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
                                              </span>
                                            </span>
                                            <input type="text" class="form-control @error('door_code') is-invalid @enderror" value="{{ isset($shipaddress->door_code)?$shipaddress->door_code:old('door_code') }}" id="door_code" name="door_code" placeholder="door code" required>
                                            @error('door_code')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-md-4">
                                        <label>Floor</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
                                              </span>
                                            </span>
                                            <input type="text" class="form-control @error('floor') is-invalid @enderror" value="{{ isset($shipaddress->floor)?$shipaddress->floor:old('floor') }}" id="floor" name="floor" placeholder="Floor" required>
                                            @error('floor')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="form-row">
                                    <fieldset class="form-group col-md-6">
                                        <label>Apartment/Suite</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-building" aria-hidden="true"></i>
                                              </span>
                                            </span>
                                            <input type="text" class="form-control @error('apartment') is-invalid @enderror" value="{{ isset($shipaddress->apartment_or_suite)?$shipaddress->apartment_or_suite:old('apartment_or_suite') }}" id="apartment" name="apartment" placeholder="Apartment/Suite" required>
                                            @error('apartment')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-md-6">
                                        <label>Door name</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
                                              </span>
                                            </span>
                                            <input type="text" class="form-control @error('door_name') is-invalid @enderror" value="{{ isset($shipaddress->door_name)?$shipaddress->door_name:old('door_name') }}" id="door_name" name="door_name" placeholder="Door name" required>
                                            @error('door_name')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="city">City</label>
                                        <select class="form-control @error('city') is-invalid @enderror"  name="city" id="city" >
                                            <option value="">Select</option>
                                            @foreach(App\City::all() as $city)
                                                <option value="{{ $city->id }}" {{ isset($shipaddress->city_id)?$shipaddress->city_id:old('city') == $city->id?'selected':'' }}>{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <fieldset class="form-group col-md-6">
                                        <label>Zip</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
                                              </span>
                                            </span>
                                            <input type="number" class="form-control @error('zip') is-invalid @enderror" value="{{ isset($shipaddress->zip)?$shipaddress->zip:old('zip') }}" id="zip" name="zip" placeholder="Zip code" required>
                                            @error('zip')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                </div>
                                @if(session()->get('shipping') == false)
                                    <input type="submit" id="submit" class="btn btn-primary" value="Save">
                                @else
                                    <button id="edit_shipping" class="btn btn-primary">Edit</button>
                                @endif
                            </form>
                        </div>
                        <!--------address section end--------->
                    </div>
                </div>
                <!---- cart section start----->
                <div class="col-md-4">
                    <div id="cart_section_cart">
                        <div class="generator-bg rounded shadow-sm mb-4 p-4 osahan-cart-item">
                            <div class="d-flex mb-4 osahan-cart-item-profile">
                                <img class="img-fluid mr-3 rounded-pill" alt="osahan" src="{{ asset('public/storage/shops/'.$shop->image) }}">
                                <div class="d-flex flex-column">
                                    <h6 class="mb-1 text-black">{{ $shop->shop_name }}
                                    </h6>
                                    <p class="mb-0 text-black"><i class="icofont-location-pin"></i> {{ $shop->location }}</p>
                                </div>
                            </div>
                            @if($cartCollections->count() >0 )
                                @foreach( $cartCollections as $cartCollection)
                                    <div class="bg-white rounded shadow-sm mb-2">
                                        <div class="gold-members p-2 border-bottom">
                                            <p class="text-gray mb-0 float-right ml-2">{{ $cartCollection->price * $cartCollection->quantity }} kr.</p>
                                            <span class="count-number float-right">
                                           <button class="btn btn-outline-secondary  btn-sm left dec" onclick="document.getElementById('decrement-form-{{ $cartCollection->id }}').submit();"> <i class="icofont-minus"></i> </button>
                                           <input class="count-number-input" type="text" value="{{ $cartCollection->quantity }}" readonly="">
                                           <button class="btn btn-outline-secondary btn-sm right inc" onclick="document.getElementById('increment-form-{{ $cartCollection->id }}').submit();"> <i class="icofont-plus"></i> </button>
                                            <form id="decrement-form-{{ $cartCollection->id }}" method="POST" action="{{ route('customer.cart.update') }}" style="display: none;">
                                                @csrf
                                                <input type="hidden" name="pid" value="1">
                                                <input type="hidden" name="cartId" value="{{ $cartCollection->id }}">
                                            </form>
                                            <form id="increment-form-{{ $cartCollection->id }}" method="POST" action="{{ route('customer.cart.update') }}" style="display: none;">
                                                @csrf
                                                <input type="hidden" name="pid" value="2">
                                                <input type="hidden" name="cartId" value="{{ $cartCollection->id }}">
                                            </form>
                                       </span>
                                            <div class="media">
                                                <div class="mr-2"><i class="icofont-ui-press text-danger food-item"></i></div>
                                                <div class="media-body">
                                                    <p class="mt-1 mb-0 text-black">{{ $cartCollection->name }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <p>Cart is empty buy now and enjoy eating!!!</p>
                            @endif

                            <div class="mb-2 bg-white rounded p-2 clearfix">
                                <div class="input-group input-group-sm mb-2">
                                    <input type="text" class="form-control" placeholder="Enter promo code">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" id="button-addon2"><i class="icofont-sale-discount"></i> APPLY</button>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2 bg-white rounded p-2 clearfix">
                                <p class="mb-1">Item Total <span class="float-right text-dark">{{ $total }} kr.</span></p>
                                <p class="mb-1">Delivery Fee <span class="text-info" data-toggle="tooltip" data-placement="top" title="Total discount breakup">
                           <i class="icofont-info-circle"></i>
                           </span> <span class="float-right text-dark">
                                        @if(session()->get('shipping') == false)
                                            30 kr.
                                        @else
                                            {{ Auth::user()->tempDistance->charge }} kr.
                                        @endif
                                    </span>
                                </p>
                                <p class="mb-1 text-success">Total Discount
                                    <span class="float-right text-success">0 kr.</span>
                                </p>
                                <hr />
                                <h6 class="font-weight-bold mb-0">TO PAY  <span class="float-right">
                                        @if(session()->get('shipping') == false)
                                            {{ $total + 30 }} kr.
                                        @else
                                            {{ $total + Auth::user()->tempDistance->charge }} kr.
                                        @endif
                                    </span>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="pt-2"></div>
                </div>
                <!---- cart section end----->

            @if(session()->get('shipping') == true)
                <!---- payment section start----->
                    <div class="col-md-12">
                        <div class="pt-2"></div>
                        <div class="bg-white rounded shadow-sm p-4 osahan-payment" id="payment-info">
                            <h4 class="mb-1">Choose payment method</h4>
                            <h6 class="mb-3 text-black-50">Credit/Debit Cards</h6>
                            <div class="row">
                                <div class="col-sm-4 pr-0">
                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="icofont-credit-card"></i> Credit/Debit Cards</a>
                                        <a class="nav-link" id="v-pills-paypal-tab" data-toggle="pill" href="#v-pills-paypal" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="icofont-bank-alt"></i> Paypal</a>
                                    </div>
                                </div>
                                <div class="col-sm-8 pl-0">
                                    <div class="tab-content h-100" id="v-pills-tabContent">
                                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                            <h6 class="mb-3 mt-0 mb-3"></h6>
                                            <p>WE ACCEPT <span class="osahan-card">
                                        <i class="icofont-visa-alt"></i> <i class="icofont-mastercard-alt"></i> <i class="icofont-american-express-alt"></i> <i class="icofont-payoneer-alt"></i> <i class="icofont-apple-pay-alt"></i> <i class="icofont-bank-transfer-alt"></i> <i class="icofont-discover-alt"></i> <i class="icofont-jcb-alt"></i>
                                        </span>
                                            </p>
                                            <form action="{{ route('order.store') }}" method="post" id="payment-form">
                                                @csrf
                                                @method('POST')
                                                <div class="form-row form-group col-md-12">
                                                    <label for="card-element">
                                                        Credit or debit card
                                                    </label>
                                                    <div id="card-element">
                                                        <!-- A Stripe Element will be inserted here. -->
                                                    </div>

                                                    <!-- Used to display form errors. -->
                                                    <div id="card-errors" role="alert"></div>
                                                </div>

                                                <div class="form-group col-md-12 mb-0">
                                                    <input type="submit" class="btn btn-success btn-block btn-lg" value="PAY {{ $total + Session::get('d_charge') }} kr." >
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-paypal" role="tabpanel" aria-labelledby="v-pills-paypal-tab">
                                            <h6 class="mb-3 mt-0 mb-3">Paypal Account</h6>
                                            <button onclick="event.preventDefault(); document.getElementById('payment-form-paypal').submit();">  <img src="{{ asset('assets/frontend/img/bank/paypal-logo.png') }}" width="150px" height="60px"></button>
                                            <form action="{{ route('paypal.checkout') }}" method="post" id="payment-form-paypal" hidden>
                                                @csrf
                                                @method('POST')

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!---- payment section end----->
                @endif

            </div>
        </div>
    </section>
@endsection


@push('js')
    @if(session()->get('shipping') == true)
        <script src="https://js.stripe.com/v3/"></script>
        <script src="{{ asset('assets/frontend/js/stripe.js') }}" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('#location').attr('disabled', true);
                $('#name').attr('disabled', true);
                $('#phone').attr('disabled', true);
                $('#road_name').attr('disabled', true);
                $('#house_no').attr('disabled', true);
                $('#door_code').attr('disabled', true);
                $('#floor').attr('disabled', true);
                $('#apartment').attr('disabled', true);
                $('#door_name').attr('disabled', true);
                $('#city').attr('disabled', true);
                $('#zip').attr('disabled', true);
                $('#edit_shipping').click(function(){
                    var _token =$('input[name="_token"]').val();
                    $.post("<?php echo url(route('shipping.address.edit')) ?>", {_token:_token}, function (data) {
                        location.reload();
                    });
                });
            });
        </script>
    @else
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDKBirQA6CA7Spq4IeqM9krNh2XaR7nQA&libraries=geometry&language=en"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script>
            var dst = 0;
            $(document).ready(function () {
                //load google map
                initialize();
                $('#my-location').on('click', function () {
                    //alert("working");
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('getLocation') }}",
                        method: "POST",
                        data: {_token: _token},
                        success: function (data) {
                            $('#location').val(data['loc']);
                            $('#lat').val(data['lat']);
                            $('#long').val(data['long']);
                            var latlng = new google.maps.LatLng(data['lat'], data['long']);
                            marker.setPosition(latlng);
                            initialize();
                            srcLocation = new google.maps.LatLng("{{$shop->lat}}","{{$shop->long}}");
                            dstLocation = new google.maps.LatLng(data['lat'], data['long']);
                            var distance = google.maps.geometry.spherical.computeDistanceBetween(srcLocation, dstLocation);
                            dst = distance/1000;
                            //alert(dst);
                            var _token =$('input[name="_token"]').val();
                            $.post("<?php echo url(route('customer.distance.calculate')) ?>", {_token:_token ,dist:dst}, function (data) {
                                //
                            });
                            $('#cart_section_cart').load('<?php echo url('customer/show/cart');?>').fadeIn('slow');

                        }
                    });
                });
                /*
                * autocomplete location search
                */
                var PostCodeid = '#location';
                $(function () {
                    $(PostCodeid).autocomplete({
                        source: function (request, response) {
                            geocoder.geocode({
                                'address': request.term
                            }, function (results, status) {
                                response($.map(results, function (item) {
                                    return {
                                        label: item.formatted_address,
                                        value: item.formatted_address,
                                        lat: item.geometry.location.lat(),
                                        lon: item.geometry.location.lng(),
                                    };
                                }));
                            });
                        },
                        select: function (event, ui) {
                            $('#location').val(ui.item.value);
                            $('#lat').val(ui.item.lat);
                            $('#long').val(ui.item.lon);
                            var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                            marker.setPosition(latlng);
                            initialize();
                            srcLocation = new google.maps.LatLng("{{$shop->lat}}","{{$shop->long}}");
                            dstLocation = new google.maps.LatLng(ui.item.lat,ui.item.lon);
                            var distance = google.maps.geometry.spherical.computeDistanceBetween(srcLocation, dstLocation);
                            dst = distance/1000;
                            //alert(dst);
                            var _token =$('input[name="_token"]').val();
                            $.post("<?php echo url(route('customer.distance.calculate')) ?>", {_token:_token ,dist:dst}, function (data) {
                                //
                            });
                            $('#cart_section_cart').load('<?php echo url('customer/show/cart');?>').fadeIn('slow');


                        }
                    });
                });

                /*
                 * Point location on google map
                 */
                $('.get_map').click(function (e) {
                    var address = $(PostCodeid).val();
                    geocoder.geocode({'address': address}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);
                            marker.setPosition(results[0].geometry.location);
                            $('#location').val(results[0].formatted_address);
                            $('#lat').val(marker.getPosition().lat());
                            $('#long').val(marker.getPosition().lng());
                        } else {
                            alert("Geocode was not successful for the following reason: " + status);
                        }
                    });
                    e.preventDefault();
                });

                //Add listener to marker for reverse geocoding
                google.maps.event.addListener(marker, 'drag', function () {
                    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#location').val(results[0].formatted_address);
                                $('#lat').val(marker.getPosition().lat());
                                $('#long').val(marker.getPosition().lng());
                            }
                        }
                    });
                });
            });
            function initialize() {
                var initialLat = $('#lat').val();
                var initialLong = $('#long').val();
                initialLat = initialLat?initialLat:59.394808;
                initialLong = initialLong?initialLong:17.909999;

                var latlng = new google.maps.LatLng(initialLat, initialLong);
                var options = {
                    zoom: 16,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById("geomap"), options);

                geocoder = new google.maps.Geocoder();

                marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    position: latlng
                });

                google.maps.event.addListener(marker, "dragend", function () {
                    var point = marker.getPosition();
                    map.panTo(point);
                    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);
                            marker.setPosition(results[0].geometry.location);
                            $('#location').val(results[0].formatted_address);
                            $('#lat').val(marker.getPosition().lat());
                            $('#long').val(marker.getPosition().lng());
                            srcLocation = new google.maps.LatLng("{{$shop->lat}}","{{$shop->long}}");
                            dstLocation = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
                            var distance = google.maps.geometry.spherical.computeDistanceBetween(srcLocation, dstLocation);
                            dst = distance/1000;
                            //alert(dst);
                            var _token =$('input[name="_token"]').val();
                            $.post("<?php echo url(route('customer.distance.calculate')) ?>", {_token:_token ,dist:dst}, function (data) {
                                //
                            });
                            $('#cart_section_cart').load('<?php echo url('customer/show/cart');?>').fadeIn('slow');
                        }
                    });
                });

            }
        </script>
    @endif
@endpush
