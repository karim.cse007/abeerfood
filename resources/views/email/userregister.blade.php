@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('assets/frontend/img/logo.png') }}" height="50px;" class="site-logo"/>
        @endcomponent
    @endslot
    {{-- Body --}}
    Welcome to AbeerFood!<br>
    Thank you for Registering to AbeerFood. We make your life easy by delivering food to your doorstep.
    When you place an order with us, you are helping hundreds of delivery riders and restaurant owners
    make a living. We are currently delivering food to 9 areas in Stockholm cityJARFALLA, SPANGA,
    SUNDBYBERG, BROMMA, KISTA, HASSELBY, VALLINGBY, BARKARBY AND JAKOBSBERG
    ORDER ONLINE NOW!


    If you have any questions, please email us at support@abeer.se

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            Thank you,<br>
            AbeerFood, Spånga<br>
            abeer.se<br>
            support@abeer.se<br>
            @endcomponent
            @endslot

            {{-- Footer --}}
            @slot('footer')
            @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}.
            @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent

