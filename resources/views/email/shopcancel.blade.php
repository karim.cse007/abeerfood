@component('mail::message')
# Fail

Sorry, we can't accept your shop.Your shop have not valid information.

@component('mail::button', ['url' => 'https://abeer.se/contact'])
contact
@endcomponent
Thank you,<br>
AbeerFood, Spånga<br>
abeer.se<br>
support@abeer.se<br>
{{ config('app.name') }}
@endcomponent
