@component('mail::message')
# Introduction

Thank you to for you registration....

@component('mail::button', ['url' => 'https://abeer.se'])
Button Text
@endcomponent

Thank you,<br>
AbeerFood, Spånga<br>
abeer.se<br>
support@abeer.se<br>
{{ config('app.name') }}
@endcomponent
