@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('assets/frontend/img/logo.png') }}" height="50px;" class="site-logo"/>
        @endcomponent
    @endslot
    {{-- Body --}}
    Welcome to AbeerFood,<br>
    Thank you for registering as a delivery rider at AbeerFood.
    Your application is under review. The status of your application will be informed through email.
    Upon approval, you can use your login details to sign in to your delivery rider account.


    If you have any questions, please email us at support@abeer.se

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            Thank you,<br>
            AbeerFood, Spånga<br>
            abeer.se<br>
            support@abeer.se<br>
            @endcomponent
            @endslot

            {{-- Footer --}}
            @slot('footer')
            @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}.
            @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
