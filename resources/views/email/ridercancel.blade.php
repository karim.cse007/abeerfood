@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('assets/frontend/img/logo.png') }}" height="50px;" class="site-logo"/>
        @endcomponent
    @endslot
    {{-- Body --}}
    Hello {{ isset($user) }},<br>
    Thank you for applying as a Delivery rider at AbeerFood. Unfortunately, your application was not
    approved this time. This might happen if your application details are invalid.


    If you have any questions, please email us at support@abeer.se

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            Thank you,<br>
            AbeerFood, Spånga<br>
            abeer.se<br>
            support@abeer.se<br>
            @endcomponent
            @endslot

            {{-- Footer --}}
            @slot('footer')
            @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}.
            @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
