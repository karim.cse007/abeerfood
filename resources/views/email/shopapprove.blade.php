@component('mail::message')
# Congrats

Your shop is approved. You can add new product and category and your shop is now online.

@component('mail::button', ['url' => 'https://abeer.se'])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
