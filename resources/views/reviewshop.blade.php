<h5 class="mb-1">All Reviews (<span>{{ $reviews->count() }}</span>)</h5>

@if ($reviews->count() >0)
    @foreach ($reviews as $review)
        <div class="reviews-members pt-4 pb-4">
            <div class="media">
                <a href="#"><img alt="{{ $review->user->name }}" src="{{ asset('public/storage/profile/'.$review->user->image) }}" class="mr-3 rounded-pill"></a>
                <div class="media-body">
                    <div class="reviews-members-header">
                        <h6 class="mb-1"><a class="text-black" href="#">{{ $review->user->name }}</a></h6>
                        <p class="text-gray">
                            @if ($review->created_at != NULL)
                                {{ $review->created_at->diffForHumans() }}
                            @endif
                        </p>
                    </div>
                    <div class="reviews-members-body">
                        <p>{{ $review->review }}</p>
                    </div>
                    <div class="reviews-members-footer">
                    </div>
                </div>
            </div>
        </div>
        <hr>
    @endforeach
@else
    <p>Have not any review yet may be you are the first </p>
@endif
