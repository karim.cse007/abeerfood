@extends('layouts.frontend.app')

@section('title','Home')

@push('css')
    <style type="text/css">
        .box {
            width: 600px;
            margin: 0 auto;
        }
        #locateme{
            cursor: pointer;
            color: #666;
        }
        #locateme:hover{
            cursor: pointer;
            color: #fff0ff;
        }
        #store-div{
            margin-left: 7%;
        }
        #store-div p{
            margin-left: 3%;
        }
        #locateme {
            position: absolute;
            right: 12px;
            bottom: 8px;
            padding-right: 12px;
            background: #ececec none repeat scroll 0 0;
        }
        #locateme:hover{
            cursor: pointer;
            color: #66C;
        }
        #locateme {
            cursor: pointer;
            color: #666;
        }
        #search_bar{
            margin-top: 12%;
        }
        @media (max-width: 991.98px) and (min-width: 768px)
        {
            #store-logo{
                max-width: 85px;
            }
            #store-div{
                margin-left: 7%;
            }
            #store-div p{
                font-size: 12px;
            }
            #search_bar{
                margin-top: 3%;
            }
        }
        @media (max-width: 767.98px) and (min-width: 576px)
        {
            #store-logo{
                max-width: 68px;
                margin-top: -4%;
            }
            #store-div{
                margin-left: 6%;
            }
            #store-div p{
                font-size: 11px;
                margin-left: 2%;
            }
            #search_bar{
                margin-top: 1%;
            }
        }
        @media (max-width: 575.98px)
        {
            #store-logo{
                max-width: 58px;
                margin-top: -8%;
            }
            #store-div{
                margin-left: 4%;
            }
            #store-div p{
                font-size: 10px;
                margin-left: 0%;
            }
            .h1, h1 {
                font-size: 1.5rem;
            }
            #search_bar{
                margin-top: -4%;
            }
        }
        @media (max-width: 470.98px)
        {
            #store-logo{
                max-width: 58px;
                margin-top: -8%;
            }
            #store-div{
                margin-left: -4%;
            }
            #store-div p{
                font-size: 10px;
                margin-left: 0%;
            }
        }
        @font-face{
            src: url({{asset('assets/frontend/front/calibri/Calibri_Regular.ttf')}});
            font-family: calibri;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/register/home.css')}}">
    <style>
        #statement{
            font-family: calibri;
        }
    </style>
@endpush

@section('content')
    <!--search section start-->
    <section class="position-relative card">
        <img src="{{asset('assets/frontend/images/homepage/home1.jpg')}}" class="card-img" alt="...">
        <div class="banner-overlay"></div>
        <div class="container">
            <div class="row card-img-overlay text-white">
                <div class="col-md-12">
                    <div class="pl-md-5 pt-md-5 pt-sm-0" id="search_bar">
                        <h1 class="mb-md-5 font-weight-normal text-white"><span class="font-weight-bold">BESTÄLL MAT </span>ONLINE
                        </h1>
                    </div>
                    <div id="space"></div>
                    <form id="search_form" action="{{ route('shop.search')}}" method="POST">
                        @csrf
                        <div class="form-row pl-md-5">
                            <div class="form-group col-md-6">
                                <input type="text" placeholder="Ange adress" required  name="searchinput" id="searchinput" autocomplete="off" class="form-control form-control-lg">
                                <a class="cross-area-icon text-black" id="locateme">
                                    <i class="icofont-ui-pointer"></i> Hitta mig</a>
                                <input type="hidden" name="lat"  id="lat">
                                <input type="hidden" name="long" id="long">
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-12">
                                <input class="form-control btn btn-primary" value="SHOW RESTAURANTS" type="submit">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section><!--order-food-online-in-your-area start-->
    <!--search section end-->
    <!--order-food-online-in-your-area start-->
    <section class="order-food-online pb-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="new-heading">
                        <h1> Order food from Stockholm's best restaurants! </h1>
                    </div>
                </div>
            </div>
            <div class="row">

                @foreach($shops as $shop)
                    @if($shop->posts->count() >0)
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <div class="all-meal">
                                <div class="top">
                                    <a href="{{ route('shop.posts',$shop->slug) }}">
                                        <div class="bg-gradient"></div>
                                    </a>
                                    <div class="top-img">
                                        <img src="{{ asset('public/storage/shops/'.$shop->image) }}"
                                             alt="{{ $shop->name }}">
                                    </div>
                                    <div class="logo-img">
                                        <img src="{{ asset('public/storage/shops/icons/'.$shop->icon) }}"
                                             alt="{{ $shop->name }}">
                                    </div>
                                    <div class="top-text">
                                        <div class="heading"><h4><a
                                                    href="{{ route('shop.posts',$shop->slug) }}">{{ $shop->shop_name }}</a>
                                            </h4></div>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <div class="bottom-text">
                                        <div class="delivery"><i class="fas fa-shopping-cart"></i>Delivery Fee : 30 Kr.
                                        </div>
                                        <div class="star">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <span>{{ \App\Rating::RatingShop($shop->id)}}.0</span>
                                            <div class="comments"><a href="javascript:;"><i
                                                        class="fas fa-comment-alt"></i>{{ $shop->reviews->count() }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
    </section>
    <!--order-food-online-in-your-area end-->
    <!--download apps section-->
    <section class="section bg-white border-bottom">
        <div class="card bg-dark text-white">
            <img style="max-height: 550px;" src="{{asset('assets/frontend/images/homepage/homeplay.jpg')}}" class="card-img" alt="...">
            <div class="card-img-overlay">
                <div id="store-div">

                    <p class="card-text text-hide" style="margin-top: 16%;">
                    <p class="text-white">Download AbeerFood App</p>
                    <a href="javascript:void(0);">
                        <img class="img-fluid" id="store-logo" src="{{ asset('assets/frontend/img/apple.png') }}" alt="abeerfood">
                    </a>
                    <a href="{{asset('public/storage/apk/abeer-food.apk')}}" download>
                        <img class="img-fluid" id="store-logo" src="{{ asset('assets/frontend/img/google.png') }}" alt="abeerfood">
                    </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--download apps section end-->
    <!--partner section-->
    <section class="section pt-5 pb-5 bg-white becomemember-section border-bottom">
        <div class="container">
            <div class="section-header text-center white-text">
                <h2 class=" became_part">Make money with AbeerFood whenever you want</h2>
                <p></p>
                <span class="line"></span>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="Card__card___1cR46 Card__withLink___3e4kW">
                        <div class="Card__img___2UpF8"
                             style="background-image:url({{ asset('assets/frontend/images/homepage/courier.jpg') }})">
                        </div>
                        <div class="Card__pad___1jcnr">
                            <h3>Make money as AbeerFood rider</h3>
                            <a href="{{ route('driver.basic')}}" class="LinkButton__link___3Gky4 Card__link___3has6">Apply
                                now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="Card__card___1cR46 Card__withLink___3e4kW">
                        <div class="Card__img___2UpF8"
                             style="background-image:url({{ asset('assets/frontend/images/homepage/restaurant.jpg') }})">
                        </div>
                        <div class="Card__pad___1jcnr">
                            <h3>Reach more people as restaurant partner</h3>
                            <a href="{{ route('add.restaurant')}}"
                               class="LinkButton__link___3Gky4 Card__link___3has6">Apply now</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--partner section end-->
    <!--what is abeerfood-->
    <section class="pt-1 pt-sm-1 pb-1 pt-md-1 pb-md-1 pb-sm-1 order-food-online">
        <div class="TitleText__container___1711t">
            <div class="TitleText__titleText___lI6K7"><h3>What is Abeer Food ?</h3>
                <div><p id="statement">AbeerFood makes your life easy by delivering food to your doorstep. When you place an order with AbeerFood, you are helping hundreds of riders and restaurant owners make a living.<br>
                        *For the time being AbeerFood is serving only
                        @foreach($areas as $key=>$area)
                            <strong>{{$area->name}}</strong>
                            @if($areas->count() > $key+1)
                                ,
                            @endif
                        @endforeach
                        within Stockhom city.</p>
                </div>
            </div>
        </div>
    </section>
    <!--what is abeerfood-->

@endsection

@push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAoo8LOXsE5bJUC5UVVzLK7qN3rKv1C__o"></script>
    <script>

        var searchInput = 'searchinput';

        $(document).ready(function () {

            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ['geocode'],
            });

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var near_place = autocomplete.getPlace();
                document.getElementById('lat').value = near_place.geometry.location.lat();
                document.getElementById('long').value = near_place.geometry.location.lng();
            });
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ['geocode'],
                componentRestrictions: {
                    country: "SE"
                }
            });

            $('#locateme').on('click', function () {
                //alert("working");
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('getLocation') }}",
                    method: "POST",
                    data: {_token: _token},
                    success: function (data) {
                        $('#searchinput').val(data['loc']);
                        $('#lat').val(data['lat']);
                        $('#long').val(data['long']);
                    }
                });
            });


        });
    </script>
@endpush
