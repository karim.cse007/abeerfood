<section class="footer pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-12 col-sm-12">
                <h6 class="mb-3">Subscribe to our Newsletter</h6>
                <center><label id="subscribe_info"></label></center>
                <form class="newsletter-form mb-1" id="subscribe-form">
                    @csrf
                    @method('POST')
                    <div class="input-group">
                        <input type="email" placeholder="example@email.com" class="form-control" id="s_email" name="s_email" required>
                        <div class="input-group-append">
                            <input type="submit" class="btn btn-primary" value="Subscribe">
                        </div>
                    </div>
                </form>
                <p><a class="text-info" href="{{ route('register') }}">Register now</a> to get updates on <a href="#">Offers and Coupons</a></p>
            </div>
            <div class="col-md-1 col-sm-6 mobile-none">
            </div>
            <div class="col-md-2 col-6 col-sm-4">
                <h6 class="mb-3">About</h6>
                <ul>
                    <li><a href="#">About us</a></li>
                    <li><a href="{{ route('contact') }}">Contact us</a></li>
                </ul>
            </div>
            <div class="col-md-2 m-none col-6 col-sm-4">
                <h6 class="mb-3">For Foodies</h6>
                <ul>
                    <li><a href="#">Blog</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-4 col-sm-4">
                <h6 class="mb-3">Register</h6>
                <ul>
                    <li><a href="{{ route('add.driver') }}">For Riders</a></li>
                    <li><a href="{{ route('add.restaurant') }}">Add a Restaurant</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<hr>
<footer class="pt-4 pb-4 text-center">
    <div class="container">
        <p class="mt-0 mb-0">© Copyright 2019 Abeer Food. All Rights Reserved</p>
        <!------
        <small class="mt-0 mb-0"> Developed <i class="fas fa-heart heart-icon text-danger"></i> by
            <a class="text-danger" target="_blank" href="#">Md abdul karim</a>
        </small>
        ----->
    </div>
</footer>
