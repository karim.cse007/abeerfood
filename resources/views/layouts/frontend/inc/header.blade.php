<nav class="navbar navbar-expand-lg navbar-light bg-light osahan-nav shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}"><img alt="abeerfood" src="{{ asset('assets/frontend/img/logo.png') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{ Request::is('/') ? 'active': '' }}">
                    <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                @guest
                <li class="nav-item {{ Request::is('login') ? 'active': '' }}">
                    <a class="nav-link" href="{{ route('login') }}">Login <span class="sr-only"></span></a>
                </li>
                <li class="nav-item {{ Request::is('register') ? 'active': '' }}">
                    <a class="nav-link" href="{{ route('register') }}">Register <span class="sr-only"></span></a>
                </li>
                @else
                    @if(\Cart::isEmpty() == false)
                        <li class="nav-item {{ Request::is('Cart') ? 'active': '' }}">
                            <a class="nav-link" href="{{ route('customer.checkout') }}">Cart <span class="sr-only"></span></a>
                        </li>
                    @endif
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img alt="Generic placeholder image" src="{{ is_numeric(Auth::user()->password) ? Auth::user()->image : asset('public/storage/profile/'.Auth::user()->image) }}" class="nav-osahan-pic rounded-pill"> My Account
                            </a>
                        @if (Auth::user()->role_id == 1)
                            <div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
                                <a class="dropdown-item" href="{{ route('admin.dashboard') }}"><i class="icofont-food-cart"></i> Dashboard</a>
                        @elseif (Auth::user()->role_id == 2)
                            <div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
                                <a class="dropdown-item" href="{{ route('author.dashboard') }}"><i class="icofont-food-cart"></i> Dashboard</a>
                        @elseif (Auth::user()->role_id == 3)
                            <div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
                                <a class="dropdown-item" href="{{ route('driver.dashboard') }}"><i class="icofont-food-cart"></i> Profile</a>
                        @elseif (Auth::user()->role_id == 4)
                            <div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
                                <a class="dropdown-item" href="{{ route('customer.dashboard') }}"><i class="icofont-food-cart"></i> Profile</a>
                        @endif
                                <a class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="icofont-food-cart"></i> Logout</a>
                            </div>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                            @method('POST')
                        </form>

                @endguest

            </ul>
        </div>
    </div>
</nav>
