<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="Abeer food this is very popular food in sweden">
    <meta name="author" content="md abdul karim">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') -{{ config('app.name', 'AbeerFood') }}</title>

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <!-- Bootstrap core CSS-->
    <link href="{{ asset('assets/frontend/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="{{ asset('assets/frontend/vendor/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="{{ asset('assets/frontend/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <!-- Select2 CSS-->
    <link href="{{ asset('assets/frontend/vendor/select2/css/select2.min.css') }}" rel="stylesheet">
    <!-- Custom styles for this template-->

    <link href="{{ asset('assets/frontend/css/style1.css') }}" rel="stylesheet">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/owl-carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/owl-carousel/owl.theme.css') }}">

    @stack('css')

</head>
<body>

    @include('layouts.frontend.inc.header')

    @yield('content')


    @include('layouts.frontend.inc.footer')

    <!-- SCIPTS -->
    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/frontend/js/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('assets/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Select2 JavaScript-->
    <script src="{{ asset('assets/frontend/vendor/select2/js/select2.min.js') }}"></script>
    <!-- Owl Carousel -->
    <script src="{{ asset('assets/frontend/vendor/owl-carousel/owl.carousel.js') }}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('assets/frontend/js/custom.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#subscribe-form").submit(function () {

                if ($('#s_email').val() == '') {
                    var msg = "<span style='color:red;'>Error! <strong>Field Must not be empty!!!</strong></span>";
                    $("#subscribe_info").html(msg);
                } else {
                    $.post("<?php echo url(route('subscriber.store')) ?>", $("#subscribe-form").serialize(), function (data) {
                        $('#s_email').val(' ');
                        $("#subscribe_info").html(data);
                    });
                }
                return false;
            });
        });
    </script>
    <script>
         var time = new Date().getTime();
         $(document.body).bind("mousemove keypress", function(e) {
             time = new Date().getTime();
         });

         function refresh() {
             if(new Date().getTime() - time >= 60000) 
                 window.location.reload(true);
             else 
                 setTimeout(refresh, 10000);
         }

         setTimeout(refresh, 10000);
    </script>
    @stack('js')

</body>
</html>
