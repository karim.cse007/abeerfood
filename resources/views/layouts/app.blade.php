<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="AbeerFood makes your life easy by delivering food to your doorstep. When you place an order with AbeerFood, you are helping hundreds of riders and restaurant owners make a living.
*For the time being AbeerFood is serving only Järfälla , Spånga , Sundbyberg , Bromma , Kista , Hässelby , Vällingby , Barkarby , Jakobsberg within Stockhom city.">
    <meta name="author" content="md abdul karim">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') -{{ config('app.name', 'AbeerFood') }}</title>

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <!-- Bootstrap core CSS-->
    <link href="{{ asset('assets/frontend/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="{{ asset('assets/frontend/vendor/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="{{ asset('assets/frontend/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <!-- Select2 CSS-->
    <link href="{{ asset('assets/frontend/vendor/select2/css/select2.min.css') }}" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('assets/frontend/css/style1.css') }}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/owl-carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/owl-carousel/owl.theme.css') }}">

    @stack('css')

</head>
<body>

@include('layouts.frontend.inc.header')

@yield('content')


@include('layouts.frontend.inc.footer')

<!-- SCIPTS -->
<!-- jQuery -->
<script src="{{ asset('assets/frontend/vendor/jquery/jquery-3.3.1.slim.min.js') }}"></script>
<!-- Bootstrap core JavaScript-->
<script src="{{ asset('assets/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Select2 JavaScript-->
<script src="{{ asset('assets/frontend/vendor/select2/js/select2.min.js') }}"></script>
<!-- Owl Carousel -->
<script src="{{ asset('assets/frontend/vendor/owl-carousel/owl.carousel.js') }}"></script>
<!-- Custom scripts for all pages-->
<script src="{{ asset('assets/frontend/js/custom.js') }}"></script>
<script>
         var time = new Date().getTime();
         $(document.body).bind("mousemove keypress", function(e) {
             time = new Date().getTime();
         });

         function refresh() {
             if(new Date().getTime() - time >= 60000) 
                 window.location.reload(true);
             else 
                 setTimeout(refresh, 10000);
         }

         setTimeout(refresh, 10000);
    </script>
@stack('js')

</body>
</html>
