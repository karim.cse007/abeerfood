<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') -{{ config('app.name', 'abeerfoood') }}</title>

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/frontend/img/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/frontend/img/favicon.ico') }}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('assets/backend/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('assets/backend/plugins/node-waves/waves.css') }}" rel="stylesheet">
    <!-- Animation Css -->
    <link href="{{ asset('assets/backend/plugins/animate-css/animate.css') }}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{ asset('assets/backend/css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('assets/backend/css/themes/all-themes.css') }}" rel="stylesheet" >
    <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

    @stack('css')

</head>
<body class="theme-grey">

    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    @include('layouts.backend.inc.header')
    <section>
        @include('layouts.backend.inc.sidebar')
    </section>

    <section class="content">
        @yield('content')
    </section>

    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/backend/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('assets/backend/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Select Plugin Js -->
   {{---- <script src="{{ asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
----}}
    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/node-waves/waves.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/backend/js/admin.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('assets/backend/js/demo.js') }}"></script> <!-- Jquery Core Js -->
    @if(Auth::user()->role->id == 1)
        <script>
            function playSound(filename){
                var mp3Source = '<source src="' + filename + '.mp3" type="audio/mpeg">';
                var oggSource = '<source src="' + filename + '.ogg" type="audio/ogg">';
                var embedSource = '<embed hidden="true" autostart="true" loop="false" src="' + filename +'.mp3">';
                document.getElementById("sound_test").innerHTML='<audio autoplay="autoplay">' + mp3Source + oggSource + embedSource + '</audio>';
            }
            function notification_refresh() {

                $('#unique_notification').load('<?php echo url('admin/notify');?>').fadeIn('slow');
            }
            setInterval("notification_refresh();",1500);//for sound
            //for notification have been seen
            $(document).ready(function() {
                $('#unique_notification').click(function(){
                    var _token =$('input[name="_token"]').val();
                    $.post("<?php echo url(route('admin.notification.seen')) ?>", {_token:_token ,data:'karim'}, function (data) {
                        //
                    });
                });
            });
        </script>
    @elseif(Auth::user()->role->id == 2)
        <script>
            function playSound(filename){
                var mp3Source = '<source src="' + filename + '.mp3" type="audio/mpeg">';
                var oggSource = '<source src="' + filename + '.ogg" type="audio/ogg">';
                var embedSource = '<embed hidden="true" autostart="true" loop="false" src="' + filename +'.mp3">';
                document.getElementById("sound_test").innerHTML='<audio autoplay="autoplay">' + mp3Source + oggSource + embedSource + '</audio>';
            }
            function notification_refresh() {

                $('#unique_notification').load('<?php echo url('author/notify');?>').fadeIn('slow');
            }
            setInterval("notification_refresh();",1500);//for sound
            //for notification have been seen
            $(document).ready(function() {
                $('#unique_notification').click(function(){

                    var _token =$('input[name="_token"]').val();
                    $.post("<?php echo url(route('author.notification.seen')) ?>", {_token:_token ,data:'karim'}, function (data) {

                    });
                });
            });
        </script>
    @endif
    <script>
         var time = new Date().getTime();
         $(document.body).bind("mousemove keypress", function(e) {
             time = new Date().getTime();
         });

         function refresh() {
             if(new Date().getTime() - time >= 60000) 
                 window.location.reload(true);
             else 
                 setTimeout(refresh, 10000);
         }

         setTimeout(refresh, 10000);
    </script>
    @stack('js')
    <script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
    {!! \Brian2694\Toastr\Facades\Toastr::message() !!}
    <script>
        @if($errors->any())
            @foreach($errors->all() as $error)
                toastr.error('{{ $error }}','Error',{
                        closeButton:true,
                        progressBar:true,
                });
            @endforeach
        @endif
    </script>

</body>
</html>
