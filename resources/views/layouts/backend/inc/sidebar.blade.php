<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ asset('public/storage/profile/'.Auth::user()->image) }}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
            <div class="email">{{ Auth::user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">

                    <li>
                        <a href="#">
                            <i class="material-icons">settings</i>Settings</a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a class="dropdown-item" href="javascript:void(0);"
                           onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>Sign Out
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>

            @if(Request::is('admin*'))
                <li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}">
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/account') ? 'active' : '' }}">
                    <a href="#">
                        <i class="material-icons">account_balance</i>
                        <span>Account</span>
                    </a>

                </li>
                <li class="{{ Request::is('admin/cashout*') ? 'active' : '' }}">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">payment</i>
                        <span>Cashout Request
                        (
                            {{ App\CashoutRequestShop::all()->count() + App\CashoutRequestDriver::all()->count() }}
                        )
                        </span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('admin.cashout.request.shop') }}">Shop
                                (
                                {{ App\CashoutRequestShop::all()->count() }}
                                )
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.cashout.request.driver') }}">Rider
                                (
                                {{ App\CashoutRequestDriver::all()->count() }}
                                )
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::is('admin/location*') ? 'active' : '' }}">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">location_on</i>
                        <span>Location</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('admin.city') }}">City</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.areas') }}">State</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::is('admin/category*') ? 'active' : '' }}">
                    <a href="{{ route('admin.category.index') }}">
                        <i class="material-icons">label</i>
                        <span>Category</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/food-item') ? 'active' : '' }}">
                    <a href="{{ route('admin.post') }}">
                        <i class="material-icons">fastfood</i>
                        <span>Food Item</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/order*') ? 'active' : '' }}">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">fastfood</i>
                        <span>Orders
                        (
                            {{ App\Order::where('is_delivered',0)->count() + App\Order::where('is_delivered',1)->where('is_approved',1)
                                ->where('is_cooked','>',1)->count()
                            }}
                        )
                        </span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('admin.order.driver.manage') }}">Rider Manage
                                (
                                {{ App\Order::where('is_delivered',0)->count() }}
                                )
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.order.complete') }}">Complete Order
                                (
                                {{ App\Order::where('is_delivered',1)->where('is_approved',1)
                                ->where('is_cooked','>',1)->count() }}
                                )
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.page.transaction') }}">Transaction
                                (
                                {{ App\Transaction::all()->count() }}
                                )
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::is('admin/admin-manage') ? 'active' : '' }}">
                    <a href="{{ route('admin.admin.manage') }}">
                        <i class="material-icons">supervised_user_circle</i>
                        <span>Admin Manage</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/shops*') ? 'active' : '' }}">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">shop</i>
                        <span>Shop
                        (
                            {{ App\Shop::where('is_approved',0)->count() }}
                        )
                        </span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('admin.shop.manage') }}">Shops & Owner Manage</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.shops.tax') }}">Tax Rate</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.pending.shops') }}">Pending Shops
                                (
                                {{ App\Shop::where('is_approved',0)->count() }}
                                )
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="{{ Request::is('admin/driver-manage') ? 'active' : '' }}">
                    <a href="{{ route('admin.driver.manage') }}">
                        <i class="material-icons">directions_car</i>
                        <span>Riders Manage</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/pending/drivers') ? 'active' : '' }}">
                    <a href="{{ route('admin.pending.drivers') }}">
                        <i class="material-icons">person_add</i>
                        <span>Pending Riders
                        (
                            {{ App\Driver::where('is_approved',0)->count() }}
                        )
                        </span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/blocked/user') ? 'active' : '' }}">
                    <a href="{{ route('admin.user.blocked') }}">
                        <i class="material-icons">block</i>
                        <span>Blocked Users
                        (
                            {{ App\User::where('is_blocked',1)->count() }}
                        )
                        </span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/subscriber') ? 'active' : '' }}">
                    <a href="{{ route('admin.subscriber.index') }}">
                        <i class="material-icons">subscriptions</i>
                        <span>Subscribers
                        (
                            {{ App\Subscriber::all()->count() }}
                        )
                        </span>
                    </a>
                </li>
                <li class="header">System</li>
                <li class="{{ Request::is('admin/settings') ? 'active' : '' }}">
                    <a href="{{ route('admin.settings') }}">
                        <i class="material-icons">settings</i>
                        <span>Settings</span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="material-icons">input</i>
                        <span>Log out</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            @endif
            @if(Request::is('author*'))
                <li class="{{ Request::is('author/dashboard') ? 'active' : '' }}">
                    <a href="{{ route('author.dashboard') }}">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{ Request::is('author/account') ? 'active' : '' }}">
                    <a href="{{ route('author.account.info') }}">
                        <i class="material-icons">account_balance</i>
                        <span>Account Info</span>
                    </a>
                </li>
                <li class="{{ Request::is('author/shop') ? 'active' : '' }}">
                    <a href="{{ route('author.shopinfo') }}">
                        <i class="material-icons">shop</i>
                        <span>Shop Info</span>
                    </a>
                </li>
                <li class="{{ Request::is('author/category*') ? 'active' : '' }}">
                    <a href="{{ route('author.category.index') }}">
                        <i class="material-icons">label</i>
                        <span>Category</span>
                    </a>
                </li>
                <li class="{{ Request::is('author/post*') ? 'active' : '' }}">
                    <a href="{{ route('author.post.index') }}">
                        <i class="material-icons">library_books</i>
                        <span>Food Manage</span>
                    </a>
                </li>
                <li class="{{ Request::is('author/order-manage') ? 'active' : '' }}">
                    <a href="{{ route('author.order.manage') }}">
                        <i class="material-icons">swap_horizontal_circle</i>
                        <span>Order Manage(
                            {{ Auth::user()->shop->orders
                            ->where('is_approved',1)
                            ->where('is_cooked','<',2)
                            ->count() }}
                            )</span>
                    </a>
                </li>
                <li class="{{ Request::is('author/transactions') ? 'active' : '' }}">
                    <a href="{{ route('author.transaction.history') }}">
                        <i class="material-icons">card_travel</i>
                        <span>Completed Orders
                            (
                            {{ Auth::user()->shop->transactions->count() }}
                            )
                        </span>
                    </a>
                </li>
                <li class="{{ Request::is('author/reviews') ? 'active' : '' }}">
                    <a href="{{ route('author.review.index') }}">
                        <i class="material-icons">rate_review</i>
                        <span>Reviews</span>
                    </a>
                </li>


                <li class="header">System</li>
                <li class="{{ Request::is('author/settings') ? 'active' : '' }}">
                    <a href="{{ route('author.settings') }}">
                        <i class="material-icons">settings</i>
                        <span>Settings</span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="material-icons">input</i>
                        <span>Log out</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>

            @endif

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2016 - 2019 <a href="javascript:void(0);">Admin</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5
        </div>
    </div>
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->
