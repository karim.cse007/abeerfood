<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header" >
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('assets/frontend/img/logo.png') }}" style=" margin-top:-10%;margin-left: 5px;"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <div id="sound_test"></div>
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown" id="unique_notification">
                    @csrf
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">notifications</i>
                    </a>
                </li>
                <!-- #END# Notifications -->
                <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
