<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
    <i class="material-icons">notifications</i>
    <span class="label-count">{{$orders->where('is_view_admin',0)->count()}}</span>
    @if($orders->where('is_view_admin',0)->count() > Session::get('notify'))
        <script> playSound("{{asset('assets/backend/sound/notify')}}"); </script>
        {{ Session::put('notify',$orders->where('is_view_admin',0)->count())}}
    @endif
</a>
<ul class="dropdown-menu">
    <li class="header">NOTIFICATIONS</li>
    <li class="body">
        <ul class="menu">
            @foreach($orders->take(5) as $key=>$order)
                <li>
                    <a href="{{route('admin.order.driver.manage')}}">
                        <div class="icon-circle bg-light-green">
                            <i class="material-icons">fastfood</i>
                        </div>
                        <div class="menu-info">
                            <h4> Have a new order</h4>
                            <p>For <strong>{{ $order->shop->shop_name .' Total: '. $order->total_price }} kr.</strong></p>
                            <p>
                                <i class="material-icons">access_time</i> {{$order->created_at->diffForHumans()}}
                            </p>
                        </div>
                    </a>
                </li>
            @endforeach
            @if($orders->count() == 0)
                <li>
                    <a href="javascript:void(0);">
                        <div class="icon-circle bg-light-green">
                            <i class="material-icons">notification_important</i>
                        </div>
                        <div class="menu-info">
                            <h4> No notification found</h4>
                        </div>
                    </a>
                </li>
            @endif
        </ul>
    </li>
</ul>
