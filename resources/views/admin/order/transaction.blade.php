@extends('layouts.backend.app')

@section('title','Order List')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            All Transactions
                            <span class="badge bg-blue">{{ $transactions->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Shop Name</th>
                                    <th>Tax Rate</th>
                                    <th>Customer Name</th>
                                    <th>Total Price</th>
                                    <th>Payment Type</th>
                                    <th>Payment Id</th>
                                    <th>Transaction/payer Id</th>
                                    <th>Receipt</th>
                                    <th>Rider name</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>SL</th>
                                    <th>Shop Name</th>
                                    <th>Tax Rate</th>
                                    <th>Customer Name</th>
                                    <th>Total Price</th>
                                    <th>Payment Type</th>
                                    <th>Payment Id</th>
                                    <th>Transaction/payer Id</th>
                                    <th>Receipt</th>
                                    <th>Rider name</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                @foreach($transactions as $key=>$transaction)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $transaction->shop->shop_name }}</td>
                                        <td>{{ $transaction->tax }}%</td>
                                        <td>{{ $transaction->user->name }}</td>
                                        <td>{{ $transaction->total_price }} kr.</td>
                                        <td>{{ $transaction->payment->name }}</td>
                                        <td>{{ $transaction->pay_id }}</td>
                                        <td>{{ $transaction->transaction_or_payer_id }}</td>
                                        <td><a href="{{ $transaction->receipt_url }}" target="_blank">Show</a></td>
                                        <td>
                                            {{ $transaction->driver->user->name }}
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
@endpush
