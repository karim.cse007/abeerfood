@extends('layouts.backend.app')

@section('title','Order List')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Completed orders
                            <span class="badge bg-blue">{{ $orders->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Shop Name</th>
                                    <th>Tax</th>
                                    <th>Customer Name</th>
                                    <th>Total Price</th>
                                    <th>Rider name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>SL</th>
                                    <th>Shop Name</th>
                                    <th>Tax</th>
                                    <th>Customer Name</th>
                                    <th>Total Price</th>
                                    <th>Rider name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                @foreach($orders as $key=>$order)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $order->shop->shop_name }}</td>
                                        <td>{{ $order->shop->tax ? $order->shop->tax->percentage:'30' }}%</td>
                                        <td>{{ $order->user->name }}</td>
                                        <td>{{ $order->total_price }} kr.</td>
                                        <td>
                                            {{ $order->driver->user->name }}
                                        </td>
                                        <td>
                                            @if ($order->is_delivered == true)
                                                <p style="color: green">Deliver Complete</p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                                @if ($order->driver_id == null)
                                                    None
                                                @else
                                                    <button class="btn btn-danger waves-effect" type="button" onclick="complete({{ $order->id }})">
                                                        Complete
                                                    </button>
                                                @endif
                                            <form method="post" id="transaction-request-{{ $order->id }}" action="{{ route('admin.order.transaction') }}" hidden>
                                                @csrf
                                                @method('POST')
                                                <input type="hidden" value="{{ $order->id }}" name="order_id">
                                                <input type="hidden" value="{{ $order->shop_id }}" name="shop_id">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function complete(id) {
            swal({
                title: 'Are you sure?',
                text: "Order is complete!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Complete!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('transaction-request-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Dont complete',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
