@extends('layouts.backend.app')

@section('title','Order List')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Rider manage
                            <span class="badge bg-blue">{{ $orders->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Customer Name</th>
                                    <th>Shop Name</th>
                                    <th>Total Price</th>
                                    <th>Payment Type</th>
                                    <th>Payment Id</th>
                                    <th>Transaction/payer Id</th>
                                    <th>Receipt</th>
                                    <th>Status</th>
                                    <th>Rider name</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>SL</th>
                                    <th>Customer Name</th>
                                    <th>Shop Name</th>
                                    <th>Total Price</th>
                                    <th>Payment Type</th>
                                    <th>Payment Id</th>
                                    <th>Transaction/payer Id</th>
                                    <th>Receipt</th>
                                    <th>Status</th>
                                    <th>Rider name</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                @foreach($orders as $key=>$order)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $order->user->name }}</td>
                                        <td>{{ $order->shop->shop_name }}</td>
                                        <td>{{ $order->total_price }} kr.</td>
                                        <td>{{ $order->payment->name }}</td>
                                        <td>{{ $order->pay_id }}</td>
                                        <td>{{ $order->transaction_or_payer_id }}</td>
                                        <td><a href="{{ $order->receipt_url }}" target="_blank">Show</a></td>
                                        <td>
                                            @if ($order->is_approved == 0)
                                                <button class="btn btn-danger waves-effect" type="button" onclick="approve({{ $order->id }})">
                                                    Approve
                                                </button>
                                                <form id="approve-form-{{ $order->id }}" action="{{ route('admin.order.approve',$order->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                    @method('PUT')
                                                </form>
                                            @elseif ($order->is_cooked == 0)
                                                <p style="color: red">Cooking</p>
                                            @elseif ($order->is_cooked == 1)
                                                <p style="color: green">cooked</p>
                                            @elseif ($order->is_cooked == 2)
                                                <p style="color: green">Deliver</p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                                @if($order->request_driver)
                                                <p style="color: green">Request sent</p>
                                                @elseif ($order->driver_id == null)
                                                <button onclick="event.preventDefault();document.getElementById('driver-request-{{ $order->id }}').submit();" {{ $order->is_cooked <1 ? 'disabled':'' }} >Request Driver</button>
                                                @else
                                                    <a href="#">{{ $order->driver->user->name }}</a>
                                                @endif
                                            <form method="post" id="driver-request-{{ $order->id }}" action="{{ route('admin.request.driver') }}" hidden>
                                                @csrf
                                                @method('POST')
                                                <input type="hidden" value="{{ $order->id }}" name="order_id">
                                                <input type="hidden" value="{{ $order->shop->id }}" name="shop_id">

                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function approve(id) {
            swal({
                title: 'Are you sure?',
                text: "You want to approve this order!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Approve!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('approve-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Dont approve',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
