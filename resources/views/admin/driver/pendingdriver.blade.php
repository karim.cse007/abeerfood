@extends('layouts.backend.app')

@section('title','Pending riders')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            All Pending Riders
                            <span class="badge bg-blue">{{ $drives->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Social Security Number</th>
                                    <th>Vehicle Type</th>
                                    <th>Working</th>
                                    <th>Hours</th>
                                    <th>Operating System</th>
                                    <th>Working Area</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Social Security Number</th>
                                    <th>Vehicle Type</th>
                                    <th>Working</th>
                                    <th>Hours</th>
                                    <th>Operating System</th>
                                    <th>Working Area</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @if ($drives->count() > 0)

                                @foreach($drives as $key=>$drive)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $drive->user->name }}</td>
                                        <td>{{ $drive->user->email}}</td>
                                        <td>{{ $drive->user->phone_number }}</td>
                                        <td>{{ $drive->social_security }}</td>
                                        <td>{{ $drive->vehicle->name }}</td>
                                        <td>{{ $drive->workingType->name }}</td>
                                        <td>{{ $drive->workinghour->name }}</td>
                                        <td>{{ $drive->smartphone->name }}</td>
                                        <td>{{ $drive->area->name }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-success waves-effect" type="button" onclick="approveDriver({{ $drive->id }})">
                                                <i class="material-icons">check_circle</i>
                                            </button>
                                            <button class="btn btn-danger waves-effect" type="button" onclick="cancelDriver({{ $drive->id }})">
                                                <i class="material-icons">cancel</i>
                                            </button>
                                            <form id="approve-form-{{ $drive->id }}" action="{{ route('admin.approve.driver',$drive->id) }}" method="POST" style="display: none;">
                                                @csrf
                                                @method('PUT')
                                            </form>
                                            <form id="cancel-form-{{ $drive->id }}" action="{{ route('admin.cancel.driver',$drive->id) }}" method="POST" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="11"><center><p style="color: green;">Haven't any Pending Driver</p></center></td>
                                    </tr>

                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function approveDriver(id) {
            swal({
                title: 'Are you sure?',
                text: "You want to approve rider!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Approve it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('approve-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'dont approve :)',
                        'error'
                    )
                }
            })
        }
        function cancelDriver(id) {
            swal({
                title: 'Are you sure?',
                text: "You want to cancel!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Cancel it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('cancel-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Cant cancel now :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
