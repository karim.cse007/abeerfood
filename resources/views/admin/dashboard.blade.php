@extends('layouts.backend.app')

@section('title','Dashboard')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush
@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">shop</i>
                    </div>
                    <div class="content">
                        <div class="text">TOTAL SHOP</div>
                        <div class="number count-to" data-from="0" data-to="{{ \App\Shop::all()->count() }}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">account_balance</i>
                    </div>
                    <div class="content">
                        <div class="text">TOTAL SALE</div>
                        <div class="number count-to" data-from="0" data-to="{{ \App\Transaction::all()->sum('total_price') }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">monetization_on</i>
                    </div>
                    <div class="content">
                        <div class="text">THIS DAY SALE</div>
                        <div class="number count-to" data-from="0" data-to="{{ \App\Transaction::where('created_at','>=', Carbon\Carbon::today())->sum('total_price') }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">transform</i>
                    </div>
                    <div class="content">
                        <div class="text">Total Transactions</div>
                        <div class="number count-to" data-from="0" data-to="{{ \App\Transaction::all()->count() }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-blue-grey hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">redeem</i>
                    </div>
                    <div class="content">
                        <div class="text">Total Revenue</div>
                        <div class="number count-to" data-from="0" data-to="{{ \App\AdminAccount::all()->sum('amount') }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">redeem</i>
                    </div>
                    <div class="content">
                        <div class="text">This day Revenue</div>
                        <div class="number count-to" data-from="0" data-to="{{ $adminacc }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box breadcrumb-bg-light-blue hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">visibility</i>
                    </div>
                    <div class="content">
                        <div class="text">Total Visitor</div>
                        <div class="number count-to" data-from="0" data-to="{{ \App\Post::PostViewCount() }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-purple hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">supervised_user_circle</i>
                    </div>
                    <div class="content">
                        <div class="text">Total Users</div>
                        <div class="number count-to" data-from="0" data-to="{{ \App\User::all()->count() }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-deep-purple hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">playlist_add_check</i>
                </div>
                <div class="content">
                    <div class="text">CATEGORY</div>
                    <div class="number count-to" data-from="0" data-to="{{ \App\Category::all()->count() }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-brown hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">fastfood</i>
                </div>
                <div class="content">
                    <div class="text">TOTAL ITEMS</div>
                    <div class="number count-to" data-from="0" data-to="{{ \App\Post::all()->count() }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">subscriptions</i>
                </div>
                <div class="content">
                    <div class="text">SUBSCRIBER</div>
                    <div class="number count-to" data-from="0" data-to="{{ \App\Subscriber::all()->count() }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-blue-grey hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">forum</i>
                </div>
                <div class="content">
                    <div class="text">REVIEW</div>
                    <div class="number count-to" data-from="0" data-to="{{ \App\Review::all()->count() }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-purple hover-zoom-effect">
                <div class="icon">
                    <i class="material-icons">local_car_wash</i>
                </div>
                <div class="content">
                    <div class="text">TOTAL RIDERS</div>
                    <div class="number count-to" data-from="0" data-to="{{ \App\Driver::all()->count() }}" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>

@endsection

@push('js')
    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-countto/jquery.countTo.js') }}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/morrisjs/morris.js') }}"></script>
    <!-- ChartJs -->
    <script src="{{ asset('assets/backend/plugins/chartjs/Chart.bundle.js') }}"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/flot-charts/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/flot-charts/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/flot-charts/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/flot-charts/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/flot-charts/jquery.flot.time.js') }}"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/index.js') }}"></script>

@endpush
