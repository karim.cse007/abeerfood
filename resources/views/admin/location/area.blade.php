@extends('layouts.backend.app')

@section('title','Category')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <style>

        i#close {
            position:absolute;
            right:-0px;
            top:0px;
            cursor:pointer;
            background: #1f6fb2;
        }
        div#popupContact {
            position:absolute;
            left:60%;
            top:17%;
            margin-left:-202px;
            font-family:'Raleway',sans-serif
        }
        form {
            max-width:300px;
            min-width:250px;
            padding:10px 50px;
            border:2px solid gray;
            border-radius:10px;
            background-color:#fff
        }
        input[type=text] {
            width:82%;
            padding:10px;
            border:1px solid #ccc;
            padding-left:40px;
            font-size:16px;
        }
        #name {

            background-repeat:no-repeat;
            background-position:5px 7px
        }
        #submit {
            text-decoration:none;
            width:82%;
            padding:5px;
            margin-top: 2px;
            text-align:center;
            display:block;
            cursor:pointer;
            border-radius:5px
        }
    </style>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <a class="btn btn-primary waves-effect" onclick="div_show()">
                <i class="material-icons">add</i>
                <span>Add New State</span>
            </a>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ALL State
                            <span class="badge bg-blue">{{ $areas->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>City</th>
                                    <th>State Name</th>
                                    <th>Total User</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>City</th>
                                    <th>State Name</th>
                                    <th>Total User</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($areas as $key=>$area)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $area->city->name }}</td>
                                        <td>{{ $area->name }}</td>
                                        <td class="text-center">{{ $area->shops->count() + $area->drivers->count() }}</td>
                                        <td>{{ $area->updated_at }}</td>
                                        <td class="text-center">
                                            <button onclick="update_div_show('{{$area->id}}','{{ $area->name }}')" class="btn btn-info waves-effect">
                                                <i class="material-icons">edit</i>
                                            </button>
                                            <button onclick="deleteState({{ $area->id }})" class="btn btn-danger waves-effect">
                                                <i class="material-icons">delete</i>
                                            </button>
                                            <form id="delete-form-{{ $area->id }}" action="{{ route('admin.area.destroy',$area->id) }}" method="POST" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
    <!-- Popup Div Starts Here -->
    <div id="popupContact">
        <!-- Contact Us Form -->
        <form id="form_area" hidden="hidden">
            @csrf
            @method('POST')
            <i id="close" class="material-icons red" onclick="div_hide()">close</i>
            <h2>Add area</h2>
            <hr>
            <p id="error_area"></p>
            <label for="city">City Name</label>
            <select class="form-group" name="city" id="city">
                <option value="0">select</option>
                @foreach($cities as $city)
                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                @endforeach
            </select>
            <label for="name">State Name</label>
            <input id="name" class="form-group @error('name') is-invalid @enderror" name="name" placeholder="Name" type="text">
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input id="submit" class="form-group" name="submit" type="submit">

        </form>
    </div>
    <!-- Popup Div Ends Here -->

    <!-- Popup Div Starts Here -->
    <div id="popupContact">
        <!-- Contact Us Form -->
        <form id="update_area_form" hidden="hidden">
            @csrf
            @method('PUT')
            <i id="close" class="material-icons red" onclick="update_div_hide()">close</i>
            <h2>Update City</h2>
            <hr>
            <p id="update_error_area"></p>
            <label for="city">City Name</label>
            <select class="form-group" name="city" id="city" required>
                @foreach($cities as $city)
                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                @endforeach
            </select>
            <label for="update_name">State Name</label>
            <input id="update_name" value="" class="form-group @error('update_name') is-invalid @enderror" name="update_name" placeholder="Name" type="text">
            @error('update_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input type="hidden" value="" id="area_id" name="area_id">
            <input id="submit" class="form-group" name="submit" type="submit">

        </form>
    </div>
    <!-- Popup Div Ends Here -->
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script type="text/javascript">
        //Function To Display Popup
        function div_show() {
            document.getElementById('form_area').style.display = "block";
        }
        //Function to Hide Popup
        function div_hide(){
            document.getElementById('form_area').style.display = "none";
            location.reload(true);
        }
        $(document).ready(function() {

            $("#form_area").submit(function () {
                $("#error_area").html('');
                if ($('#name').val() == '' | $('#city').val() <=0) {
                    var msg = "<span style='color:red;'>Error! <strong>Field Must not be empty!!!</strong></span>";
                    $("#error_area").html(msg);
                } else {
                    //alert("working");
                    $.post("<?php echo url(route('admin.area.add')) ?>", $("#form_area").serialize(), function (data) {
                        //alert("working");
                        $('#name').val('');
                        $("#error_area").html(data);
                    });

                }
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        //Function To Display Popup
        function update_div_show(id, name) {
            //alert(id);
            document.getElementById('update_area_form').style.display = "block";
            $("#area_id").val(id);
            $("#update_name").val(name);
            $("#update_error_area").html('');
        }
        //Function to Hide Popup
        function update_div_hide(){
            document.getElementById('update_area_form').style.display = "none";
            location.reload(true);//for page refresh
        }
        $(document).ready(function() {

            $("#update_area_form").submit(function () {
                $("#update_error_area").html('');
                if ($('#update_name').val() == '') {
                    var msg = "<span style='color:red;'><strong>Field Must not be empty!!!</strong></span>";
                    $("#update_error_area").html(msg);
                } else {
                    //alert("working");
                    $.post("<?php echo url(route('admin.area.update')) ?>", $("#update_area_form").serialize(), function (data) {
                        $("#update_error_area").html(data);
                    });
                }
                return false;
            });
        });
    </script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function deleteState(id) {
            swal({
                title: 'Are you sure?',
                text: "You want to delete this State!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Cant delete :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
