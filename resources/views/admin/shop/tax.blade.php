@extends('layouts.backend.app')

@section('title','Shop and Owner Manage')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <style>

        i#close {
            position:absolute;
            right:-0px;
            top:0px;
            cursor:pointer;
            background: #1f6fb2;
        }
        div#popupContact {
            position:absolute;
            left:60%;
            top:17%;
            margin-left:-202px;
            font-family:'Raleway',sans-serif
        }
        form {
            max-width:300px;
            min-width:250px;
            padding:10px 50px;
            border:2px solid gray;
            border-radius:10px;
            background-color:#fff
        }
        input[type=number] {
            width:78%;
            padding:10px;
            border:1px solid #ccc;
            padding-left:40px;
            font-size:16px;
        }
        #name {

            background-repeat:no-repeat;
            background-position:5px 7px
        }
        #submit {
            text-decoration:none;
            width:82%;
            padding:5px;
            margin-top: 2px;
            text-align:center;
            display:block;
            cursor:pointer;
            border-radius:5px
        }
    </style>
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Tax Rate
                            <span class="badge bg-blue">{{ $shops->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Shop Name</th>
                                    <th>Email</th>
                                    <th>Tax Rate</th>
                                    <th>Shop Location</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Shop Name</th>
                                    <th>Email</th>
                                    <th>Tax Rate</th>
                                    <th>Shop Location</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @if ($shops->count() > 0)

                                    @foreach($shops as $key=>$shop)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $shop->user->name }}</td>
                                            <td>{{ $shop->shop_name}}</td>
                                            <td>{{ $shop->user->email }}</td>
                                            <td>{{ $shop->percentage ? $shop->percentage->percentage:'30' }}%</td>
                                            <td>{{ $shop->location }}</td>

                                            <td class="text-center">
                                                <button class="btn btn-danger waves-effect" type="button" onclick="div_show({{$shop->id}})">
                                                    Change
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7"><center><p style="color: green;">Have not any Pending Shop</p></center></td>
                                    </tr>

                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
    <!-- Popup Div Starts Here -->
    <div id="popupContact">
        <!-- Contact Us Form -->
        <form id="form_tax" hidden="hidden">
            @csrf
            @method('POST')
            <i id="close" class="material-icons red" onclick="div_hide()">close</i>
            <h2>Change Tax Rate</h2>
            <hr>
            <p id="error_area"></p>
            <label for="tax">Tax Rate</label>
            <input id="tax" value="30" class="form-group @error('tax') is-invalid @enderror" name="tax" type="number">%
            @error('tax')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input type="hidden" name="shopid" id="shopid">
            <input id="submit" class="form-group" name="submit" type="submit">

        </form>
    </div>
    <!-- Popup Div Ends Here -->
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script type="text/javascript">
        //Function To Display Popup
        function div_show(id) {
            document.getElementById('form_tax').style.display = "block";
            $("#error_area").html('');
            $("#shopid").val(id);
        }
        //Function to Hide Popup
        function div_hide(){
            document.getElementById('form_tax').style.display = "none";
            location.reload(true);//for page refresh
        }
        $(document).ready(function() {

            $("#form_tax").submit(function () {
                $("#error_area").html('');
                if ($('#tax').val() == '') {
                    var msg = "<span style='color:red;'><strong>Field Must not be empty!!!</strong></span>";
                    $("#error_area").html(msg);
                } else {
                    $.post("<?php echo url(route('admin.shops.tax.update')) ?>", $("#form_tax").serialize(), function (data) {
                        $("#error_area").html(data);
                    });

                }
                return false;
            });
        });
    </script>
@endpush
