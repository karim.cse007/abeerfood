@extends('layouts.backend.app')

@section('title','Shop and Owner Manage')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ALL Shop Who are request for cashout
                            <span class="badge bg-blue">{{ $shops->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Shop Name</th>
                                    <th>Owner Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Shop Address</th>
                                    <th>Available Balance</th>
                                    <th>Request Amount</th>
                                    <th>Request At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>SL</th>
                                    <th>Shop Name</th>
                                    <th>Owner Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Shop Address</th>
                                    <th>Available Balance</th>
                                    <th>Request Amount</th>
                                    <th>Request At</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @if ($shops->count() > 0)

                                    @foreach($shops as $key=>$shop)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $shop->shop->shop_name }}</td>
                                            <td>{{ $shop->shop->user->name}}</td>
                                            <td>{{ $shop->shop->user->email }}</td>
                                            <td>{{ $shop->shop->phone_number }}</td>
                                            <td>{{ $shop->shop->location }}</td>
                                            <td>{{ $shop->shop->shopowneraccount->sum('amount') }} kr.</td>
                                            <td>{{ $shop->amount }} kr.</td>
                                            <td>{{ $shop->created_at }}</td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-info waves-effect">
                                                    <i class="material-icons">check_circle</i>
                                                </a>
                                                <button class="btn btn-danger waves-effect" type="button" onclick="deleteCategory({{ $shop->id }})">
                                                    <i class="material-icons">cancel</i>
                                                </button>
                                                <form id="delete-form-{{ $shop->id }}" action="{{ route('admin.category.destroy',$shop->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10"><center><p style="color: green;">Haven't any Cash out request</p></center></td>
                                    </tr>

                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function unblockUser(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, unblock it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('unblock-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
        function blockUser(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, unblock it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('block-form-'+id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
