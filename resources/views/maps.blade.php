@extends('layouts.frontend.app')

@section('title','Home')

@push('css')
   <style>
        #geomap{
            width: 100%;
            height: 230px;
        }
    </style>
@endpush

@section('content')

    <div class="form-row">
        <!-- display google map -->
        <div id="geomap"></div>
        <fieldset class="form-group col-md-5">
            <label>GPS location</label>
            <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-location-arrow" aria-hidden="true"></i>
                          </span>
                            </span>
                <input type="text" class="form-control @error('location') is-invalid @enderror" id="location" value="{{ old('location') }}" autocomplete="off" name="location" placeholder="gps location" required>
                <div class="icon-btn">
                    <div class="cross-area-icon" id="my-location">
                        <i class="fas fa-crosshairs"></i>
                    </div>
                </div>
                @error('location')
                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                @enderror
                <input type="hidden" class="form-control @error('lat') is-invalid @enderror" name="lat" value="{{ old('lat') }}" id="lat">
                <input type="hidden" class="form-control @error('long') is-invalid @enderror" name="long" value="{{ old('long') }}" id="long">
            </div>
        </fieldset>
    </div>



@endsection


@push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDKBirQA6CA7Spq4IeqM9krNh2XaR7nQA"></script>
    <script>
        function initialize() {
            var initialLat = $('#lat').val();
            var initialLong = $('#long').val();
            initialLat = initialLat?initialLat:23.7574452;
            initialLong = initialLong?initialLong:90.3875378;

            var latlng = new google.maps.LatLng(initialLat, initialLong);
            var options = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("geomap"), options);

            geocoder = new google.maps.Geocoder();

            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: latlng
            });

            google.maps.event.addListener(marker, "dragend", function () {
                var point = marker.getPosition();
                map.panTo(point);
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $('#location').val(results[0].formatted_address);
                        $('#lat').val(marker.getPosition().lat());
                        $('#long').val(marker.getPosition().lng());
                    }
                });
            });

        }
    </script>
    <script>
        $(document).ready(function () {
            //load google map
            initialize();

            /*
             * autocomplete location search
             */
            var PostCodeid = '#location';
            $(function () {
                $(PostCodeid).autocomplete({
                    source: function (request, response) {
                        geocoder.geocode({
                            'address': request.term
                        }, function (results, status) {
                            response($.map(results, function (item) {
                                return {
                                    label: item.formatted_address,
                                    value: item.formatted_address,
                                    lat: item.geometry.location.lat(),
                                    lon: item.geometry.location.lng()
                                };
                            }));
                        });
                    },
                    select: function (event, ui) {
                        $('#location').val(ui.item.value);
                        $('#lat').val(ui.item.lat);
                        $('#long').val(ui.item.lon);
                        var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                        marker.setPosition(latlng);
                        initialize();
                    }
                });
            });

            /*
             * Point location on google map
             */
            $('.get_map').click(function (e) {
                var address = $(PostCodeid).val();
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $('#location').val(results[0].formatted_address);
                        $('#lat').val(marker.getPosition().lat());
                        $('#long').val(marker.getPosition().lng());
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
                e.preventDefault();
            });

            //Add listener to marker for reverse geocoding
            google.maps.event.addListener(marker, 'drag', function () {
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#location').val(results[0].formatted_address);
                            $('#lat').val(marker.getPosition().lat());
                            $('#long').val(marker.getPosition().lng());
                        }
                    }
                });
            });
        });
    </script>
@endpush
