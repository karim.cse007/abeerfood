@extends('layouts.frontend.app')

@section('title','Home')

@push('css')
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/error/notfound.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/register/home.css')}}">
@endpush

@section('content')
    <!--meals start-->
    <section class="all-partners">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 mobile-none">
                    <div class="filters partner-bottom">
                        <div class="filter-heading">
                            <h3>Filters</h3>
                        </div>
                        <div class="filters-body">
                            <div id="accordion">
                                <div class="filters-card border-bottom p-4">
                                    <div class="filters-card-header" id="headingOne">
                                        <h6 class="mb-0">
                                            <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Location <i class="icofont-arrow-down float-right"></i>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="filters-card-body card-shop-filters">
                                            <div id="collapseOne" class="collapse show" data-parent="#accordionone">
                                                <div class="search-area">
                                                    <form>
                                                        <input class="search-area-input" name="search" type="text" value="{{ Session::get('loc') }}" placeholder="Search your area">
                                                        <div class="icon-btn">
                                                            <div class="cross-area-icon">
                                                                <i class="fas fa-crosshairs"></i>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filters-card border-bottom p-4">
                                    <div class="filters-card-header" id="headingTwo">
                                        <h6 class="mb-0">
                                            <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                                                All cuisines
                                                <i class="icofont-arrow-down float-right"></i>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapsetwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="filters-card-body card-shop-filters">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb6">
                                                <label class="custom-control-label" for="cb6">American <small class="text-black-50">156</small></label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb7">
                                                <label class="custom-control-label" for="cb7">Pizza <small class="text-black-50">120</small></label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb8">
                                                <label class="custom-control-label" for="cb8">Healthy <small class="text-black-50">130</small></label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb9">
                                                <label class="custom-control-label" for="cb9">Vegetarian <small class="text-black-50">120</small></label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb10">
                                                <label class="custom-control-label" for="cb10"> Chinese <small class="text-black-50">111</small></label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb11">
                                                <label class="custom-control-label" for="cb11"> Hamburgers <small class="text-black-50">95</small></label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb12">
                                                <label class="custom-control-label" for="cb12"> Dessert <small class="text-black-50">50</small></label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb13">
                                                <label class="custom-control-label" for="cb13"> Chicken <small class="text-black-50">32</small></label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="cb14">
                                                <label class="custom-control-label" for="cb14"> Indian <small class="text-black-50">156</small></label>
                                            </div>
                                            <div class="mt-2"><a href="#" class="link">See all</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="col-lg-12 col-md-12 m-left m-right">
                        <div class="all-meals-show">
                            <div class="new-heading">
                                <h1> All Shops </h1>
                                <div class="loc-title">
                                    Shops In Your Area
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(isset($shops)){ ?>
                    <div class="row">

                            @foreach($shops as $shop)

                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="all-meal">
                                <div class="top">
                                    <a href="{{ route('shop.posts',$shop->slug) }}"><div class="bg-gradient"></div></a>
                                    <div class="top-img">
                                        <img src="{{ asset('public/storage/shops/'.$shop->image) }}" alt="{{ $shop->name }}">
                                    </div>
                                    <div class="logo-img">
                                        <img src="{{ asset('assets/frontend/images/homepage/meals/logo-2.jpg') }}" alt="{{ $shop->name }}">
                                    </div>
                                    <div class="top-text">
                                        <div class="heading"><h4><a href="{{ route('shop.posts',$shop->slug) }}">{{ $shop->shop_name }}</a></h4></div>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <div class="bottom-text">
                                        <div class="delivery"><i class="fas fa-shopping-cart"></i>Delivery Fee : Free</div>
                                        <div class="time"><i class="far fa-clock"></i>Delivery Time : 30 Min</div>
                                        <div class="star">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <span>4.5</span>
                                            <div class="comments"><a href="#"><i class="fas fa-comment-alt"></i>{{ $shop->reviews->count() }}</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <?php }else{ ?>
                        <div id="notfound">
                            <div class="notfound">
                                <div class="notfound-404">
                                    <h1>Oops!</h1>
                                </div>
                                <h2>Can't found any shop</h2>
                                <p>In your area. Sorry!!</p>
                                <a href="/">Go To Homepage</a>
                            </div>
                        </div>
                    <?php }?>
                </div>

            </div>
        </div>
    </section>
    <!--meals end-->


    <!--order-food-online-in-your-area end-->
    <section class="section pt-5 pb-5 bg-white becomemember-section border-bottom">
        <div class="container">
            <div class="section-header text-center white-text">
                <h2 class="LinkButton__link___3Gky4 became_part">Become a partner</h2>
                <p></p>
                <span class="line"></span>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="Card__card___1cR46 Card__withLink___3e4kW">
                        <div class="Card__img___2UpF8"
                             style="background-image:url({{ asset('assets/frontend/images/homepage/courier.jpg') }})">
                        </div>
                        <div class="Card__pad___1jcnr">
                            <h3>Make money as AbeerFood rider</h3>
                            <a href="{{ route('add.driver')}}" class="LinkButton__link___3Gky4 Card__link___3has6">Apply
                                now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="Card__card___1cR46 Card__withLink___3e4kW">
                        <div class="Card__img___2UpF8"
                             style="background-image:url({{ asset('assets/frontend/images/homepage/restaurant.jpg') }})">
                        </div>
                        <div class="Card__pad___1jcnr">
                            <h3>Reach more people as restaurant partner</h3>
                            <a href="{{ route('add.restaurant')}}"
                               class="LinkButton__link___3Gky4 Card__link___3has6">Apply now</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection


@push('js')

@endpush
