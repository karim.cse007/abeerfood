@extends('layouts.frontend.app')

@section('title','Profile')

@push('css')
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
@endpush

@section('content')
    <!--title-bar start-->
    <section class="title-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="left-title-text">
                        <h3>My Account</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--title-bar end-->
    <!--my-account start-->
    <section class="my-account">
        <div class="profile-bg">
            <div class="my-Profile-dt" style="margin-top: 112px;">
                <div class="container">
                    <div class="row">
                        <div class="container">
                            <div class="profile-dpt">
                                <img src="{{ is_numeric($user->password) ? $user->image : asset('public/storage/profile/'.$user->image) }}" style="max-width: 200px; max-height: 200px;" alt="abeer food">
                            </div>
                            <div class="profile-all-dt">
                                <div class="profile-name-dt">
                                    <h1>{{ $user->name }}</h1>
                                    <p><span><i class="fas fa-map-marker-alt"></i></span>{{ $user->addressl1 . $user->addressl2 .','. $user->city}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--my-account end-->
    <!--my-account-tabs start-->
    <section class="all-profile-details">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-12">
                    <div class="left-tab-links">
                        <div class="nav nav-pills nav-stacked nav-tabs ui vertical menu fluid">
                            <a href="#about" data-toggle="tab" class="item user-tab cursor-pointer">About</a>
                            <a href="#edit-profile" data-toggle="tab" class="item user-tab cursor-pointer">Edit</a>
                            <a href="#change-password" data-toggle="tab" class="item user-tab cursor-pointer">Change Password</a>
                            <a href="#my-orders" data-toggle="tab" class="item user-tab cursor-pointer">My Orders</a>
                            <a href="#order-history" data-toggle="tab" class="item user-tab cursor-pointer">Order History</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="about">
                            <div class="profile-about">
                                <div class="about-dtp">
                                    <div class="about-bg">
                                        <ul>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Ful Name</h6>
                                                    <p>{{ $user->name }}</p>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Mobile Number</h6>
                                                    <p>{{ $user->phone_number }}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Email Address</h6>
                                                    <p>{{ $user->email }}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Location</h6>
                                                    <p>{{ $user->addressl1 . $user->addressl2 .','. $user->city}}</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="edit-profile">
                            <div class="timeline">
                                <div class="edit-profile">
                                    <center><label id="showprofileinfo"></label></center>
                                    <form id="customer_update_form" enctype="multipart/form-data">
                                        @csrf
                                        @method('POST')

                                        <div class="setting-dt">
                                            <h4>Image</h4>
                                            <div class="avtar">
                                                <img src="{{ is_numeric($user->password) ? $user->image : asset('public/storage/profile/'.$user->image) }}" alt="abeer food">
                                            </div>
                                            <div class="upload-avatar">
                                                <div class="input-heading">Upload a Image</div>
                                                <div class="input-container">
                                                    <input type="file" name="image">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nameUser">Full Name*</label>
                                            <input type="text" class="video-form" id="name" name="name" value="{{ $user->name }}"  placeholder="Your name">
                                        </div>
                                        <div class="form-group">
                                            <label for="telPhone">Phone Number*</label>
                                            <input type="text" class="video-form" id="phone_number" name="phone_number" value="{{ $user->phone_number }}" placeholder="your phone number">
                                        </div>
                                        <div class="form-group">
                                            <label for="locationUser">Address line 1*</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" id="addressl1" name="addressl1" value="{{ $user->addressl1 }}" placeholder="your address">
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="locationUser">Address line 2</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" name="addressl2" id="addressl2" value="{{ $user->addressl2 }}" placeholder="your address">
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="locationUser">City*</label>
                                            <div class="field-input">
                                                <select name="city" id="city">
                                                    @foreach($cities as $city)
                                                        <option value="{{ $city->id }}" {{ $city->name == $user->city ? 'selected':'' }}>{{ $city->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="locationUser">Post Code</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" name="post_code" id="post_code" value="{{ $user->post_code }}" placeholder="your address">
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>

                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="change-password">
                            <div class="timeline">
                                <div class="edit-profile">
                                    <center><label id="showpassinfo"></label></center>
                                    <form id="customer_password_form">
                                        @csrf
                                        @method('POST')

                                        <div class="form-group">
                                            <label for="OldPassword">Old Password*</label>
                                            <input type="password" class="video-form" id="old_password" name="old_password" placeholder="Enter Old Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="newPassword">New Password*</label>
                                            <input type="password" class="video-form" id="password" name="password" placeholder="Enter New Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="confirmPassword">Confirm Password*</label>
                                            <input type="password" class="video-form" id="password_confirmation" name="password_confirmation" placeholder="Enter Confirm Password">
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="my-orders">
                            <div class="timeline">
                                <div class="tab-content-heading">
                                    <h4>My Orders</h4>
                                </div>
                                <div class="my-orders">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="my-checkout">
                                                <div class="table-responsive">
                                                    <table class="table  table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <td class="td-heading">Restaurants</td>
                                                            <td class="td-heading">Items</td>
                                                            <td class="td-heading">Total Quantity</td>
                                                            <td class="td-heading">Total Price</td>
                                                            <td class="td-heading">Delivery Address</td>
                                                            <td class="td-heading">Delivery Person</td>
                                                            <td>Status</td>
                                                            <td>Action</td>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($user->orders->where('is_reviewed',0) as $key=>$order)
                                                        <tr>
                                                            <td class="td-content">
                                                                <div class="name-dt">
                                                                    {{ $order->shop->shop_name }}
                                                                </div>
                                                            </td>
                                                            <td class="td-content">{{ $order->item_list }}</td>
                                                            <td class="td-content">{{ $order->total_quantity }}</td>
                                                            <td class="td-content">{{ $order->total_price }} kr.</td>
                                                            <td class="td-content">
                                                                {{ $order->shippingaddress->road_name_or_number }}
                                                                {{ $order->shippingaddress->apartment_or_suite }}
                                                                {{ $order->shippingaddress->city->name }}
                                                            </td>
                                                            <td class="td-content">
                                                                @if ($order->is_cooked >= 2 and $order->driver_id != null)
                                                                    <a href="#" onclick="driverifo({{ $order->driver->id }}); return false;">{{ $order->driver->user->name }} </a>
                                                                    <input type="hidden" id="driverName{{ $order->driver->id }}" value="{{ $order->driver->user->name }}">
                                                                    <input type="hidden" id="driverPhone{{ $order->driver->id }}" value="{{ $order->driver->user->phone_number }}">
                                                                    <input type="hidden" id="driverVehicle{{ $order->driver->id }}" value="{{ $order->driver->vehicle->name }}">
                                                                    <input type="hidden" id="driverVehicleNumber{{ $order->driver->id }}" value="{{ $order->driver->vehicle_number }}">
                                                                    <input type="hidden" id="driverImg{{ $order->driver->id }}" value="{{ asset('public/storage/icon/abeerfood.ico') }}">
                                                                @else
                                                                    None
                                                                @endif

                                                            </td>
                                                            <td class="td-content">
                                                                <p style="color: green">
                                                                    @if ($order->is_approved == 0)Pending
                                                                    @elseif ($order->is_cooked == 0) Packing
                                                                    @elseif($order->is_cooked == 1) waiting for Rider
                                                                    @elseif($order->is_cooked == 2) Delivering
                                                                    @endif
                                                                </p>
                                                            </td>
                                                            <td class="td-content">
                                                                @if ($order->status == 3)
                                                                    <a href="#" onclick="review({{ $order->id }}); return false;">Feed Back</a>
                                                                    <form id="review-form-{{ $order->id }}" style="display: none;">
                                                                        @csrf
                                                                        @method('POST')
                                                                        <input type="hidden" id="review{{ $order->id }}">
                                                                        <input type="hidden" id="driverId{{ $order->id }}" value="{{ $order->driver->id }}">
                                                                        <input type="hidden" id="orderId{{ $order->id }}" value="{{ $order->id }}">
                                                                    </form>
                                                                @else
                                                                    None
                                                                @endif

                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="order-history">
                            <div class="timeline">
                                <div class="tab-content-heading">
                                    <h4>Order History</h4>
                                </div>
                                <div class="my-orders">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="my-checkout">
                                                <div class="table-responsive">
                                                    <table class="table  table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <td class="td-heading">Restaurants</td>
                                                            <td class="td-heading">Items</td>
                                                            <td class="td-heading">Total Quantity</td>
                                                            <td class="td-heading">Total Price</td>
                                                            <td class="td-heading">Delivery Address</td>
                                                            <td class="td-heading">Status</td>
                                                            <td class="td-heading">Orders</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($user->transaction->where('is_delivered',1) as $key=>$order)
                                                            <tr>
                                                                <td class="td-content">
                                                                    {{ $order->shop->shop_name }}
                                                                </td>
                                                                <td class="td-content">{{ $order->item_list }}</td>
                                                                <td class="td-content">{{ $order->total_quantity }}</td>
                                                                <td class="td-content">{{ $order->total_price }} kr.</td>
                                                                <td class="td-content">
                                                                    {{ $order->shippingaddress->road_name_or_number }}
                                                                    {{ $order->shippingaddress->apartment_or_suite }}
                                                                    {{ $order->shippingaddress->city->name }}
                                                                </td>
                                                                <td class="td-content">
                                                                    <p style="color: green">
                                                                         Shipped
                                                                    </p>
                                                                </td>
                                                                <td class="td-content">{{  $order->created_at->diffForHumans() }}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--my-account-tabs end-->

@endsection


@push('js')
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script>

        $(document).ready(function(){

            $("#customer_update_form").submit(function(){
                if( $('#name').val() == '' || $('#phone_number').val() == ''|| $('#addressl1').val() == '' || $('#city').val() == ''|| $('#post_code').val() == ''){
                    var msg ="<span style='color:red;'>Error! <strong>Field Must not be empty!!!</strong></span>";
                    $("#showprofileinfo").html(msg);
                }else{
                    $.post("<?php echo url(route('customer.profile.update')) ?>", $("#customer_update_form").serialize(), function(data){
                        //alert("working");
                        $("#showprofileinfo").html(data);
                    });

                }
                return false;
            });

            $("#customer_password_form").submit(function(){
                if( $('#old_password').val() == '' || $('#password').val() == ''|| $('#password_confirmation').val() == ''){
                    var msg ="<span style='color:red;'>Error! <strong>Field Must not be empty!!!</strong></span>";
                    $("#showpassinfo").html(msg);
                    //return false;
                }else if( $('#password').val() != $('#password_confirmation').val()){
                    $('#old_password').val('');
                    $('#password').val('');
                    $('#password_confirmation').val('');
                    var msg ="<span style='color:red;'>Error! <strong>Confirm Password not match!!!</strong></span>";
                    $("#showpassinfo").html(msg);
                    //return false;
                }else if( $('#password').val().length < 6){
                    $('#old_password').val('');
                    $('#password').val('');
                    $('#password_confirmation').val('');
                    var msg ="<span style='color:red;'>Error! <strong>Password length must be grather than Six!!!</strong></span>";
                    $("#showpassinfo").html(msg);
                    //return false;
                }else{
                    $.post("<?php echo url(route('customer.password.update')) ?>", $("#customer_password_form").serialize(), function(data){
                        //alert("working");
                        $("#showpassinfo").html(data);
                        $('#old_password').val('');
                        $('#password').val('');
                        $('#password_confirmation').val('');
                    });

                }
                return false;

            });

        });
        function review(id) {
            swal({
                title: "Authenicating for continuation",
                input: "text",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Write something",

            }, function() {
                if ($("input").val() === false) return false;
                if ($("input").val() === "") {
                    swal.showInputError("You need to write something!");
                    return false
                }

                 swal("Nice!", "You wrote: " + inputValue, "success");
            });
            return false;
        }
        function driverifo(id) {
            swal({
                title: $("#driverName"+id).val(),
                text: "phn: "+$("#driverPhone"+id).val()+"\n" +"Vehicle Type: "
                    +$("#driverVehicle"+id).val()+"\n Vehicle Number: "
                    +$("#driverVehicleNumber"+id).val(),
                imageUrl: $("#driverImg"+id).val()
            });
            return false;
        }


    </script>

@endpush
