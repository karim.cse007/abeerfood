@extends('layouts.frontend.app')

@section('title','Partner')

@push('css')
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">
    <style>
        /* Hide the browser's default radio button */
        .radio-container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* On mouse-over, add a grey background color */
        .radio-container:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the radio button is checked, add a blue background */
        .radio-container input:checked ~ .checkmark {
            background-color: #2196F3;
        }


        /* Show the indicator (dot/circle) when checked */
        .radio-container input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the indicator (dot/circle) */
        .radio-container .checkmark:after {
            top: 9px;
            left: 9px;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background: white;
        }
    </style>
@endpush

@section('content')
    <!--add-restaurant start-->
    <section class="add-restaurant">
        <div class="container pull-l4 pl-lg-5 pl-sm-5">
            <div class="resto-heading align-center">
                <img src="{{ asset('assets/frontend/images/partner-with-us/icon-2.svg') }}" alt="abeer food">
                <h1>Rider Registration</h1>
            </div>
        <form action="{{ route('store.driver') }}" method="POST">
            @csrf
            @method('POST')
                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label>Name</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-user-circle"></i>
                          </span>
                            </span>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="name" value="{{ old('name') }}" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <small class="text-muted">ex. your name</small>
                    </fieldset>
                    <fieldset class="form-group col-md-5">
                        <label>Email</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-envelope"></i>
                          </span>
                            </span>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" id="email" placeholder="email" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <small class="text-muted">ex. example@email.com</small>
                    </fieldset>
                </div>
                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label>Phone Number</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-phone-square"></i>
                          </span>
                            </span>
                            <input type="text" class="form-control @error('phone_number') is-invalid @enderror" value="{{ old('phone_number') }}" required id="phone_number" name="phone_number" placeholder="1234">
                            @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <small class="text-muted">ex. your phone number</small>
                    </fieldset>
                    <fieldset class="form-group col-md-5">
                        <label>Social Security Number</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-male"></i>
                          </span>
                            </span>
                                <input type="text" class="form-control @error('ssecurity') is-invalid @enderror" value="{{ old('ssecurity') }}" name="ssecurity" id="ssecurity tin-input" placeholder="YYMMDD-XXXX" required>
                                @error('ssecurity')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <small class="text-muted">ex. YYMMDD-XXXX</small>
                    </fieldset>
                </div>
                <div class="form-row">
                    <fieldset class="form-group col-md-3">
                        <label>City</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                            </span>
                            <select class="form-group @error('city') is-invalid @enderror" name="city" required>
                                <option value="">Select</option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" {{ old('city')==$city->id?'selected':'' }}>{{ $city->name }}</option>
                                @endforeach
                            </select>
                            @error('city')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                    <div class="form-group col-md-3">
                        <label for="area">My area is</label>
                        <select class="video-form @error('area') is-invalid @enderror" name="area" required>
                            <option value="">Select</option>
                            @foreach($areas as $area)
                                <option value="{{ $area->id }}" {{ old('area')==$area->id?'selected':'' }}>{{ $area->name }}</option>
                            @endforeach
                        </select>
                        @error('area')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="vehicle">I own a</label>
                        <select class="video-form @error('vehicle') is-invalid @enderror" name="vehicle" required>
                            <option value="">Select</option>
                            @foreach($vehicles as $vehicle)
                                <option value="{{ $vehicle->id }}" {{ old('vehicle')==$vehicle->id?'selected':'' }}>{{ $vehicle->name }}</option>
                            @endforeach
                        </select>
                        @error('vehicle')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group  col-md-3">
                        <label for="w_during">I am willing to work during</label>
                        <select class="form-control @error('w_during') is-invalid @enderror" name="w_during" required>
                            <option value="">Select</option>
                            @foreach($working_types as $working_type)
                                <option value="{{ $working_type->id }}" {{ old('w_during')==$working_type->id?'selected':'' }}>{{ $working_type->name }}</option>
                            @endforeach
                        </select>
                        @error('w_during')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-3">
                        <label for="w_hours">I am available to work per week*</label>
                        <select class="form-control @error('w_hours') is-invalid @enderror" id="w_hours" name="w_hours" required>
                            <option value="">Select</option>
                            @foreach($working_hours as $working_hour)
                                <option value="{{ $working_hour->id }}" {{ old('w_hours')==$working_hour->id?'selected':'' }}>{{ $working_hour->name }}</option>
                            @endforeach
                        </select>
                        @error('w_hours')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="phone_type">My smartphone operating system is*</label>
                        <select class="form-control @error('phone_type') is-invalid @enderror" name="phone_type" required>
                            <option value="">Select</option>
                            @foreach($smartphones as $smartphone)
                                <option value="{{ $smartphone->id }}" {{ old('phone_type')==$smartphone->id?'selected':'' }}>{{ $smartphone->name }}</option>
                            @endforeach
                        </select>
                        @error('phone_type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label>Password</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-key"></i>
                          </span>
                            </span>
                            <input type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" id="addressl2" name="password" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <small class="text-muted">Minimum length eight</small>
                    </fieldset>
                    <fieldset class="form-group col-md-5">
                        <label>Confirm Password</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-key"></i>
                          </span>
                            </span>
                            <input type="password" placeholder="Confirm password" class="form-control @error('password-confirm') is-invalid @enderror"  id="password-confirm" name="password_confirmation" required>
                            @error('password-confirm')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </fieldset>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                </div>
        </form>
        </div>
    </section>
    <!--add-restaurant end-->

@endsection
@push('js')
<script>
    $(document).ready(function(){
        $('#ssn-input').mask('YYMMDD-XXXX');
    });
</script>
@endpush
