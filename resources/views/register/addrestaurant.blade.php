@extends('layouts.frontend.app')

@section('title','Partner')

@push('css')
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <style>
        .cross-area-icon {
            position: absolute;
            right: 12px;
            bottom: -28px;
            font-size: 14px;
            cursor: pointer;
            padding: 7px 10px;
            border-radius: 3px;
        }
        #geomap{

            height: 230px;
        }
    </style>
@endpush

@section('content')
    <!--add-restaurant start-->
    <section class="add-restaurant">
        <div class="container pull-l4 pl-lg-5 pl-sm-5">
            <div class="resto-heading">
                <img src="{{ asset('assets/frontend/images/partner-with-us/icon-1.svg') }}" alt="abeer food">
                <h1>Register Restaurant</h1>
            </div>
            <form method="POST" action="{{ route('store.restaurant') }}">
                @csrf
                @method('POST')

                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label>Name</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-user-circle"></i>
                          </span>
                            </span>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="name" value="{{ old('name') }}" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                    <fieldset class="form-group col-md-5">
                        <label>Email</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-envelope"></i>
                          </span>
                            </span>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" id="email" placeholder="example@email.com" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                </div>
                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label>Phone Number</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-phone-square"></i>
                          </span>
                            </span>
                            <input type="text" class="form-control @error('phone_number') is-invalid @enderror" value="{{ old('phone_number') }}" id="phone_number" name="phone_number" placeholder="1234" required>
                            @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                    <fieldset class="form-group col-md-5">
                        <label>Shop Name</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                          </span>
                            </span>
                            <input type="text" class="form-control @error('shop_name') is-invalid @enderror" id="shop_name" name="shop_name" placeholder="shop name" value="{{ old('shop_name') }}" required>
                            @error('shop_name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                </div>

                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label>Shop Phone Number</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-building" aria-hidden="true"></i>
                          </span>
                            </span>
                            <input type="text" class="form-control @error('shop_phone') is-invalid @enderror" id="shop_phone" name="shop_phone" placeholder="Shop Phone number" value="{{ old('shop_phone') }}" required>
                            @error('shop_phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </fieldset>
                    <fieldset class="form-group col-md-5">
                        <label>Trade Licence</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-suitcase" aria-hidden="true"></i>
                          </span>
                            </span>
                            <input type="text" class="form-control @error('license') is-invalid @enderror" value="{{ old('license') }}" name="license" id="license" placeholder="Trade License" required>
                            @error('license')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-5">
                        <label for="city">City*</label>
                        <select class="form-control @error('city') is-invalid @enderror" id="city" name="city" required>
                            <option value="">Select</option>
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}" {{ old('city')==$city->id?'selected':'' }}>{{ $city->name }}</option>
                            @endforeach
                        </select>
                        @error('city')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-5">
                        <label for="area">Area*</label>
                        <select class="form-control @error('area') is-invalid @enderror" id="area" name="area" required>
                            <option value="">Select</option>
                            @foreach($areas as $area)
                                <option value="{{ $area->id }}" {{ old('area')==$area->id?'selected':'' }}>{{ $area->name }}</option>
                            @endforeach
                        </select>
                        @error('area')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label>Open at</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
                          </span>
                            </span>
                            <input type="text" autocomplete="off" placeholder="10:00 AM" class="form-control timepicker @error('open_at') is-invalid @enderror" name="open_at" id="open_at" value="{{ old('open_at') }}" required>
                            @error('open_at')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                    <fieldset class="form-group col-md-5">
                        <label>Close at</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
                          </span>
                            </span>
                            <input type="text" autocomplete="off" placeholder="8:00 PM" class="form-control timepicker @error('close_at') is-invalid @enderror" name="close_at" id="close_at" value="{{ old('close_at') }}" required>
                            @error('close_at')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <div id="geomap" class="form-control"></div>
                    </div>
                    <fieldset class="form-group col-md-10">
                        <label>GPS location</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-location-arrow" aria-hidden="true"></i>
                          </span>
                            </span>
                            <input type="text" class="form-control @error('location') is-invalid @enderror" id="location" autocomplete="off" name="location" placeholder="gps location" required>
                            <div class="icon-btn">
                                <div class="cross-area-icon" id="my-location">
                                    <i class="fas fa-crosshairs"></i>
                                </div>
                            </div>
                            @error('location')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <input type="hidden" name="lat" value="{{ old('lat') }}" id="lat">
                            <input type="hidden" name="long" value="{{ old('long') }}" id="long">
                        </div>
                    </fieldset>
                </div>
                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label>Password</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                          </span>
                            </span>
                            <input type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <small class="text-muted">Minimum length eight</small>
                    </fieldset>
                    <fieldset class="form-group col-md-5">
                        <label>Confirm Password</label>
                        <div class="input-group">
                        <span class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                          </span>
                            </span>
                            <input type="password" placeholder="Confirm password" class="form-control @error('password-confirm') is-invalid @enderror"  id="password-confirm" name="password_confirmation" required>
                            @error('password-confirm')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </fieldset>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                </div>
            </form>
        </div>
    </section>
    <!--add-restaurant end-->

@endsection


@push('js')

    <script>
        $(document).ready(function () {

            $('#my-location').on('click', function () {

                var _token = $('input[name="_token"]').val();
                //alert(_token);
                $.ajax({
                    url: "{{ route('getLocation') }}",
                    method: "POST",
                    data: {_token: _token},
                    success: function (data) {
                        //alert(data['long']);
                        $('#location').val(data['loc']);
                        $('#lat').val(data['lat']);
                        $('#long').val(data['long']);
                        var latlng = new google.maps.LatLng(data[lat], data[long]);
                        marker.setPosition(latlng);
                        initialize();
                    }
                });
            });
            $('input.timepicker').timepicker({});
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDKBirQA6CA7Spq4IeqM9krNh2XaR7nQA"></script>
    <script>
        function initialize() {
            var initialLat = $('#lat').val();
            var initialLong = $('#long').val();
            initialLat = initialLat?initialLat:23.7574452;
            initialLong = initialLong?initialLong:90.3875378;

            var latlng = new google.maps.LatLng(initialLat, initialLong);
            var options = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("geomap"), options);

            geocoder = new google.maps.Geocoder();

            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: latlng
            });

            google.maps.event.addListener(marker, "dragend", function () {
                var point = marker.getPosition();
                map.panTo(point);
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $('#location').val(results[0].formatted_address);
                        $('#lat').val(marker.getPosition().lat());
                        $('#long').val(marker.getPosition().lng());
                    }
                });
            });

        }
    </script>
    <script>
        $(document).ready(function () {
            //load google map
            initialize();

            /*
             * autocomplete location search
             */
            var PostCodeid = '#location';
            $(function () {
                $(PostCodeid).autocomplete({
                    source: function (request, response) {
                        geocoder.geocode({
                            'address': request.term
                        }, function (results, status) {
                            response($.map(results, function (item) {
                                return {
                                    label: item.formatted_address,
                                    value: item.formatted_address,
                                    lat: item.geometry.location.lat(),
                                    lon: item.geometry.location.lng()
                                };
                            }));
                        });
                    },
                    select: function (event, ui) {
                        $('#location').val(ui.item.value);
                        $('#lat').val(ui.item.lat);
                        $('#long').val(ui.item.lon);
                        var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                        marker.setPosition(latlng);
                        initialize();
                    }
                });
            });

            /*
             * Point location on google map
             */
            $('.get_map').click(function (e) {
                var address = $(PostCodeid).val();
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $('#location').val(results[0].formatted_address);
                        $('#lat').val(marker.getPosition().lat());
                        $('#long').val(marker.getPosition().lng());
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
                e.preventDefault();
            });

            //Add listener to marker for reverse geocoding
            google.maps.event.addListener(marker, 'drag', function () {
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#location').val(results[0].formatted_address);
                            $('#lat').val(marker.getPosition().lat());
                            $('#long').val(marker.getPosition().lng());
                        }
                    }
                });
            });

        });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
@endpush
