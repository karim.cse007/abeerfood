@extends('layouts.frontend.app')

@section('title','Home')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/register/select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/register/style5.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/register/linkbutton.css')}}">
    <style>
        h1,h2,h3,h4,h5,h6,p{
            font-family: calibri;
        }
        #store-div{
            margin-left: 7%;
        }
        #store-div p{
            margin-left: 3%;
        }
        @media (max-width: 991.98px) and (min-width: 768px)
        {
            #store-logo{
                max-width: 85px;
            }
            #store-div{
                margin-left: 7%;
            }
            #store-div p{
                font-size: 12px;
            }
        }
        @media (max-width: 767.98px) and (min-width: 576px)
        {
            #store-logo{
                max-width: 68px;
                margin-top: -4%;
            }
            #store-div{
                margin-left: 6%;
            }
            #store-div p{
                font-size: 11px;
                margin-left: 2%;
            }
        }
        @media (max-width: 575.98px)
        {
            #store-logo{
                max-width: 58px;
                margin-top: -8%;
            }
            #store-div{
                margin-left: 4%;
            }
            #store-div p{
                font-size: 10px;
                margin-left: 0%;
            }
        }
    </style>
@endpush

@section('content')
    <div class="PrismicPage__root___3lxBL rtl">
        <div class="Hero__hero___264qW">
            <div class="Hero__videoRoot___2Pvky">
                <img src="{{ asset('assets/frontend/images/register/home1.jpg') }}">
            </div>
            <div class="Hero__container___3Zy6_">
                <div class="Hero__wrapper___28_Ae">
                    <h1>Make money when you want</h1>
                </div>
            </div>
        </div>
        <div class="TitleText__container___1711t">
            <div class="TitleText__titleText___lI6K7">
                <h3>What is AbeerFood delivery rider?</h3>
                <div>
                    <p>As a delivery rider at AbeerFood, you can make money by delivering food to people’s homes.<br>
                        AbeerFood offers flexible timeframes so you can make money anytime.
                    </p>
                </div>
            </div>
        </div>
        <div class="HalfImageBg__halfImageBg___Kt8DU">
            <div class="HalfImageBg__container___1SyOx">
                <div class="HalfImageBg__content___BvO35 noRtl">
                    <div class="HalfImageBg__half___V-WE3 HalfImageBg__mobileBg___3Z1YY"
                         style="background-image: url({{ asset('assets/frontend/images/register/home2.jpg') }});">
                        <div class="HalfImageBg__bgOverlay___1OFKX"></div>
                        <div class="HalfImageBg__padding___8e1ds">
                            <h2>Why choose us?</h2>
                            <div>
                                <p>
                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    Flexible work hours.<br>
                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    Easy online cash out system.<br>
                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    Quick and fair payment.</p>
                            </div>
                        </div>
                    </div>
                    <div class="HalfImageBg__half___V-WE3 HalfImageBg__desktop___2UVFF"
                         style="background-image: url({{ asset('assets/frontend/images/register/home2.jpg') }});"></div>
                </div>
            </div>
            <a class="HalfImageBg__hiddenLink___3rgQl" href=""></a>
        </div>
        <div class="CardRow__cardRow___1t0kI">
            <h2>Work part time or full time, you choose…</h2>
            <div class="CardRow__cards___2fit2">
                <div class="Card__card___1cR46">
                    <div class="Card__img___2UpF8"
                         style="background-image: url({{asset('assets/frontend/images/register/home3.JPG') }});">

                    </div>
                    <div class="Card__pad___1jcnr">
                        <h3>Deliver more, earn more</h3>
                        <div>
                            <p>The more you deliver, the
                                more money you make with
                                AbeerFood. Earn more with
                                long distance deliveries.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="Card__card___1cR46">
                    <div class="Card__img___2UpF8"
                         style="background-image: url({{asset('assets/frontend/images/register/home4.jpg') }});">
                    </div>
                    <div class="Card__pad___1jcnr">
                        <h3>Choose your vehicle</h3>
                        <div>
                            <p>Whether you have a car,
                                bike or a scooter, you can
                                do AbeerFood deliveries
                                anyway, anytime.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="Card__card___1cR46">
                    <div class="Card__img___2UpF8"
                         style="background-image: url({{asset('assets/frontend/images/register/home5.jpg') }});"></div>
                    <div class="Card__pad___1jcnr">
                        <h3>Cash out with a click</h3>
                        <div>
                            <p>Cash out and receive your
                                earnings as soon as you
                                reach threshold limit.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="FeatureComponent__featureComponent___GhXOy">
            <div class="FeatureComponent__content___3uMHE">
                <h2>An ‘easy to use’ delivery rider app in Google Play</h2>
                <div>
                    <p>Download AbeerFood delivery Rider app from Google Play store and get started with online food
                        delivery.</p>
                </div>
            </div>
            <!--download apps section-->
            <section class="section bg-white border-bottom">
                <div class="card bg-dark text-white">
                    <img style="max-height: 550px;" src="{{asset('assets/frontend/images/homepage/homeplay.jpg')}}" class="card-img" alt="...">
                    <div class="card-img-overlay">
                        <div id="store-div">

                            <p class="card-text text-hide" style="margin-top: 16%;">
                            <p class="text-white">Download AbeerFood App</p>
                            <a href="javascript:void(0);">
                                <img class="img-fluid" id="store-logo" src="{{ asset('assets/frontend/img/apple.png') }}" alt="abeerfood">
                            </a>
                            <a href="{{asset('public/storage/apk/Abeer-Food-Rider.apk')}}" download>
                                <img class="img-fluid" id="store-logo" src="{{ asset('assets/frontend/img/google.png') }}" alt="abeerfood">
                            </a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!--download apps section end-->
        </div>
        <div class="TitleCta__titleCta___2cVOP">
            <div class="TitleCta__content___crDxf">
                <h1>Are you ready to become AbeerFood delivery rider?</h1>
                <a href="{{ route('add.driver') }}" class="LinkButton__link___3Gky4 LinkButton__largeSmallLight___1ctxQ TitleCta__link___2JVoB">
                    Apply now
                </a>
            </div>
        </div>
    </div>
    </div>

@endsection


@push('js')

@endpush
