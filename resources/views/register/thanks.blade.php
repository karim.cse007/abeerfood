@extends('layouts.frontend.app')

@section('title','Posts by Shop')

@push('css')
    <style>
        .img-fluid {
            max-width: 90%;
            height: 30%;
        }
    </style>
@endpush

@section('content')
    <section class="section pt-5 pb-5 osahan-not-found-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center pt-5 pb-5">
                    <img class="img-fluid mb-4" src="{{ asset('assets/frontend/img/thanks.png') }}" alt="404">
                    <h1 class="mt-2 mb-2 text-success">Thank you for registration!</h1>
                    <p class="mb-5">Your application is pending verification. <br>
                        Your status will be informed via email.</p>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('js')

@endpush
