@extends('layouts.frontend.app')

@section('title','Home')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/register/cssfor2.css')}}">
    <link href="https://uploads-ssl.webflow.com/569646c836852a786cb03c3e/css/newwoltcms.webflow.d7aaad9e1.min.css"
          rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
    <script
        type="text/javascript">WebFont.load({google: {families: ["Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"]}});</script><!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"
            type="text/javascript"></script><![endif]-->
    <script type="text/javascript">!function (o, c) {
            var n = c.documentElement, t = " w-mod-";
            n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
        }(window, document);</script>
@endpush

@section('content')

    <div id="easiestDelivery" data-ix="invisible-until-scrolled-halfway-down" class="easiest-delivery"
         style="opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); display: block; transition: opacity 500ms ease 0s, transform 200ms ease-in 0s;">
        <div class="restaurants-delivery-container w-container"><h2 class="wolt-header-center"
                                                                    data-ix="show-details-on-click">Abeer Food is the easiest
                way to offer delivery to your&nbsp;customers</h2>
            <h3 class="wolt-header-center wolt-sub-header">It's free to join. There are no monthly fees. Your sales
                increase on day one.</h3>
            <div class="w-row">
                <div data-ix="scaleonscroll" class="icon-grid-item w-col w-col-4"
                     style="opacity: 1; transform: scaleX(1) scaleY(1) scaleZ(1); transform-style: preserve-3d; transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                    <div class="icon-ball">
                        <div class="icon growth"></div>
                    </div>
                    <h4 class="icon-header">Abeer Food increases&nbsp;sales</h4>
                    <p class="icon-text">91 percent of Abeer Food orders are extra sales you wouldn't get otherwise.</p></div>
                <div data-ix="scaleonscroll" class="icon-grid-item w-col w-col-4"
                     style="opacity: 1; transform: scaleX(1) scaleY(1) scaleZ(1); transform-style: preserve-3d; transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                    <div class="icon-ball">
                        <div class="icon fast"></div>
                    </div>
                    <h4 class="icon-header">Abeer Food is&nbsp;easy</h4>
                    <p class="icon-text">Your staff will learn Abeer Food in 15 minutes, guaranteed.</p></div>
                <div data-ix="scaleonscroll" class="icon-grid-item w-col w-col-4"
                     style="opacity: 1; transform: scaleX(1) scaleY(1) scaleZ(1); transform-style: preserve-3d; transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                    <div class="icon-ball">
                        <div class="icon delivery"></div>
                    </div>
                    <h4 class="icon-header">Abeer Food&nbsp;delivers</h4>
                    <p class="icon-text">On average, your customer will get their meal in around 35–40
                        minutes.&nbsp;</p></div>
            </div>
            <div class="w-row">
                <div class="check-list-container w-col w-col-6">
                    <ul class="check-list w-list-unstyled">
                        <li data-ix="slide-from-left-on-scroll" class="check-list-item"
                            style="opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                            You get automated weekly payments with sales reports straight to your accountant's email.
                        </li>
                        <li data-ix="slide-from-left-on-scroll" class="check-list-item"
                            style="opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                            Payments happen automatically in the background. No fumbling with cash or credit cards.
                        </li>
                        <li data-ix="slide-from-left-on-scroll" class="check-list-item"
                            style="opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                            The customers get their receipts straight into their email – great for business customers.
                        </li>
                    </ul>
                </div>
                <div class="check-list-container w-col w-col-6">
                    <ul class="check-list w-list-unstyled">
                        <li data-ix="slide-from-right-on-scroll" class="check-list-item"
                            style="opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                            More and more customers can order from you without ever leaving their home.
                        </li>
                        <li data-ix="slide-from-right-on-scroll" class="check-list-item"
                            style="opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                            Our cut includes all the card transaction fees. We also carry the credit risk for you.
                        </li>
                        <li data-ix="slide-from-right-on-scroll" class="check-list-item"
                            style="opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
                            Your restaurant can be on Abeer Food tomorrow. Contact us now to check if Abeer Food is the right fit!
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="howWoltWorks" data-ix="appear-on-load" class="how-wolt-works"
         style="background-image: url({{asset('assets/frontend/images/register/abeer1.jpg')}}); opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); transition: opacity 500ms ease 0s, transform 500ms ease 0s;">
        <div data-ix="page-navigation-sticky-when-scrolled-by" class="w-container">
            <div class="w-row">
                <div class="w-col w-col-5"><h2 data-ix="page-navigation-sticky-when-scrolled-by" class="section-header">
                        How Abeer Food works</h2><h4>Orders arrive on a tablet. They are always already paid for.</h4>
                    <ol class="restaurants-list w-list-unstyled">
                        <li class="restaurants-list-item">The customer sees your menu on their smartphone or browser.
                            They build the order and send it your way.
                        </li>
                        <li class="restaurants-list-item">You are notified of new orders. With only a couple taps, you
                            inform when the order will be ready for pickup.
                        </li>
                        <li class="restaurants-list-item">A Abeer Food courier partner picks up the order and delivers it to
                            the customer. The customer can also choose to pick up the order themselves.
                        </li>
                    </ol>
                </div>
                <div class="restaurants-ipad-container w-col w-col-7"><img
                        src="https://uploads-ssl.webflow.com/569646c836852a786cb03c3e/56a8ad42fa627bad31e7766c_works-ipad.png"
                        alt="" class="restaurant-ipad-box"></div>
            </div>
        </div>
    </div>

    <div id="merchantsOnWolt" data-ix="invisible-until-scrolled-halfway-down" class="merchants-on-wolt divider"
         style="opacity: 1; transform: translateX(0%) translateY(0%) translateZ(0px); display: block; transition: opacity 500ms ease 0s, transform 200ms ease-in 0s;">
        <div class="w-container"><h2 class="wolt-header-center">Restaurant owners: “Abeer Food simply works“</h2>
            <h3 class="wolt-header-center wolt-sub-header">Abeer Food has significantly boosted sales for Tortilla House,
                Street Gastro and many&nbsp;others.</h3>
            <div class="w-row">
                <div class="w-clearfix w-col w-col-6">
                    <div class="merchants-grid item-1"></div>
                    <div class="merchants-grid item-2"></div>
                </div>
                <div class="w-clearfix w-col w-col-6">
                    <div class="merchants-grid item-3"></div>
                    <div class="merchants-grid-quote">
                        <blockquote class="wolt-quote">“Abeer Food offers us a home delivery which we could never do by
                            ourselves. After only a couple weeks we are already receiving a pile of delivery orders
                            every day.”
                        </blockquote>
                        <div class="wolt-quote-author">Jyrki Karumo, entrepreneur, Tortilla&nbsp;House</div>
                    </div>
                </div>
            </div>
            <div class="w-row">
                <div class="w-clearfix w-col w-col-6">
                    <div class="blue-block"><p class="blue-block-text">Large chains from McDonald's to Pizza Hut and
                            Subway to Burger King are using Abeer Food as well.</p></div>
                    <div class="merchants-grid item-4"></div>
                </div>
                <div class="w-clearfix w-col w-col-6">
                    <div class="merchants-grid item-5"></div>
                </div>
            </div>
            <div class="merchants-grid-quote quote-centered">
                <blockquote class="wolt-quote quote-centered">”Abeer Food has increased our sales significantly. Employees
                    learned it in minutes. It simply works.”
                </blockquote>
                <div class="wolt-quote-author">Pasi Hassinen, entrepreneur, Street&nbsp;Gastro</div>
            </div>
        </div>
    </div>

    <div id="joinWolt" class="join-wolt">
        <div class="join-wolt-container w-container"><h2 class="wolt-header-center"><a href="https://wolt.com/contact"
                                                                                       class="link-2">Contact us
                    now!</a></h2>
            <div class="button-centered-container">
                <a href="{{ route('add.restaurant')}}" class="join-wolt-button w-button">APPLY TO BECOME A Abeer Food
                    restaurant</a>
            </div>
        </div>
    </div>


@endsection


@push('js')

@endpush
