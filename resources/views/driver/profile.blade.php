@extends('layouts.frontend.app')

@section('title','Partner')

@push('css')
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">
    <style>
        .first-profile{
            margin-left: 20px;
        }
        .nav-tabs {
            border-bottom: 2px solid #eee;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs > li {
            position: relative;
            top: 3px;
            left: -2px;
        }
        .nav-tabs li.active a {
            color: #222 !important;
        }
        .nav-tabs > li > a {
            border: none !important;
            color: #999 !important;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            -ms-border-radius: 0;
            border-radius: 0;
        }
    </style>
@endpush

@section('content')

    <!--my-account start-->
    <section class="my-account">
        <div class="profile-bg">
            <div class="my-Profile-dt">
                <div class="container">
                    <div class="row">
                        <div class="container">
                            <div class="profile-all-dt">
                                <div class="profile-name-dt first-profile">
                                    <h1>{{ $user->name }}</h1>
                                    <p><span><i class="fas fa-map-marker-alt"></i></span>{{ $user->addressl1 }}<br>{{ $user->city }}</p>
                                </div>
                            </div>
                            <div class="profile-all-dt">
                                <div class="profile-name-dt">
                                    <h1>Balance</h1>
                                    <p>${{ $user->driver->account->sum('amount') - $user->driver->cashoutdriver->sum('amount') }}</p>
                                </div>
                            </div>
                            <div class="profile-all-dt">
                                <div class="profile-name-dt">
                                    <h1>Total Ride</h1>
                                    <center><p>{{ $user->driver->account->count() }}</p></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--my-account end-->
    <!--my-account-tabs start-->
    <section class="all-profile-details">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-12">
                    <div class="left-tab-links">
                        <div class="nav nav-pills nav-stacked nav-tabs ui vertical menu fluid">
                            <a href="#about" data-toggle="tab" class="item user-tab cursor-pointer">About</a>
                            <a href="#edit-profile" data-toggle="tab" class="item user-tab cursor-pointer">Edit</a>
                            <a href="#change-password" data-toggle="tab" class="item user-tab cursor-pointer">Change Password</a>
                            <a href="#account" data-toggle="tab" class="item user-tab cursor-pointer">Account</a>
                            <a href="#vehicle-info" data-toggle="tab" class="item user-tab cursor-pointer">driving Info</a>
                            <a href="#notifications" data-toggle="tab" class="item user-tab cursor-pointer">Notifications <span class="n-badge">2</span></a>
                            <a href="#my-orders" data-toggle="tab" class="item user-tab cursor-pointer">My Orders</a>
                            <a href="#order-history" data-toggle="tab" class="item user-tab cursor-pointer">Order History</a>
                            <a href="#reviews" data-toggle="tab" class="item user-tab cursor-pointer">Reviews</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="about">
                            <div class="profile-about">
                                <div class="about-dtp">
                                    <div class="about-bg">
                                        <ul>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Ful Name</h6>
                                                    <p>{{ $user->name }}</p>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Mobile Number</h6>
                                                    <p>{{ $user->phone_number }}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Email Address</h6>
                                                    <p>{{ $user->email }}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dp-detail">
                                                    <h6>Location</h6>
                                                    <p>{{ $user->addressl1 . $user->addressl2 .','. $user->city}}</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="edit-profile">
                            <div class="timeline">
                                <div class="edit-profile">
                                    <center><label id="showprofileinfo"></label></center>
                                    <form id="driver_profile_update">
                                        @csrf
                                        @method('POST')

                                        <div class="form-group">
                                            <label for="nameUser">Full Name*</label>
                                            <input type="text" class="video-form" id="name" name="name" value="{{ $user->name }}"  placeholder="Your name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="telPhone">Phone Number*</label>
                                            <input type="text" class="video-form" id="phone_number" name="phone_number" value="{{ $user->phone_number }}" placeholder="your phone number" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="locationUser">Address line 1*</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" id="addressl1" name="addressl1" value="{{ $user->addressl1 }}" placeholder="your address" required>
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="locationUser">Address line 2</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" name="addressl2" id="addressl2" value="{{ $user->addressl2 }}" placeholder="your address">
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="locationUser">City*</label>
                                            <div class="field-input">
                                                <select name="city" id="city" required>
                                                    <option value="Stockhome" selected>Stockhome</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="locationUser">Post Code</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" name="post_code" id="post_code" value="{{ $user->post_code }}" placeholder="your address" required>
                                                <i class="fas fa-search"></i>
                                            </div>
                                        </div>

                                        <input type="submit" class="btn btn-primary" name="Submit">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="change-password">
                            <div class="timeline">
                                <div class="edit-profile">
                                    <center><label id="showpass"></label></center>
                                    <form id="user_password_form">
                                        @csrf
                                        @method('POST')
                                        <div class="form-group">
                                            <label for="OldPassword">Old Password*</label>
                                            <input type="password" class="video-form" id="old_password" name="old_password" placeholder="Enter Old Password" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="newPassword">New Password*</label>
                                            <input type="password" class="video-form" id="password" name="password" placeholder="Enter New Password" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="confirmPassword">Confirm Password*</label>
                                            <input type="password" class="video-form" id="password_confirmation" name="password_confirmation" placeholder="Enter Confirm Password" required>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Save Password</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="account">
                            <div class="timeline">
                                <div class="edit-profile">
                                    <div class="container-fluid">
                                        <div class="nav nav-pills nav-stacked nav-tabs ui menu fluid">
                                        <a href="#balance" class="item user-tab cursor-pointer" data-toggle="tab">
                                            Balance
                                        </a>
                                        <a href="#cashout" class="item user-tab cursor-pointer" data-toggle="tab">
                                            Cashout
                                        </a>
                                        </div>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="balance">
                                                <!-- Horizontal Layout -->
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                            <label for="#">Current Balance</label>
                                                                Cr.<span style="color: green;"> {{ $user->driver->account->sum('amount') - $user->driver->cashoutdriver->sum('amount') - $user->driver->cashoutrequestdriver->sum('amount')}}</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                            <label for="#">This day Earnig</label>
                                                            Cr.<span style="color: green;"> 200</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                            <label for="#">Total Earning</label><br>
                                                            Cr.<span style="color: green;"> {{ $user->driver->account->sum('amount') }}</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                            <label for="#">Cashout Request</label>
                                                            Cr.<span style="color: green;"> {{ $user->driver->cashoutrequestdriver->sum('amount') }}</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                            <label for="#">Total Cashout</label><br>
                                                            Cr.<span style="color: green;"> {{ $user->driver->cashoutdriver->sum('amount') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <label for="#">Transation history</label></br>
                                                        <table>
                                                            <tr>
                                                                <th>Id</th>
                                                                <th>Transaction Type</th>
                                                                <th>Amont</th>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                </div>
                                                <!-- #END# Horizontal Layout -->
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="cashout">
                                                <!-- Horizontal Layout -->
                                                <div class="body">
                                                    <p id="balancerror"></p>
                                                    <form id="cashout-form"  class="form-horizontal">
                                                        @csrf
                                                        @method('PUT')

                                                        <div class="row clearfix">
                                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                                <label for="old_password">Current Balance</label>
                                                            </div>
                                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <input type="text" id="current_balance" name="current_balance" class="form-control" value="{{ $user->driver->account->sum('amount') - $user->driver->cashoutdriver->sum('amount') - $user->driver->cashoutrequestdriver->sum('amount') }}" readonly="readonly">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                                <label for="password">Cashout</label>
                                                            </div>
                                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <input type="number" id="reqbalance" name="reqbalance" value="0"  class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row clearfix">
                                                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!-- #END# Horizontal Layout -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="vehicle-info">
                            <div class="timeline">
                                <div class="edit-profile">
                                    <center><label id="showprofileinfo"></label></center>
                                    <form id="driver_vehicle_info_update">
                                        @csrf
                                        @method('POST')

                                        <div class="form-group">
                                            <label for="city">City*</label>
                                            <div class="field-input">
                                                <select name="city_id" id="city_id" required>
                                                    <option value="$user->driver->city->id" selected>{{ $user->driver->city->name }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="area">Area*</label>
                                            <div class="field-input">
                                                <select name="area" id="area" required>
                                                    <option value="$user->driver->area->id" selected>{{ $user->driver->area->name }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="area">Vehicle Type*</label>
                                            <div class="field-input">
                                                <select name="vehicle_type" id="vehicle_type" required>
                                                    <option value="{{ $user->driver->vehicle->id }}" selected>{{ $user->driver->vehicle->name }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="vehicle_model">Vehicle Model*</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" id="vehicle_model" name="vehicle_model" value="{{ $user->driver->vehicle_model }}" placeholder="vehicle model" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="vehicle_number">Vehicle Number*</label>
                                            <div class="field-input">
                                                <input type="text" class="video-form" id="vehicle_number" name="vehicle_number" value="{{ $user->driver->vehicle_number }}" placeholder="vehicle number" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="checkbox-title">is your Vehicle?* <span style="color:#a1a1a1;"></span></div>
                                            <div class="filter-radio">
                                                <ul>
                                                    <li>
                                                        <input type="radio" id="c3" {{ $user->driver->is_your_vehicle ==1 ?'checked':'' }} name="cb2">
                                                        <label for="c3">Yes</label>
                                                    </li>
                                                    <li>
                                                        <input type="radio" id="c4" {{ $user->driver->is_your_vehicle ==0 ?'checked':'' }} name="cb2">
                                                        <label for="c4">No</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="checkbox-title">Have Driving License?* <span style="color:#a1a1a1;"></span></div>
                                            <div class="filter-radio">
                                                <ul>
                                                    <li>
                                                        <input type="radio" id="cc3" {{ $user->driver->is_have_license ==1 ?'checked':'' }} name="cb3">
                                                        <label for="cc3">Yes</label>
                                                    </li>
                                                    <li>
                                                        <input type="radio" id="cc4" {{ $user->driver->is_have_license ==0 ?'checked':'' }} name="cb3">
                                                        <label for="cc4">No</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <input type="submit" class="btn btn-primary" name="Submit">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="notifications">
                            <div class="profile-about">
                                <div class="tab-content-heading">
                                    <h4>Notifications</h4>
                                </div>
                                <div class="noti-all">
                                    <div class="noti-bg">
                                        <ul>
                                            <li>
                                                <div class="n-media">
                                                    <div class="n-media-left">
                                                        <img src="{{ asset('assets/frontend/images/profile/noti-1.png') }}" alt="">
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            Jassica William
                                                        </h4>
                                                        <p>comment on your Video.</p>
                                                        <div class="n-comment-date">
                                                            2 min ago
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="n-media">
                                                    <div class="n-media-left">
                                                        <img src="{{ asset('assets/frontend/images/profile/noti-1.png') }}" alt="">
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            Jassica William
                                                        </h4>
                                                        <p>comment on your Video.</p>
                                                        <div class="n-comment-date">
                                                            2 min ago
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="n-media">
                                                    <div class="n-media-left">
                                                        <img src="{{ asset('assets/frontend/images/profile/noti-1.png') }}" alt="">
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            Jassica William
                                                        </h4>
                                                        <p>comment on your Video.</p>
                                                        <div class="n-comment-date">
                                                            2 min ago
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="my-orders">
                            <div class="timeline">
                                <div class="tab-content-heading">
                                    <h4>My Orders</h4>
                                </div>
                                <div class="my-orders">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="my-checkout">
                                                <div class="table-responsive">
                                                    <table class="table  table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <td class="td-heading">Restaurants</td>
                                                            <td class="td-heading">Delivery address</td>
                                                            <td class="td-heading">Items</td>
                                                            <td class="td-heading">Total Quantity</td>
                                                            <td class="td-heading">Price</td>
                                                            <td class="td-heading">Customer Name</td>
                                                            <td class="td-heading">Action</td>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if ($orders->count() > 0)
                                                            @foreach($orders as $order)
                                                                <tr>
                                                                    <td>
                                                                        <div class="name-dt">
                                                                            <a href="#"><h4>{{ $order->shop->shop_name }}</h4></a>
                                                                        </div>
                                                                    </td>
                                                                    <td class="td-content">
                                                                        {{ $order->shippingaddress->road_name_or_number }}
                                                                        {{ $order->shippingaddress->apartment_or_suite }}
                                                                        {{ $order->shippingaddress->city->name }}
                                                                    </td>
                                                                    <td class="td-content">{{ $order->item_list}}</td>
                                                                    <td class="td-content">{{ $order->total_quantity}}</td>
                                                                    <td class="td-content">{{ $order->total_price}}</td>
                                                                    <td class="td-content">
                                                                        @if ($order->is_cooked <2)
                                                                            Not avail able now
                                                                        @else
                                                                            <p style="color: green;"> {{ $order->user->name }} </p>
                                                                        @endif
                                                                    </td>
                                                                    <td class="td-content">
                                                                        @if ($order->is_cooked == 1)
                                                                            Collect Food from shop
                                                                        @elseif ($order->is_cooked == 2)
                                                                            <button class="btn btn-danger waves-effect" type="button" onclick="deliver({{ $order->id }})">
                                                                                Deliver</i>
                                                                            </button>
                                                                            <form id="deliver-form-{{ $order->id }}" action="{{ route('driver.order.deliver') }}" method="POST" style="display: none;">
                                                                                @csrf
                                                                                @method('PUT')
                                                                                <input type="hidden" value="{{ $order->id }}" name="order_id">
                                                                            </form>
                                                                        @elseif($order->is_delivered == true)
                                                                            Shifted
                                                                        @endif
                                                                    </td>

                                                                </tr>
                                                            @endforeach

                                                        @else
                                                            <tr> <td colspan="7"><center>No order found currently</center></td></tr>
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="order-history">
                            <div class="timeline">
                                <div class="tab-content-heading">
                                    <h4>Order History</h4>
                                </div>
                                <div class="my-orders">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="my-checkout">
                                                <div class="table-responsive">
                                                    <table class="table  table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <td class="td-heading">Restaurant</td>
                                                            <td class="td-heading">Delivery address</td>
                                                            <td class="td-heading">Items</td>
                                                            <td class="td-heading">Total Quantity</td>
                                                            <td class="td-heading">Price</td>
                                                            <td class="td-heading">Customer name</td>
                                                            <td class="td-heading">Action</td>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if ($ordershistory->count() > 0)
                                                            @foreach($ordershistory as $order)
                                                                <tr>
                                                                    <td>
                                                                        <div class="name-dt">
                                                                            <a href="#"><h4>{{ $order->shop->shop_name }}</h4></a>
                                                                        </div>
                                                                    </td>
                                                                    <td class="td-content">
                                                                        {{ $order->shippingaddress->road_name_or_number }}
                                                                        {{ $order->shippingaddress->apartment_or_suite }}
                                                                        {{ $order->shippingaddress->city->name }}
                                                                    </td>
                                                                    <td class="td-content">{{ $order->item_list}}</td>
                                                                    <td class="td-content">{{ $order->total_quantity}}</td>
                                                                    <td class="td-content">{{ $order->total_price}}</td>
                                                                    <td class="td-content">

                                                                            <p style="color: green;"> {{ $order->user->name }} </p>
                                                                    </td>
                                                                    <td class="td-content">
                                                                        <p style="color: green">
                                                                                Delivered
                                                                        </p>
                                                                    </td>

                                                                </tr>
                                                            @endforeach

                                                        @else
                                                            <tr> <td colspan="7"><center>No order found currently</center></td></tr>
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="reviews">
                            <div class="timeline">
                                <div class="tab-content-heading">
                                    <h4>Reviews</h4>
                                </div>
                                @if ($reviews->count() > 0)
                                    @foreach($reviews as $review)
                                        <div class="main-comments">
                                            <div class="rating-1">
                                                <div class="user-detail-heading">
                                                    <a href="#"><img src="{{ asset('public/storage/profile/'.$review->user->image ) }}" alt="abeer food"></a>
                                                    <h4> {{ $review->user->name  }}</h4><br>
                                                    <div class="rate-star">
                                                        <i class="{{ $review->rating >= 1 ? 'fas':'far' }} fas fa-star"></i>
                                                        <i class="{{ $review->rating >= 2 ? 'fas':'far' }} fa-star"></i>
                                                        <i class="{{ $review->rating >= 3 ? 'fas':'far' }} fa-star"></i>
                                                        <i class="{{ $review->rating >= 4 ? 'fas':'far' }} fa-star"></i>
                                                        <i class="{{ $review->rating >= 5 ? 'fas':'far' }} fa-star"></i>
                                                        <span>{{ $review->rating }}</span>
                                                    </div>
                                                </div>
                                                <div class="reply-time">
                                                    <p><i class="far fa-clock"></i>
                                                        @if ($review->created_at != NULL)
                                                            {{ $review->created_at->diffForHumans() }}
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="comment-description">
                                                    <p>{{ $review->review }}</p>
                                                </div>

                                            </div>

                                            <div class="like-comment-dt">

                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="main-comments">
                                        <div class="rating-1">
                                            <div class="user-detail-heading">

                                                <h4></h4><br>
                                                <div class="rate-star">

                                                </div>
                                            </div>
                                            <div class="reply-time">
                                                <p><i class="far fa-clock"></i></p>
                                            </div>
                                            <div class="comment-description">
                                                <p> NO review yet</p>
                                            </div>

                                        </div>

                                        <div class="like-comment-dt">

                                        </div>
                                    </div>

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--my-account-tabs end-->

@endsection

@push('js')
    <script>
        $(document).ready(function(){
            $("#driver_profile_update").submit(function(){
                if( $('#name').val() == '' || $('#phone_number').val() == ''|| $('#addressl1').val() == '' || $('#city').val() == ''|| $('#post_code').val() == ''){
                    var msg ="<span style='color:red;'>Error! <strong>Field Must not be empty!!!</strong></span>";
                    $("#showprofileinfo").html(msg);
                }else{
                    $.post("<?php echo url(route('driver.update.profile')) ?>", $("#driver_profile_update").serialize(), function(data){
                        //alert("working");
                        $("#showprofileinfo").html(data);
                    });

                }
                return false;
            });

            $("#user_password_form").submit(function(){
                if( $('#old_password').val() == '' || $('#password').val() == ''|| $('#password_confirmation').val() == ''){
                    $('#old_password').val('');
                    $('#password').val('');
                    $('#password_confirmation').val('');
                    var msg ="<span style='color:red;'>Error! <strong>Field Must not be empty!!!</strong></span>";
                    $("#showpass").html(msg);
                }else if( $('#password').val() != $('#password_confirmation').val()){
                    $('#old_password').val('');
                    $('#password').val('');
                    $('#password_confirmation').val('');
                    var msg ="<span style='color:red;'>Error! <strong>Confirm Password not match!!!</strong></span>";
                    $("#showpass").html(msg);
                }else if( $('#password').val().length < 6){
                    $('#old_password').val('');
                    $('#password').val('');
                    $('#password_confirmation').val('');
                    var msg ="<span style='color:red;'>Error! <strong>Password length must be grather than Six!!!</strong></span>";
                    $("#showpass").html(msg);
                }else{
                    $.post("<?php echo url(route('driver.update.password')) ?>", $("#user_password_form").serialize(), function(data){
                        //alert("working");
                        $("#showpass").html(data);
                        $('#old_password').val('');
                        $('#password').val('');
                        $('#password_confirmation').val('');
                    });

                }
                return false;
            });
            $("#cashout-form").submit(function(){
                //alert($('#reqbalance').val());
                if( $('#reqbalance').val() <= 0 ){
                    var msg ="<span style='color:red;'>Error! <strong>Amout must not be empty!!!</strong></span>";
                    $("#balancerror").html(msg);
                }else{
                    //alert("working");
                    $.post("<?php echo url(route('driver.cashout.request')) ?>", $("#cashout-form").serialize(), function(data){
                        //alert("working");
                        $("#balancerror").html(data);
                        $('#reqbalance').val(0);
                    });
                }
                return false;
            });
        });
    </script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function deliver(id) {
            swal({
                title: 'Are you sure?',
                text: "Customer receive the product",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Received!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('deliver-form-'+ id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Customer cant receive the product',
                        'info'
                    )
                }
            })
        }
    </script>
@endpush
