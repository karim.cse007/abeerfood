@extends('layouts.frontend.app')

@section('title','Posts by Shop')

@push('css')
    <style>
        .active{
            color: blue;
            cursor: pointer;
        }
        #cart_icon:hover{
            color: white;
        }
    </style>
@endpush

@section('content')
    <!---head section in this page---->
    <section class="restaurant-detailed-banner">
        <div class="text-center">
            <img class="img-fluid cover" src="{{ asset('public/storage/shops/'.$shop->image) }}" alt="">
        </div>
        <div class="restaurant-detailed-header">
            <div class="container">
                <div class="row d-flex align-items-end">
                    <div class="col-md-8">
                        <div class="restaurant-detailed-header-left">
                            <img class="img-fluid mr-3 float-left" alt="osahan" src="{{ asset('public/storage/shops/icons/'.$shop->icon) }}">
                            <h2 class="text-white">{{ $shop->shop_name }}</h2>
                            <p class="text-white mb-1">
                                <i class="icofont-location-pin"></i> {{ $shop->location }}
                            </p>
                            <p><span class="badge badge-success">{{ $shop->isOpen($shop->id)==true?'Shop is Open':'Shop is Closed' }}</span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="restaurant-detailed-header-right text-right">
                            <button class="btn btn-success" ><i class="icofont-clock-time"></i> {{ date("h:i A", strtotime($shop->open_at)) }}
                            </button>
                            <button class="btn btn-success"><i class="icofont-clock-time"></i> {{ date("h:i A", strtotime($shop->close_at)) }}
                            </button>
                            <h6 class="text-white mb-0 restaurant-detailed-ratings"><span class="green rounded text-white"><i class="icofont-ui-rating"></i></span> {{ \App\Rating::RatingShop($shop->id)}}  <i class="ml-3 icofont-speech-comments">{{ $shop->reviews->count() }}</i></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!---end head section in this page---->
    <!---menue section start---->
    <section class="offer-dedicated-nav bg-white border-top-0 shadow-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                  <span class="restaurant-detailed-action-btn float-right">

                      @guest
                          <button class="btn btn-light btn-sm border-light-btn" type="submit"><i class="icofont-heart text-danger"></i> Mark as Favourite{{ ' ('. $shop->favorites_to_users->count() .')' }}</button>
                      @else
                          <button class="btn btn-light btn-sm border-light-btn"   type="button" onclick="document.getElementById('favorite-form-{{ $shop->id }}').submit();">
                              <i class="icofont-heart {{ Auth::user()->favorite_shops->where('pivot.shop_id',$shop->id)->count() >0 ? 'text-success':'text-black' }}"></i> Mark as Favourite{{ ' ('. $shop->favorites_to_users->count() .')' }}
                          </button>

                          <form id="favorite-form-{{ $shop->id }}" method="POST" action="{{ route('shop.favorite',$shop->id) }}" style="display: none;">
                              @csrf
                          </form>
                      @endguest
                  </span>
                    <ul class="nav" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-order-online-tab" data-toggle="pill" href="#pills-order-online" role="tab" aria-controls="pills-order-online" aria-selected="true">Order Online</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" id="pills-restaurant-info-tab" data-toggle="pill" href="#pills-restaurant-info" role="tab" aria-controls="pills-restaurant-info" aria-selected="false">Restaurant Info</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews" role="tab" aria-controls="pills-reviews" aria-selected="false">Ratings & Reviews</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!---menue section end---->
    <section class="offer-dedicated-body pt-2 pb-2 mt-4 mb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="offer-dedicated-body-left">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-order-online" role="tabpanel" aria-labelledby="pills-order-online-tab">
                                @if($shop->isOpen($shop->id)==false)
                                    <h5 class="mb-1 text-center text-danger">Shop is close now</h5>
                                @endif
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-selected="true">All</a>
                                        @foreach($categories as $category)
                                            @if($category->posts->count() >0)
                                            <a class="nav-item nav-link" id="nav-{{str_slug($category->name)}}-tab" data-toggle="tab" href="#nav-{{str_slug($category->name)}}" role="tab" aria-controls="nav-{{$category->name}}" aria-selected="false">{{ $category->name }}</a>
                                            @endif
                                        @endforeach
                                    </div>
                                </nav>
                                <div class="tab-content container" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">

                                        @foreach($categories as $category)
                                            @if($category->posts->count() >0)
                                                <h5>{{ $category->name }}</h5>
                                                <div class="row">
                                                    @foreach($category->posts as $post)
                                                        <div class="col-md-4 col-sm-6 mb-4">
                                                            <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                                                                <div class="p-3 position-relative">
                                                                    <div class="list-card-body">
                                                                        <h6 class="mb-1"><a  class="text-black">{{ $post->title }}</a></h6>
                                                                        <p class="text-gray mb-2">{{ $post->about }}</p>
                                                                        <p class="text-gray time mb-0"><a class="btn btn-link btn-sm pl-0 text-black pr-0">{{ $post->price }} kr. </a>
                                                                            <span class="float-right">
                                                                            @if($shop->isOpen($shop->id)==true)
                                                                                    @if($post->days->where('pivot.day_id',\Carbon\Carbon::now()->dayOfWeek +1)->count() <1)
                                                                                        <span style="color: red;">Not Available</span>
                                                                                    @elseif($post->available == 0)
                                                                                        <span style="color: red;">Out of stock</span>
                                                                                    @else
                                                                                        <a class="btn btn-outline-secondary btn-sm" id="cart_icon" onclick="document.getElementById('cart-form-{{ $post->id }}').submit(); return false;"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                                                                    @endif
                                                                                @endif
                                                                        </span>
                                                                        </p>
                                                                        @if($shop->isOpen($shop->id)==true)
                                                                            <form id="cart-form-{{ $post->id }}" method="POST" action="{{ route('customer.cart.add',$post->id) }}" style="display: none;">
                                                                                @csrf

                                                                            </form>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>

                                    @foreach($categories as $category)
                                        @if($category->posts->count() >0)
                                        <div class="tab-pane fade show" id="nav-{{str_slug($category->name)}}" role="tabpanel" aria-labelledby="nav-{{str_slug($category->name)}}-tab">
                                            <h5>{{ $category->name }}</h5>
                                            <div class="row">
                                            @foreach($category->posts as $post)
                                                <div class="col-md-4 col-sm-6 mb-4">
                                                    <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                                                        <div class="p-3 position-relative">
                                                            <div class="list-card-body">
                                                                <h6 class="mb-1"><a  class="text-black">{{ $post->title }}</a></h6>
                                                                <p class="text-gray mb-2">{{ $post->about }}</p>
                                                                <p class="text-gray time mb-0"><a class="btn btn-link btn-sm pl-0 text-black pr-0">{{ $post->price }} kr. </a>
                                                                    <span class="float-right">
                                                                        @if($shop->isOpen($shop->id)==true)
                                                                            @if($post->days->where('pivot.day_id',\Carbon\Carbon::now()->dayOfWeek +1)->count() <1)
                                                                                <span style="color: red;">Not Available</span>
                                                                            @elseif($post->available == 0)
                                                                                <span style="color: red;">Out of stock</span>
                                                                            @else
                                                                            <a class="btn btn-outline-secondary btn-sm" id="cart_icon" onclick="document.getElementById('cart-form-{{ $post->id }}').submit(); return false;"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                                                            @endif
                                                                        @endif
                                                                    </span>
                                                                </p>
                                                                @if($shop->isOpen($shop->id)==true)
                                                                    <form id="cart-form-{{ $post->id }}" method="POST" action="{{ route('customer.cart.add',$post->id) }}" style="display: none;">
                                                                        @csrf

                                                                    </form>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <!----restaurant info section----->
                            <div class="tab-pane fade" id="pills-restaurant-info" role="tabpanel" aria-labelledby="pills-restaurant-info-tab">
                                <div id="restaurant-info" class="bg-white rounded shadow-sm p-4 mb-4">
                                    <div class="address-map float-right ml-5">
                                        <div class="mapouter">
                                            <div class="gmap_canvas">
                                               <img src="https://maps.googleapis.com/maps/api/staticmap?center={{ $shop->lat }},{{ $shop->long }}&zoom=13&size=400x220&maptype=roadmap&markers=color:blue%7Clabel:S%7C{{ $shop->lat }},{{ $shop->long }}&key=AIzaSyAoo8LOXsE5bJUC5UVVzLK7qN3rKv1C__o">
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="mb-4">{{ $shop->shop_name }}</h5>
                                    <p class="mb-3">{{ $shop->location }}

                                    </p>
                                    <p class="mb-2 text-black"><i class="icofont-phone-circle text-primary mr-2"></i> {{ $shop->phone_number }}</p>
                                    <p class="mb-2 text-black"><i class="icofont-email text-primary mr-2"></i> {{ $shop->user->name }}</p>
                                    <p class="mb-2 text-black"><i class="icofont-clock-time text-primary mr-2"></i> Today  {{date("h:i A", strtotime($shop->open_at)) }} – {{ date("h:i A", strtotime($shop->close_at)) }}
                                        <span class="badge badge-success"> {{ $shop->isOpen($shop->id)==true?'Open':'Close' }} </span>
                                    </p>
                                    <hr class="clearfix">

                                </div>
                            </div>
                            <!----restaurant info section end----->
                            <!----review section start---->
                            <div class="tab-pane fade" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">

                                <div class="bg-white rounded shadow-sm p-4 mb-4 restaurant-detailed-ratings-and-reviews" id="review-content">
                                    <h5 class="mb-1">All Reviews (<span>{{ $shop->reviews->count() }}</span>)</h5>

                                    @if ($reviews->count() >0)
                                        @foreach ($reviews as $review)
                                            <div class="reviews-members pt-4 pb-4">
                                                <div class="media">
                                                    <a href="#"><img alt="{{ $review->user->name }}" src="{{ asset('public/storage/profile/'.$review->user->image) }}" class="mr-3 rounded-pill"></a>
                                                    <div class="media-body">
                                                        <div class="reviews-members-header">
                                                            <h6 class="mb-1"><a class="text-black" href="#">{{ $review->user->name }}</a></h6>
                                                            <p class="text-gray">
                                                                @if ($review->created_at != NULL)
                                                                    {{ $review->created_at->diffForHumans() }}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="reviews-members-body">
                                                            <p>{{ $review->review }}</p>
                                                        </div>
                                                        <div class="reviews-members-footer">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        @endforeach
                                        @if ($reviews->count() >5)
                                        <a class="text-center w-100 d-block mt-4 font-weight-bold" id="review-all" href="javascript:void(0);">See All Reviews</a>
                                        @endif
                                    @else
                                        <p>Have not any review yet may be you are the first </p>
                                    @endif
                                </div>
                                <div class="bg-white rounded shadow-sm p-4 mb-5 rating-review-select-page">
                                    <h5 class="mb-4">Leave Comment</h5>
                                    @guest
                                        <p>For review you must be login first</p>
                                    @else
                                        <p class="mb-2">Rate the Shop</p>
                                        <div class="alert alert-success hidden" id="rating_success" role="alert"></div>
                                        <div class="mb-4">
                                         <span class="star-rating">
                                         <a href="javascript:void(0);" id="rat1"><i class="icofont-ui-rating icofont-2x"></i></a>
                                         <a href="javascript:void(0);" id="rat2"><i class="icofont-ui-rating icofont-2x"></i></a>
                                         <a href="javascript:void(0);" id="rat3"><i class="icofont-ui-rating icofont-2x"></i></a>
                                         <a href="javascript:void(0);" id="rat4"><i class="icofont-ui-rating icofont-2x"></i></a>
                                         <a href="javascript:void(0);" id="rat5"><i class="icofont-ui-rating icofont-2x"></i></a>
                                         </span>
                                        </div>
                                        @if(session('s_review'))
                                            <div class="alert alert-danger" role="alert">
                                                {{ session('s_review') }}
                                            </div>
                                        @endif
                                        <form id="form-comment-user" method="post" action="{{ route('review.store',$shop->id) }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="review">Your Review</label>
                                                <textarea class="form-control" name="review" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <input class="btn btn-primary btn-sm" type="submit" value="Submit Review">
                                            </div>
                                        </form>
                                    @endguest

                                </div>

                            </div>
                            <!----review section end---->
                        </div>
                    </div>
                </div>

                <!-- Cart section start---->
                <div class="col-md-4">
                    <div class="generator-bg rounded shadow-sm mb-4 p-4 osahan-cart-item">
                        <div class="d-flex mb-4 osahan-cart-item-profile" id="cart-info">

                            <div class="d-flex flex-column">
                                <h6 class="mb-1 text-black">Your Order
                                </h6>
                                <p class="mb-0 text-white"><i class="icofont-location-pin"></i></p>
                            </div>
                        </div>
                        <?php $i = 0; $flag =0 ;?>
                        @foreach( $cartCollections as $cartCollection)
                            @if($cartCollection->attributes->shop_id == $shop->id)
                                <?php $flag =1 ; ?>
                                <div class="bg-white rounded shadow-sm mb-2">
                                    <div class="gold-members p-2 border-bottom">
                                        <p class="text-gray mb-0 float-right ml-2">{{ $cartCollection->price * $cartCollection->quantity }} kr.</p>
                                        <span class="count-number float-right">
                                       <button class="btn btn-outline-secondary  btn-sm left dec" onclick="document.getElementById('decrement-form-{{ $cartCollection->id }}').submit();"> <i class="icofont-minus"></i> </button>
                                       <input class="count-number-input" type="text" value="{{ $cartCollection->quantity }}" readonly="">
                                       <button class="btn btn-outline-secondary btn-sm right inc" onclick="document.getElementById('increment-form-{{ $cartCollection->id }}').submit();"> <i class="icofont-plus"></i> </button>
                                        <form id="decrement-form-{{ $cartCollection->id }}" method="POST" action="{{ route('customer.cart.update') }}" style="display: none;">
                                            @csrf
                                            <input type="hidden" name="pid" value="1">
                                            <input type="hidden" name="cartId" value="{{ $cartCollection->id }}">
                                        </form>
                                        <form id="increment-form-{{ $cartCollection->id }}" method="POST" action="{{ route('customer.cart.update') }}" style="display: none;">
                                            @csrf
                                            <input type="hidden" name="pid" value="2">
                                            <input type="hidden" name="cartId" value="{{ $cartCollection->id }}">
                                        </form>
                                   </span>
                                        <div class="media">
                                            <div class="mr-2"><i class="icofont-ui-press text-danger food-item"></i></div>
                                            <div class="media-body">
                                                <p class="mt-1 mb-0 text-black">{{ $cartCollection->name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        @if ( $flag == 0)
                            <p>Cart is empty buy now and enjoy eating!!!</p>
                        @endif
                    </div>
                    <div class="mb-2 bg-white rounded p-2 clearfix">
                        <p class="mb-1">Item Total <span class="float-right text-dark">{{ $total }} kr.</span></p>
                        <p class="mb-1">Delivery Fee <span class="text-info" data-toggle="tooltip" data-placement="top" title="With in 5km charge 30 Cr. only">
                           <i class="icofont-info-circle"></i>
                           </span> <span class="float-right text-dark">0 kr.</span>
                        </p>

                        <hr />
                        <h6 class="font-weight-bold mb-0">TO PAY  <span class="float-right">{{ $total }} kr.</span></h6>
                    </div>
                    <a href="{{ route('customer.checkout') }}" class="btn btn-primary btn-block btn-lg">Checkout {{ $total }} kr.
                        <i class="icofont-long-arrow-right"></i></a>

                </div>
            </div>
            <!-- Cart section end---->
        </div>
    </section>

@endsection

@push('js')
    <script>
        $(document).ready(function(){
            $("#rat1").hover(function(){
                $(this).addClass("active");
                $('#rat2').removeClass("active");
                $('#rat3').removeClass("active");
                $('#rat4').removeClass("active");
                $('#rat5').removeClass("active");
            },function () {
                $('#rat1').removeClass('active');
                $('#rat2').removeClass('active');
                $('#rat3').removeClass('active');
                $('#rat4').removeClass('active');
                $('#rat5').removeClass('active');
            });
            $("#rat2").hover(function(){
                $(this).addClass("active");
                $('#rat1').addClass("active");
                $('#rat3').removeClass("active");
                $('#rat4').removeClass("active");
                $('#rat5').removeClass("active");
            },function () {
                $('#rat1').removeClass('active');
                $('#rat2').removeClass('active');
                $('#rat3').removeClass('active');
                $('#rat4').removeClass('active');
                $('#rat5').removeClass('active');
            });
            $("#rat3").hover(function(){
                $(this).addClass("active");
                $('#rat1').addClass("active");
                $('#rat2').addClass("active");
                $('#rat4').removeClass("active");
                $('#rat5').removeClass("active");
            },function () {
                $('#rat1').removeClass('active');
                $('#rat2').removeClass('active');
                $('#rat3').removeClass('active');
                $('#rat4').removeClass('active');
                $('#rat5').removeClass('active');
            });
            $("#rat4").hover(function(){
                $(this).addClass("active");
                $('#rat1').addClass("active");
                $('#rat2').addClass("active");
                $('#rat3').addClass("active");
                $('#rat5').removeClass("active");
            },function () {
                $('#rat1').removeClass('active');
                $('#rat2').removeClass('active');
                $('#rat3').removeClass('active');
                $('#rat4').removeClass('active');
                $('#rat5').removeClass('active');
            });
            $("#rat5").hover(function(){
                $(this).addClass("active");
                $('#rat1').addClass("active");
                $('#rat2').addClass("active");
                $('#rat3').addClass("active");
                $('#rat4').addClass("active");
            },function () {
                $('#rat1').removeClass('active');
                $('#rat2').removeClass('active');
                $('#rat3').removeClass('active');
                $('#rat4').removeClass('active');
                $('#rat5').removeClass('active');
            });
            var _token = $('input[name="_token"]').val();
            var shop_id = "{{ $shop->id }}";
            $('#rat1').on('click',function () {
               $.post("<?php echo url(route('rating.store'))?>", { _token:_token,rate:1,shop_id:shop_id},function (data) {
                   if(data['success']){
                       //alert("Working");
                       $('#rating_success').removeClass('hidden');
                       $('#rating_success').html(data['success']);
                       $(this).addClass("active");
                       $('#rat2').removeClass("active");
                       $('#rat3').removeClass("active");
                       $('#rat4').removeClass("active");
                       $('#rat5').removeClass("active");
                   }
               });
                return false;
            });
            $('#rat2').on('click',function () {
                $.post("<?php echo url(route('rating.store'))?>", { _token:_token,rate:2,shop_id:shop_id},function (data) {
                    if(data['success']){
                        //alert("Working");
                        $('#rating_success').removeClass('hidden');
                        $('#rating_success').html(data['success']);
                        $(this).addClass("active");
                        $('#rat1').addClass("active");
                        $('#rat3').removeClass("active");
                        $('#rat4').removeClass("active");
                        $('#rat5').removeClass("active");
                    }
                });
                return false;
            });
            $('#rat3').on('click',function () {
                $.post("<?php echo url(route('rating.store'))?>", { _token:_token,rate:3,shop_id:shop_id},function (data) {
                    if(data['success']){
                        //alert("Working");
                        $('#rating_success').removeClass('hidden');
                        $('#rating_success').html(data['success']);
                        $(this).addClass("active");
                        $('#rat1').addClass("active");
                        $('#rat2').addClass("active");
                        $('#rat4').removeClass("active");
                        $('#rat5').removeClass("active");
                    }
                });
                return false;
            });
            $('#rat4').on('click',function () {
                $.post("<?php echo url(route('rating.store'))?>", { _token:_token,rate:4,shop_id:shop_id},function (data) {
                    if(data['success']){
                        //alert("Working");
                        $('#rating_success').removeClass('hidden');
                        $('#rating_success').html(data['success']);
                        $(this).addClass("active");
                        $('#rat1').addClass("active");
                        $('#rat2').addClass("active");
                        $('#rat3').addClass("active");
                        $('#rat5').removeClass("active");
                    }
                });
                return false;
            });
            $('#rat5').on('click',function () {
                $.post("<?php echo url(route('rating.store'))?>", { _token:_token,rate:5,shop_id:shop_id},function (data) {
                    if(data['success']){
                        //alert("Working");
                        $('#rating_success').removeClass('hidden');
                        $('#rating_success').html(data['success']);
                        $(this).addClass("active");
                        $('#rat1').addClass("active");
                        $('#rat2').addClass("active");
                        $('#rat3').addClass("active");
                        $('#rat4').addClass("active");
                    }
                });
                return false;
            });
            $('#review-all').on('click',function () {
                $('#review-content').load("{{route('load.reviews',$shop->id)}}").fadeIn('slow');
            });
        });
    </script>
@endpush
