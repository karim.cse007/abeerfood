@extends('layouts.frontend.app')

@section('title','Contact')

@push('css')
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">
@endpush

@section('content')

    <!--title-bar start-->
    <section class="title-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="left-title-text">
                        <h3>Contact Us</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--title-bar end-->
    <!--contact-us start-->
    <section class="contact-us">
        <div class="contact-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d352.72696009959935!2d17.9095053!3d59.3947736!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x465f9e55c3425c35%3A0xd474cdffe811ee18!2s2nd%20floor%2C%20Risingeplan%2032%2C%20163%2068%20Sp%C3%A5nga%2C%20Sweden!5e1!3m2!1sen!2sbd!4v1568012554159!5m2!1sen!2sbd" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </section>
    <section class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-12">
                    <div class="contact-heading">
                        <h1>Contact Information</h1>
                    </div>
                    <div class="contact-info">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="contact-item">
                                    <img src="{{ asset('assets/frontend/images/contact/icon-1.svg') }}" alt="abeer food">
                                    <h4>Address</h4>
                                    <p>Risingeplan 32, 2nd floor,16368 Spånga, Sweden</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="contact-item">
                                    <img src="{{ asset('assets/frontend/images/contact/icon-2.svg') }}" alt="abeer food">
                                    <h4>Email Address</h4>
                                    <p>admin@abeer.se<br>info@abeer.se</p>
                                </div>
                            </div>
                            <!------
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="contact-item">
                                    <img src="{{ asset('assets/frontend/images/contact/icon-3.svg') }}" alt="abeer food">
                                    <h4>Phone Number</h4>
                                    <p>+2 123 456 789<br>+2 987 654 3210</p>
                                </div>
                            </div>
                            ------>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="contact-item">
                                    <img src="{{ asset('assets/frontend/images/contact/icon-4.svg') }}" alt="abeer food">
                                    <h4>24 Support</h4>
                                    <p>support@abeer.se</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-12">
                    <div class="contact-heading">
                        <h1>Write To Us</h1>
                    </div>
                    <div id="contact-info"></div>
                    <div class="contact-info">
                        <form id="contact-form">
                            @csrf
                            @method('POST')
                            @if(Session::has('success'))
                                  <div class="alert alert-success">
                                      <b>Successfully</b> Sent Your message.
                                  </div>
                            @endif
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="form-group">
                                        <label for="userName">Name*</label>
                                        <input type="text" class="video-form @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" autocomplete="name" placeholder="Your name" autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="form-group">
                                        <label for="emailAddress">Email*</label>
                                        <input type="email" class="video-form @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" autocomplete="email" placeholder="example@email.com" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="form-group">
                                        <label for="typeMessage">Message*</label>
                                        <textarea class="text-area @error('message') is-invalid @enderror" id="message" name="message" required autocomplete="message" placeholder="Your message write here" autofocus>{{ old('message') }}</textarea>
                                        @error('message')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <input type="submit" class="btn btn-primary" name="submit" value="Sent Message">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--contact-us end-->

@endsection


@push('js')
    <script>
        $(document).ready(function() {

            $("#contact-form").submit(function () {
                if ($('#name').val() == '' || $('#email').val() == '' || $('#message').val() == '' || $('#city').val() == '' || $('#post_code').val() == '') {
                    var msg = "<div class='alert alert-danger'>" +
                        "<b>Field</b> Must not be empty" +
                        "</div>";
                    $("#contact-info").html(msg);
                } else {
                    $.post("<?php echo url(route('contact.sent')) ?>", $("#contact-form").serialize(), function (data) {
                        //alert("working");
                        $("#contact-info").html(data);
                        $("#name").val('');
                        $("#email").val('');
                        $("#message").val('');
                    });

                }
                return false;
            });
        });
    </script>
@endpush
