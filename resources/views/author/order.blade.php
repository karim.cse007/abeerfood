@extends('layouts.backend.app')

@section('title','Order List')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            All orders
                            <span class="badge bg-blue">{{ $orders->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Customer Name</th>
                                    <th>Item List</th>
                                    <th>Total Quantity</th>
                                    <th>Total Price</th>
                                    <th>Address At</th>
                                    <th>Rider name</th>
                                    <th>Cooking</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>SL</th>
                                    <th>Customer Name</th>
                                    <th>Item List</th>
                                    <th>Total Quantity</th>
                                    <th>Total Price</th>
                                    <th>Address At</th>
                                    <th>Rider name</th>
                                    <th>Cooking</th>
                                    <th>Status</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                @foreach($orders as $key=>$order)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $order->user->name }}</td>
                                        <td>{{ $order->item_list }}</td>
                                        <td>{{ $order->total_quantity }}</td>
                                        <td>{{ $order->total_price }} kr.</td>
                                        <td>{{ $order->shippingaddress->location }}</td>
                                        <td class="text-center">
                                            @if ($order->driver_id == null)
                                                waiting
                                            @else
                                                <a href="#">{{ $order->driver->user->name }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($order->is_cooked == 0)
                                                <button class="btn btn-danger waves-effect" type="button" onclick="cookingReady({{ $order->id }})">
                                                    Complete</i>
                                                </button>
                                                <form id="cooking-form-{{ $order->id }}" action="{{ route('author.cooking.ready',$order->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                    @method('PUT')
                                                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                                                </form>
                                            @elseif($order->is_cooked == 1)
                                                <button class="btn btn-danger waves-effect"  {{ $order->driver_id ==null ? 'disabled':'' }} type="button" onclick="deliver({{ $order->id }})">
                                                    Deliver</i>
                                                </button>
                                                <form id="deliver-form-{{ $order->id }}" action="{{ route('author.shop.cooking.deliver',$order->id) }}" method="POST" style="display: none;">
                                                    @csrf
                                                    @method('PUT')
                                                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                                                </form>
                                            @elseif($order->is_cooked ==2)
                                                <p style="color:green">Success</p>
                                            @endif
                                        </td>
                                        <td>
                                            <p style="color: green">{{ $order->is_cooked == 2 ? 'Delivering': ''}}</p>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function cookingReady(id) {
            swal({
                title: 'Are you sure?',
                text: "Your cooking is ready to deliver",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, ready!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('cooking-form-'+ id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Your cooking cant ready to deliver',
                        'info'
                    )
                }
            })
        }
        function deliver(id) {
            swal({
                title: 'Are you sure?',
                text: "Delivery man receive the product",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Received!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('deliver-form-'+ id).submit();
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swal(
                        'Cancelled',
                        'Delivery man cant receive the product',
                        'info'
                    )
                }
            })
        }
    </script>
@endpush
