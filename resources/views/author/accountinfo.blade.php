@extends('layouts.backend.app')

@section('title','Shop Information')

@push('css')

@endpush

@section('content')
    <div class="container-fluid">
        <!-- Tabs With Icon Title -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Account Information
                        </h2>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#profile_with_icon_title" data-toggle="tab">
                                    <i class="material-icons">shop</i> Basic Info
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#cashout_request" data-toggle="tab">
                                    <i class="material-icons">shop</i> Cashout
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="profile_with_icon_title">
                                <!-- Horizontal Layout -->
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="#">Current Balance</label><br>
                                            <span style="color: green;"> Cr. {{ $currentBalance  }}</span>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="#">This day Earnig</label><br>
                                            <span style="color: green;">Cr. {{ $shop->shopowneraccount->where('created_at', '<=', \Carbon\Carbon::now())->sum('amount')  }}</span>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="#">Total Earning</label><br>
                                            Cr.<span style="color: green;"> {{ $totalBalance  }}</span>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="#">Cashout Request</label><br>
                                            Cr.<span style="color: green;"> {{ $totalCashoutRequest }}</span>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="#">Total Cashout</label><br>
                                            Cr.<span style="color: green;"> {{ $totalCashout  }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- #END# Horizontal Layout -->
                            </div>
                            <div role="tabpanel" class="tab-pane fade in" id="cashout_request">
                                <!-- Horizontal Layout -->
                                <div class="body">
                                    <div class="row clearfix">
                                        <p id="balancerror"></p>
                                        <form id="cashout-form"  class="form-horizontal" method="POST">
                                            @csrf
                                            @method('PUT')

                                            <div class="row clearfix">
                                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                    <label for="old_password">Available Balance</label>
                                                </div>
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="current_balance" name="current_balance" class="form-control" value="{{ $currentBalance }}" readonly="readonly">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                    <label for="password">Cashout</label>
                                                </div>
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="number" id="reqbalance" name="reqbalance" value="0"  class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- #END# Horizontal Layout -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Tabs With Icon Title -->
    </div>
@endsection

@push('js')
<script>
    $(document).ready(function() {
        $("#cashout-form").submit(function () {
            //alert($('#reqbalance').val());
            if ($('#reqbalance').val() <= 0) {
                var msg = "<span style='color:red;'>Error! <strong>Pleas enter amount to cashout!!!</strong></span>";
                $("#balancerror").html(msg);
            } else {
                //alert("working");
                $.post("<?php echo url(route('author.cashout.request')) ?>", $("#cashout-form").serialize(), function (data) {
                    //alert("working");
                    $("#balancerror").html(data);
                    $('#reqbalance').val(0);
                });

            }
            return false;
        });
    });
</script>
@endpush
