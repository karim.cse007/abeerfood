@extends('layouts.backend.app')

@section('title','Shop Information')

@push('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <style>
        #geomap{
            height: 230px;
        }
    </style>
@endpush


@section('content')
    <div class="container-fluid">
        <!-- Tabs With Icon Title -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Shop Information
                        </h2>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#profile_with_icon_title" data-toggle="tab">
                                    <i class="material-icons">shop</i> Update
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="profile_with_icon_title">
                                <!-- Horizontal Layout -->

                                <div class="body">
                                    @if ($shop->is_approved == false)
                                        <center><label style="color: red">Your Shop is not approved yet!!</label></center>
                                    @elseif($shop->is_blocked == true)
                                        <center><label style="color: red">Your account is blocked contact with admin</label></center>
                                    @else
                                        <center><label style="color: green">Active Shop</label></center>
                                    @endif
                                    <div class="row clearfix">
                                        <div class="col-md-10 pl-md-5" style="margin-left: 10%">
                                            <div class="form-group">
                                                <center> <img src="{{ asset('public/storage/shops/'.$shop->image) }}" width="100%;" height="200px;"></center>
                                            </div>
                                        </div>
                                    </div>
                                    <center><label id="show"></label></center>
                                    <form method="POST" action="{{ route('author.shop.update') }}" class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                <label for="image">Shop Image:</label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="file" name="image">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">

                                            <div class="col-md-10 pl-md-5" style="margin-left: 18%">
                                                <div class="form-group">
                                                    <img src="{{ asset('public/storage/shops/icons/'.$shop->icon) }}" width="60px" height="60px">

                                                </div>
                                            </div>

                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                <label for="image">Shop Icon:</label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="file" name="icon">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                <label for="name">Shop Name:</label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" id="shop_name" name="shop_name" value="{{ $shop->shop_name }}" class="form-control @error('shop_name') is-invalid @enderror" placeholder="Enter your shop name" required>
                                                        @error('shop_name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                <label for="name">Phone Number:</label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" id="phone_number" name="phone_number" value="{{ $shop->phone_number }}" class="form-control" placeholder="Phone Number" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                <label for="email_address_2">Trade Licence:</label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" id="trade_license" name="trade_license" value="{{ $shop->trade_license }}" class="form-control" placeholder="Enter Trade license" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 form-control-label">
                                                <label for="city">City:</label>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-3">
                                                <div class="form-group">
                                                    <select class=" @error('city') is-invalid @enderror" id="city" name="city" required>
                                                        <option value="">Select</option>
                                                        @foreach($cities as $city)
                                                            <option value="{{ $city->id }}" {{ $shop->city->id ==$city->id?'selected':'' }}>{{ $city->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('city')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                                <label for="area">State:</label>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-3">
                                                <div class="form-group">
                                                    <select class="video-form @error('area') is-invalid @enderror" id="area" name="area" required>
                                                        <option value="">Select</option>
                                                        @foreach($areas as $area)
                                                            <option value="{{ $area->id }}" {{ $shop->area->id==$area->id?'selected':'' }}>{{ $area->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('area')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 form-control-label">
                                                <label for="open_at">Open:</label>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-3">
                                                <div class="form-group">
                                                    <div class="checkbox-title">From*</div>
                                                    <input type="text" autocomplete="off" value="{{ date("h:i A",strtotime($shop->open_at)) }}" class="time_picker @error('open_at') is-invalid @enderror" name="open_at" id="open_at" required>
                                                    @error('open_at')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-3">
                                                <div class="form-group">
                                                    <div class="checkbox-title">To*</div>
                                                    <input type="text" autocomplete="off" value="{{ date("h:i A",strtotime($shop->close_at)) }}" class="time_picker @error('close_at') is-invalid @enderror" name="close_at" id="close_at" required>
                                                    @error('close_at')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">

                                            <div class="col-md-10 pl-md-5" style="margin-left: 10%">
                                                <div class="form-group">
                                                    <div id="geomap" class="form-control"></div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                <label for="location">Location:</label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" id="location" name="location" value="{{ $shop->location }}" class="form-control" placeholder="Enter shop location">
                                                        <input type="hidden" name="lat" value="{{ $shop->lat }}" id="lat">
                                                        <input type="hidden" name="long" value="{{ $shop->long }}" id="long">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <!-- #END# Horizontal Layout -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Tabs With Icon Title -->
    </div>
@endsection

@push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoo8LOXsE5bJUC5UVVzLK7qN3rKv1C__o"></script>
    <script>
        function initialize() {
            var initialLat = $('#lat').val();
            var initialLong = $('#long').val();
            initialLat = initialLat?initialLat:"{{ $shop->lat }}";
            initialLong = initialLong?initialLong:"{{$shop->long}}";

            var latlng = new google.maps.LatLng(initialLat, initialLong);
            var options = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("geomap"), options);

            geocoder = new google.maps.Geocoder();

            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: latlng
            });

            google.maps.event.addListener(marker, "dragend", function () {
                var point = marker.getPosition();
                map.panTo(point);
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $('#location').val(results[0].formatted_address);
                        $('#lat').val(marker.getPosition().lat());
                        $('#long').val(marker.getPosition().lng());
                    }
                });
            });

        }
    </script>
    <script>
        $(document).ready(function () {
            //load google map
            initialize();

            /*
             * autocomplete location search
             */
            var PostCodeid = '#location';
            $(function () {
                $(PostCodeid).autocomplete({
                    source: function (request, response) {
                        geocoder.geocode({
                            'address': request.term
                        }, function (results, status) {
                            response($.map(results, function (item) {
                                return {
                                    label: item.formatted_address,
                                    value: item.formatted_address,
                                    lat: item.geometry.location.lat(),
                                    lon: item.geometry.location.lng()
                                };
                            }));
                        });
                    },
                    select: function (event, ui) {
                        $('#location').val(ui.item.value);
                        $('#lat').val(ui.item.lat);
                        $('#long').val(ui.item.lon);
                        var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                        marker.setPosition(latlng);
                        initialize();
                    }
                });
            });

            /*
             * Point location on google map
             */
            $('.get_map').click(function (e) {
                var address = $(PostCodeid).val();
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $('#location').val(results[0].formatted_address);
                        $('#lat').val(marker.getPosition().lat());
                        $('#long').val(marker.getPosition().lng());
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
                e.preventDefault();
            });

            //Add listener to marker for reverse geocoding
            google.maps.event.addListener(marker, 'drag', function () {
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#location').val(results[0].formatted_address);
                            $('#lat').val(marker.getPosition().lat());
                            $('#long').val(marker.getPosition().lng());
                        }
                    }
                });
            });

        });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script>
        $(document).ready(function () {
            $('input.time_picker').timepicker({});
        });
    </script>
@endpush
