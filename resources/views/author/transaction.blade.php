@extends('layouts.backend.app')

@section('title','Order List')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Completed orders
                            <span class="badge bg-blue">{{ $shop->transactions->count() }}</span>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Customer Name</th>
                                    <th>Item List</th>
                                    <th>Total Quantity</th>
                                    <th>Total Price</th>
                                    <th>Charge</th>
                                    <th>Pay</th>
                                    <th>Address At</th>
                                    <th>Driver name</th>
                                    <th>Status</th>

                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>SL</th>
                                    <th>Customer Name</th>
                                    <th>Item List</th>
                                    <th>Total Quantity</th>
                                    <th>Total Price</th>
                                    <th>Charge</th>
                                    <th>Pay</th>
                                    <th>Address At</th>
                                    <th>Driver name</th>
                                    <th>Status</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                @foreach($shop->transactions as $key=>$transaction)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $transaction->user->name }}</td>
                                        <td>{{ $transaction->item_list }}</td>
                                        <td>{{ $transaction->total_quantity }}</td>
                                        <td>{{ $transaction->total_price }} kr.</td>
                                        <td>{{ $transaction->tax }}%</td>
                                        <td>{{ $transaction->total_price-($transaction->total_price*($transaction->tax/100)) }} kr.</td>
                                        <td>
                                            {{\App\Shippingaddress::find($transaction->shippingaddress_id)->road_name_or_number}}<br>
                                            {{\App\Shippingaddress::find($transaction->shippingaddress_id)->apartment_or_suite}}<br>
                                            {{\App\Shippingaddress::find($transaction->shippingaddress_id)->city->name.'-'.\App\Shippingaddress::find($transaction->shippingaddress_id)->zip}}
                                        </td>
                                        <td class="text-center">
                                            <a href="#">{{ $transaction->driver->user->name }}</a>
                                        </td>
                                        <td>
                                            <p style="color: green">Delivared</p>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/backend/js/pages/tables/jquery-datatable.js') }}"></script>
@endpush
