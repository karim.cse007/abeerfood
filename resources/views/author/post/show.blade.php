@extends('layouts.backend.app')

@section('title','Create')

@push('css')

@endpush


@section('content')
    <div class="container-fluid">

        <!-- Vertical Layout | With Floating Label -->
        <a href="{{ route('author.post.index') }}" class="btn btn-danger waves-effect">Back</a>

        <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ $post->title }}
                                <small>Posted By <strong><a href="">{{ $post->user->name }}</a> </strong>on {{ $post->created_at->toFormattedDateString() }}</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label>Quantity</label><br>
                                    {{ $post->quantity }}
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label>Price</label><br>
                                    {{ $post->price }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Short description about food</label><br>
                                {{ $post->about }}
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h2>
                                Category
                            </h2>
                        </div>
                        <div class="body">
                            <span class="label bg-cyan">{{ $post->category->name }}</span>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-cyan">
                            <h2>
                                Days in avail able
                            </h2>
                        </div>
                        <div class="body">
                            @foreach($post->days  as $day)
                                <span class="label bg-cyan">
                                    {{ $day->name }}
                                </span>
                            @endforeach
                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-amber">
                            <h2>
                                Featured Image
                            </h2>
                        </div>
                        <div class="body">
                            <img class="img-responsive thumbnail" src="{{ asset('public/storage/post/'.$post->image) }}" alt="">
                        </div>
                    </div>
                </div>
        </div>
        <!-- Vertical Layout | With Floating Label -->
    </div>

@endsection

@push('js')

@endpush
