@extends('layouts.backend.app')

@section('title','Create')

@push('css')
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush


@section('content')
    <div class="container-fluid">

        <!-- Vertical Layout | With Floating Label -->
        <div class="row clearfix">
            <form action="{{ route('author.post.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Add new Post
                            </h2>
                        </div>
                        <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="title" class="form-control" name="title">
                                        <label class="form-label">Food Name*</label>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="float" id="price" class="form-control" name="price">
                                        <label class="form-label">Price per Quantity*</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Upload Images(Optional)</label>
                                    <input type="file" name="image">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">About Food</label>
                                    <textarea class="form-control" id="about" rows="5" name="about" style="outline: none !important;border-color: #719ECE;box-shadow: 0 0 10px #719ECE;"> </textarea>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Categories
                            </h2>
                        </div>
                        <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label for="category">Categories*</label>
                                        <select name="category" id="category" class="form-control show-tick" data-live-search="true">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">
                                                    {{ $category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label for="category">Days*</label>
                                        <select name="days[]" id="days" class="form-control show-tick" data-live-search="true" multiple>
                                            @foreach($days as $day)
                                                <option value="{{ $day->id }}">
                                                    {{ $day->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <a class="btn btn-danger m-t-15 waves-effect" href="{{ route('author.post.index') }}">Back</a>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- Vertical Layout | With Floating Label -->
    </div>

@endsection

@push('js')
    <!-- Select Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
@endpush
