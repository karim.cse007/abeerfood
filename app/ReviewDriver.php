<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewDriver extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function driver(){
        return $this->belongsTo('App\Driver');
    }
}
