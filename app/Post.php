<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
    public function days(){
        return $this->belongsToMany('App\Day')->withTimestamps();
    }
    public function scopeIsAvailable($query)
    {
        return $query->where('available',1);
    }
    public function scopePostCount($query)
    {
        return $query->count();
    }
    public function scopeUserPostCount($query)
    {
        return $query->where('user_id',Auth::id())->count();
    }
    public function scopePostViewCount($query)
    {
        return $query->sum('view_count');
    }
}
