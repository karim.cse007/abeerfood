<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function drivers(){
        return $this->hasMany('App\Driver');
    }
    public function areas(){
        return $this->hasMany('App\Area');
    }
}
