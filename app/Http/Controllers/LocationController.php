<?php

namespace App\Http\Controllers;

use App\Shop;
use Illuminate\Http\Request;
use App\Area;
use Illuminate\Support\Facades\Session;
use Torann\GeoIP\Facades\GeoIP;

class LocationController extends Controller
{
    public function index(Request $request){
        //dd($request);
        //$loc = \Location::get('116.58.201.15');
        //dd($loc);
        $location = GeoIP::getLocation();

        //dd($location);
        $country = $location['country'];
        $city = $location['city'];
        $state = $location['state'];
        $state_name= $location['state_name'];
        $postal_code= $location['postal_code'];
        $lat = $location['lat'];
        $lng = $location['lon'];
        //$ip = request()->ip();
        //$data = \Location::get($ip);
        $loc = $state_name.','.$state.','.$city.'-'.$postal_code;
        $locat =array(
            'loc' => $loc,
            'lat' => $lat,
            'long' => $lng,
        );
        return $locat;
    }

    public function maps(){
        $shops = Shop::where('is_approved',1)->where('is_blocked',0)->get();
        if ($shops->count() >7) {
            $shops = Shop::latest(8)->IsApproved()->IsBlocked()->get();
        }
        $areas = Area::all();
        return view('maps',compact('shops','areas'));
    }
}
