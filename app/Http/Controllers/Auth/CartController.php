<?php

namespace App\Http\Controllers\Auth;

use App\Order;
use App\Shippingaddress;
use App\User;
use http\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
class CartController extends Controller
{
        public function store(Request $request){//stripe payment section
            $ship =Shippingaddress::where('user_id',Auth::id())->latest('created_at')->first();
            $cartCollections = \Cart::getContent();
            if ($ship && $cartCollections){
                foreach ($cartCollections as $key=>$cart){
                    $shop_id = $cart->attributes->shop_id;
                    $items[$key] = $cart->name .' ('. $cart->quantity.')=>'.$cart->price*$cart->quantity;
                }
                $cartTotalQuantity = \Cart::getTotalQuantity();
                $total = \Cart::getTotal() + Auth::user()->tempDistance->charge;

                \Stripe\Stripe::setApiKey('sk_test_NB4cy4WmXgZvn5ztIE2Qf0aV00fp0GkPFH');
                $charge = \Stripe\Charge::create([
                    'amount' => $total * 100,
                    'currency' => 'SEK',
                    'source' => $request->stripeToken,
                    'receipt_email' => $ship->email,
                    'description' => 'Abeer food order',
                ]);
                if ($charge && $charge->paid == true){
                    $order = new Order();
                    $order->user_id = Auth::id();
                    $order->shop_id = $shop_id;
                    $order->item_list = implode(" ",$items);
                    $order->total_quantity = $cartTotalQuantity;
                    $order->total_price = $charge->amount/100;
                    $order->delivery_charge = Auth::user()->tempDistance->charge;
                    $order->payment_id = 2;
                    $order->pay_id = $charge->id;
                    $order->transaction_or_payer_id = $charge->balance_transaction;
                    $order->receipt_url = $charge->receipt_url;
                    $order->shippingaddress_id = $ship->id;
                    $order->save();
                    \Cart::clear();
                    session()->put('shipping',false);//this condition for payment option disable
                    $distance = Auth::user()->tempDistance;
                    $distance->delete();
                    return redirect(route('customer.checkout.thanks'));
                }else{
                    return $this->cancel();
                }
            }
            return redirect('/');

        }

        public function paypal(Request $request){
            $ship =Shippingaddress::where('user_id',Auth::id())->latest('created_at')->first();
            $cartCollections = \Cart::getContent();
            if ($ship && $cartCollections){
                $apiContext = new \PayPal\Rest\ApiContext(
                    new \PayPal\Auth\OAuthTokenCredential(
                        env('PAYPAL_CLIENT_ID'),
                        env('PAYPAL_CLIENT_SECRET')
                    )
                );
                // Create new payer and method
                $payer = new Payer();
                $payer->setPaymentMethod("paypal");

                // Set redirect URLs
                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(route('paypal.process'))
                    ->setCancelUrl(route('paypal.cancel'));
                $total = \Cart::getTotal()+ Auth::user()->tempDistance->charge;//charge means delivery charge
                // Set payment amount
                $amount = new Amount();
                $amount->setCurrency("SEK")
                    ->setTotal($total);

                // Set transaction object
                $transaction = new Transaction();
                $transaction->setAmount($amount)
                    ->setDescription("Abeer food");

                // Create the full payment object
                $payment = new Payment();
                $payment->setIntent('sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirectUrls)
                    ->setTransactions(array($transaction));

                // Create payment with valid API context
                try {
                    $payment->create($apiContext);

                    // Get PayPal redirect URL and redirect the customer
                    $approvalUrl = $payment->getApprovalLink();
                    //customer data needed
                    return redirect($approvalUrl);
                    // Redirect the customer to $approvalUrl
                } catch (PayPal\Exception\PayPalConnectionException $ex) {
                    echo $ex->getCode();
                    echo $ex->getData();
                    die($ex);
                } catch (Exception $ex) {
                    die($ex);
                }
            }

        }

        public function process(Request $request){

            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    env('PAYPAL_CLIENT_ID'),
                    env('PAYPAL_CLIENT_SECRET')
                )
            );
            // Get payment object by passing paymentId
            $paymentId = $request->paymentId;
            $payment = Payment::get($paymentId, $apiContext);
            $payerId = $request->PayerID;
            // Execute payment with payer ID
            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);

            try {
                $ship = User::find(Auth::id())->shippingaddress;
                $cartCollections = \Cart::getContent();
                if ($ship && $cartCollections){
                    // Execute payment
                    $result = $payment->execute($execution, $apiContext);
                    //dd($result);
                }

                if (isset($result) && strtolower($result->state) == 'approved'){
                    $shop_id='';
                    $items = [];
                    foreach ($cartCollections as $key=>$cart){
                        $shop_id = $cart->attributes->shop_id;
                        $items[$key] = $cart->name .' ('. $cart->quantity.')=>'.$cart->price*$cart->quantity;
                    }
                    $cartTotalQuantity = \Cart::getTotalQuantity();
                    $total = \Cart::getTotal();

                    $order = new Order();
                    $order->user_id = Auth::id();
                    $order->shop_id = $shop_id;
                    $order->item_list = implode(" ",$items);
                    $order->total_quantity = $cartTotalQuantity;
                    $order->total_price = $total;
                    $order->delivery_charge = Auth::user()->tempDistance->charge;
                    $order->payment_id = 1;
                    $order->pay_id = $result->id;
                    $order->transaction_or_payer_id = $result->payer->payer_info->payer_id;
                    $order->shippingaddress_id = $ship->id;
                    $order->save();
                    \Cart::clear();
                    session()->put('shipping',false);//this condition for payment option disable
                    $distance = Auth::user()->tempDistance;
                    $distance->delete();
                    return redirect(route('customer.checkout.thanks'));
                }
            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                echo $ex->getCode();
                echo $ex->getData();
                die($ex);
            } catch (Exception $ex) {
                die($ex);
            }
        }
        public function cancel(Request $request){
            echo "You request is cancel because invalid credential";
        }
}
