<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo='/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
     /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
     public function redirectToProvider($facebook){
        return Socialite::driver($facebook)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($facebook){
        $userinfo = Socialite::driver($facebook)->user();
        //dd($userinfo);
        $finduser = User::where('email',$userinfo->email)->where('password',$userinfo->id)->first();
        if($finduser){
            $user = $finduser;
        }else{
            $user = new User();
            $user->name = $userinfo->name;
            $user->role_id = 4;
            $user->email = $userinfo->email;
            $user->password = $userinfo->id;
            $user->image = $userinfo->avatar;
            $user->save();
        }
        Auth::login($user);
        return redirect($this->redirectTo);

    }

}
