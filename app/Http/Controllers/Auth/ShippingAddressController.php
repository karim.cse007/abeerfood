<?php

namespace App\Http\Controllers\Auth;

use App\Shippingaddress;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ShippingAddressController extends Controller
{
    public function store(Request $request){

        $this->validation($request->all())->validate();
            $user = User::findOrFail(Auth::id());
            if ($user){
                $shippingaddress = new Shippingaddress();
                $shippingaddress->user_id = Auth::id();
                $shippingaddress->name = $request->name;
                $shippingaddress->phone_number = $request->phone;
                $shippingaddress->road_name_or_number = $request->road_name;
                $shippingaddress->house_no = $request->house_no;
                $shippingaddress->door_code = $request->door_code;
                $shippingaddress->floor = $request->floor;
                $shippingaddress->apartment_or_suite = $request->apartment;
                $shippingaddress->door_name = $request->door_name;
                $shippingaddress->location = $request->location;
                $shippingaddress->lat = $request->lat;
                $shippingaddress->long = $request->long;
                $shippingaddress->city_id = $request->city;
                $shippingaddress->zip = $request->zip;
                $shippingaddress->save();
                session()->put('shipping',true);//this condition for payment option show
                return redirect(url()->previous().'#payment-info');
            }
            return redirect()->back();
    }
    public function edit(Request $request){
        session()->put('shipping',false);//for shipping edit permission
    }

    protected function validation(array $data){
        session()->put('shipping',false);//this condition for payment option dont show
        return Validator::make($data,[
            'location' => ['required','string','max:191'],
            'lat' => ['required'],
            'long' => ['required'],
            'name' => ['required','string','max:191'],
            'phone' => ['required','string','max:191'],
            'road_name' => ['required','string','max:100'],
            'house_no' => ['required','string','max:100'],
            'door_code' => ['required','string','max:100'],
            'floor' => ['required','string','max:100'],
            'apartment' => ['required','string','max:100'],
            'door_name' => ['required','string','max:100'],
            'city' => ['required','integer'],
            'zip' => ['required','integer'],
        ]);
    }
}
