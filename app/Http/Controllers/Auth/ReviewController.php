<?php

namespace App\Http\Controllers\Auth;

use App\Review;
use App\Shop;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function store(Request $request, $shop_id){
        $this->validate($request,[
            'review' =>'required'
        ]);

        $review = new Review();
        $review->shop_id = $shop_id;
        $review->user_id = Auth::id();
        $review->review = $request->review;
        $review->save();

        //Toastr::success('Review successfully Publish :)','Success');
        return redirect(url()->previous().'#pills-reviews')->with('s_review','Thank you for your comment');
    }
    public function loadReviewPage($id){
        $shop = Shop::findOrFail($id);
        $reviews = Review::latest()->where('shop_id',$shop->id)->get();
        return view('reviewshop',compact('reviews'));
    }
}
