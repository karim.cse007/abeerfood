<?php

namespace App\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function add($id){
        $user = Auth::user();
        $isFavorite = $user->favorite_shops()->where('shop_id',$id)->count();
        if ($isFavorite == 0)
        {
            $user->favorite_shops()->attach($id);
            Toastr::success('Post successfully added to your favorite list :)','Success');
            return redirect()->back();
        } else {
            $user->favorite_shops()->detach($id);
            Toastr::success('Post successfully removed form your favorite list :)','Success');
            return redirect()->back();
        }
    }
}
