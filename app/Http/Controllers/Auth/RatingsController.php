<?php

namespace App\Http\Controllers\Auth;

use App\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RatingsController extends Controller
{
    public function addnewrate(Request $request)
    {
        //dd($request);
        $this->validate($request,[
           'rate'=>'required|integer|not_in:0',
           'shop_id'=>'required|integer|not_in:0"exists:shops,id',
        ]);
        $user = Auth::user();
        $exists = Rating::where('user_id',$user->id)
                        ->where('shop_id',$request->shop_id)
                        ->first();
        if (isset($exists->id)){
            $exists->rating = $request->rate;
            $exists->save();
            $data=[
              'success'=> "Your rated $request->rate.0",
            ];
            return response()->json($data,200);
        }else{
            $rating = new Rating();
            $rating->user_id = $user->id;
            $rating->shop_id = $request->shop_id;
            $rating->rating =$request->rate;
            $rating->save();
            $data=[
                'success'=> "Your rated $request->rate.0",
            ];
            return response()->json($data,200);
        }
    }
}
