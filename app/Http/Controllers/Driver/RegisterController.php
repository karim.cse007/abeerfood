<?php

namespace App\Http\Controllers\Driver;

use App\Area;
use App\City;
use App\Driver;
use App\Jobs\RiderRegisterJob;
use App\Smartphone;
use App\User;
use App\Vehicle;
use App\Workinghour;
use App\WorkingType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function driverbasic(){

        return view('register.basicdriver');
    }
    public function driveradd(){
        $cities = City::all();
        $areas = Area::all();
        $vehicles = Vehicle::all();
        $working_types = WorkingType::all();
        $working_hours = Workinghour::all();
        $smartphones = Smartphone::all();
        return view('register.adddriver',compact('cities','areas','vehicles','working_types','working_hours','smartphones'));
    }
    public function store(Request $request){

        $regifailcheck= User::where('email',$request->email)->where('role_id',3)->first();
        if ($regifailcheck and $regifailcheck->driver == null){
            $regifailcheck->delete();
        }
        $this->validation($request->all())->validate();
        $user = new User();
        $user->role_id=3;
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->save();
        $user = User::where('email',$request->email)->first();

        if ($user){
            $driver = new Driver();
            $driver->user_id = $user->id;
            $driver->social_security = $request->ssecurity;
            $driver->city_id = $request->city;
            $driver->area_id = $request->area;
            $driver->vehicle_id = $request->vehicle;
            $driver->working_type_id = $request->w_during;
            $driver->workinghour_id = $request->w_hours;
            $driver->smartphone_id = $request->phone_type;
            $driver->save();
            RiderRegisterJob::dispatch($user)->delay(now()->addSeconds(20));
            return redirect(route('registration.thanks'));
        }
        return redirect('/');
    }

    protected function validation(array $data){
        return Validator::make($data,[
            'name' => ['required','string','max:191'],
            'email' => ['required','email','unique:users','max:50'],
            'phone_number' => ['required','string','unique:users','max:191'],
            'ssecurity' => ['required','string','unique:drivers,social_security','max:11'],
            'city' => ['required','integer','not_in:0','exists:cities,id'],
            'area' => ['required','integer','not_in:0','exists:areas,id'],
            'vehicle' => ['required','integer','not_in:0','exists:vehicles,id'],
            'w_during' => ['required','integer','not_in:0','exists:working_types,id'],
            'w_hours' => ['required','integer','not_in:0','exists:workinghours,id'],
            'phone_type' => ['required','integer','not_in:0','exists:smartphones,id'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
}
