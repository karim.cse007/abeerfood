<?php

namespace App\Http\Controllers\Driver;

use App\CashoutRequestDriver;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function cashout(Request $request){
        //dd($request);
        $user = User::findOrFail(Auth::id());
        if($user){

            if(Auth::user()->role->id == 3){
                $user = Auth::user();
                if($request->isMethod('PUT')){

                    $this->validate($request,[
                        'reqbalance' => 'required|integer|not_in:0',
                    ]);

                    $bl1 = $user->driver->account->sum('amount'); //this is total balance
                    $bl2 = $user->driver->cashoutdriver->sum('amount'); //this is already cashout balance
                    $bl3 = $user->driver->cashoutrequestdriver->sum('amount'); //this is pending cashout balance
                    $bl = $bl1-($bl2+$bl3);
                    //dd($request);
                    if($request->reqbalance <= $bl){
                        $cashoutRequest = new CashoutRequestDriver();
                        $cashoutRequest->driver_id = $user->driver->id;
                        $cashoutRequest->user_id = $user->id;
                        $cashoutRequest->amount = $request->reqbalance;
                        $cashoutRequest->save();
                        $msg = "<span style='color:green;'>Success !<strong> Your cashout request is sent</strong></span>";
                        return $msg;
                    }else{
                        $msg ="<span style='color:red;'>Error! <strong>Not enoung balance to cashout</strong></span>";
                        return $msg;
                    }
                }
            }
        }

    }
}
