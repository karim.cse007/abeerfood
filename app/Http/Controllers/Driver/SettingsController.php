<?php

namespace App\Http\Controllers\Driver;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function updateProfile(Request $request){
        if(Auth::user()->role->id == 3){
            $user = User::findOrFail(Auth::id());
            if($user){
                if($request->isMethod('POST')){
                    //dd($request);
                    $this->validate($request,[
                        'name' => 'required|string|max:191',
                        'phone_number' => 'required|string|max:20',
                        'addressl1' => 'required|string|max:191',
                        'city' => 'required|string|max:100',
                        'post_code' => 'required|string|max:100',
                    ]);
                    //dd($request);
                    $user->name = $request->name;
                    $user->phone_number = $request->phone_number;
                    $user->addressl1 = $request->addressl1;
                    $user->addressl2 = $request->addressl2;
                    $user->city = $request->city;
                    $user->post_code = $request->post_code;
                    $user->save();
                    $msg = "<span style='color:green;'>Success !<strong> Succesfuly change Information</strong></span>";
                    return $msg;
                }
            }
        }
    }

    public function updatePassword( Request $request){

        if(Auth::user()->role->id == 3){
            $user = User::findOrFail(Auth::id());
            if($user){
                if($request->isMethod('POST')){
                    //dd($request);
                    $this->validate($request,[
                        'old_password' => 'required',
                        'password' => 'required|confirmed|min:6',
                    ]);
                    $hashpassword = Auth::user()->password;

                    if(Hash::check($request->old_password, $hashpassword)){
                        if (!Hash::check($request->password,$hashpassword)){
                            $user->password = Hash::make($request->password);
                            $user->save();
                            $msg = "<span style='color:green;'>Success !<strong> Succesfuly change the password</strong></span>";
                            return $msg;

                        }else{
                            $msg ="<span style='color:red;'>Error! <strong>New password cannot be the same as old password</strong></span>";
                            return $msg;
                        }
                    }else{
                        $msg ="<span style='color:red;'>Error! <strong>old password not match!!</strong></span>";
                        return $msg;
                    }
                }
            }
        }
    }
}
