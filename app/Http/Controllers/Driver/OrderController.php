<?php

namespace App\Http\Controllers\Driver;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function deliver(Request $request){
        $order = Order::findOrFail($request->order_id);
        if ($order){
            if ($order->driver_id == Auth::user()->driver->id and $order->is_approved == 1 and $order->is_cooked==2){
                $order->is_delivered = 1;
                $order->save();
                return redirect(route('driver.dashboard').'#order-history');
            }
        }
        return redirect(route('driver.dashboard').'#my-orders');
    }
}
