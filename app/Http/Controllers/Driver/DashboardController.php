<?php

namespace App\Http\Controllers\Driver;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(){
        $user = User::findOrFail(Auth::id());
        if($user){
            if(Auth::user()->role->id == 3){
                //dd(Auth::user()->driver);
                $driver = Auth::user()->driver;
                if($driver){
                    $orders = $driver->orders->where('is_delivered',0)->where('is_approved',1);
                    $ordershistory = $driver->orders->where('is_delivered',1);
                    $reviews = $driver->reviews;
                    if($user){
                        return view('driver.profile',compact('user','orders','ordershistory','reviews'));
                    }
                }
            }
        }
        return redirect()->back();
    }
}
