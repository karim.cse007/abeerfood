<?php

namespace App\Http\Controllers\Author;

use App\Order;
use App\Shippingaddress;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function index(){
        if (Auth::user()->role->id == '2'){
            $shop = User::find(Auth::id())->shop;
            if($shop){
                return view('author.transaction',compact('shop'));
            }
        }
        Auth::logout();
        return redirect()->back();
    }
}
