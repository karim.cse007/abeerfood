<?php

namespace App\Http\Controllers\Author;

use App\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index()
    {
        if (!Session::get('notify')){
            Session::put('notify',0);
        }
        if(Auth::user()->role_id == 2){
            $shop = Shop::where('user_id',Auth::id())->first();
            if($shop){
                return view('author.dashboard',compact('shop'));
            }
            return back();
        }return redirect()->back();

    }
}
