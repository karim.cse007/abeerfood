<?php

namespace App\Http\Controllers\Author;

use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function makeSeenOrder(Request $request){
        $orders = Order::where('created_at','>=',Carbon::today())->where('is_approved',1)->where('is_view_shop',0)->get();
        if ($orders){
            foreach ($orders as $order){
                $order->is_view_shop=true;
                $order->save();
            }
        }
    }
}
