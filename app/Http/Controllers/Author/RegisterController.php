<?php

namespace App\Http\Controllers\Author;

use App\Area;
use App\City;
use App\Jobs\ShopRegisterJob;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function restaurantbasic(){

        return view('register.basicresto');
    }
    public function restaurantadd(){
        $cities = City::all();
        $areas = Area::all();
        return view('register.addrestaurant',compact('cities','areas'));
    }
    public function store(Request $request){
        $regifailcheck= User::where('email',$request->email)->where('role_id',2)->first();
        if ($regifailcheck and $regifailcheck->shop == null){
            $regifailcheck->delete();
        }
        $this->validation($request->all())->validate();
        $user = new User();
        $user->role_id=2;
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->save();
        $user = User::where('email',$request->email)->first();
        if ($user){
            //$currentDate = Carbon::now()->toDateString();
            $slug = str_slug($request->shop_name).'-'.uniqid();
            $shop = new Shop();
            $shop->user_id = $user->id;
            $shop->shop_name = $request->shop_name;
            $shop->slug = $slug;
            $shop->phone_number = $request->shop_phone;
            $shop->trade_license = $request->license;
            $shop->open_at = date("G:i", strtotime($request->open_at));
            $shop->close_at = date("G:i", strtotime($request->close_at));
            $shop->city_id = $request->city;
            $shop->area_id = $request->area;
            $shop->location = $request->location;
            $shop->lat = $request->lat;
            $shop->long = $request->long;
            $shop->save();
            ShopRegisterJob::dispatch($user)->delay(now()->addSeconds(20));
            return redirect(route('registration.thanks'));
        }
        return redirect()->back();
    }
    protected function validation(array $data){
        return Validator::make($data,[
            'name' => ['required','string','max:191'],
            'email' => ['required','email','unique:users','max:50'],
            'phone_number' => ['required','string','unique:users','max:191'],
            'shop_name' => ['required','string','max:191'],
            'shop_phone' => ['required','string','max:20'],
            'license' => ['required','string','unique:shops,trade_license','max:191'],
            'city' => ['required','integer','not_in:0','exists:cities,id'],
            'area' => ['required','integer','not_in:0','exists:areas,id'],
            'location' => ['required','string'],
            'lat' => ['required'],
            'long' => ['required'],
            'open_at' => ['required','string'],
            'close_at' => ['required','string'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
}
