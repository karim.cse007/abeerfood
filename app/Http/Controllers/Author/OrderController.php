<?php

namespace App\Http\Controllers\Author;

use App\Order;
use App\Shop;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    public function index(){
        if (Auth::user()->role->id == '2'){
            $shop = User::find(Auth::id())->shop;
            if($shop){
                $orders = $shop->orders->where('is_approved','1');
                return view('author.order',compact('orders'));
            }
        }
        Auth::logout();
        return redirect()->back();

    }
    public function cookingReady(Request $request){
        $this->validate($request, [
            'order_id' => 'required|not_in:0|exists:orders,id',
        ]);
        $order = Order::findOrFail($request->order_id);
        $condition = $order->where('is_approved',1)
                            ->where('is_cooked', '<' ,1)
                            ->where('is_delivered', 0)
                            ->first();
        if ($order){
            if (Auth::user()->shop->id == $order->shop->id and $condition) {
                $order->is_cooked = 1;
                $order->save();
                Toastr::success('succesfully change status)', 'Succes');
                return redirect()->back();
            }else{
                Toastr::error('You dont have permisson to access this','Error');
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
    public function cookingDeliver(Request $request){
        $this->validate($request, [
            'order_id' => 'required|not_in:0|exists:orders,id',
        ]);
        $order = Order::findOrFail($request->order_id);
        if ($order->driver_id == null){
            Toastr::error('Driver not assign yet','Error');
            return redirect()->back();
        }
        $condition = $order->where('is_approved',1)
            ->where('is_cooked', '=' ,1)
            ->where('is_delivered', 0)
            ->where('driver_id', '!=' ,null)
            ->first();
        if ($order){
            if (Auth::user()->shop->id == $order->shop->id and $condition) {
                $order->is_cooked = 2;
                $order->save();
                Toastr::success('Deliver success :)', 'Succes');
                return redirect()->back();
            }else{
                Toastr::error('You dont have permisson to access this','Error');
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
}
