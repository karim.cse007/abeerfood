<?php

namespace App\Http\Controllers\Author;

use App\Category;
use App\Day;
use App\Post;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Auth::user()->posts()->latest()->get();
        return view('author.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role->id == '2') {
            $shop = User::find(Auth::id())->shop;
            if ($shop) {
                $categories = Category::all()->where('shop_id',$shop->id);
                $days = Day::all();
                //dd(Carbon::now()->dayOfWeek);
                return view('author.post.create', compact('categories','days'));
            }
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request,[
            'category' => 'required|exists:categories,id',
            'title' => 'required',
            'about' =>'required',
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'price' => 'required|integer',
            'days' => 'required|exists:days,id',
        ]);

        //get from image
        $image = $request->file('image');
        //make current date and time
        $currentDate = Carbon::now()->toDateString();
        $slug = str_slug($request->title).'-'.$currentDate.'-'.uniqid();
        if (Auth::user()->role->id == 2 && User::find(Auth::id())->shop->user_id == Auth::id()){
            if(isset($image)){

                $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

                //check category dir is exits
                if (!Storage::disk('public')->exists('post')){
                    Storage::disk('public')->makeDirectory('post');
                }

                //resize images for category and upload
                $post = Image::make($image)->resize(800,600)->save();
                Storage::disk('public')->put('post/'.$imagename,$post);
            }else{
                $imagename = 'default.png';
            }
            //get user id
            $user = Auth::id();
            $shopId = User::find(Auth::id())->shop->id;
            $posts = new Post();
            $posts->user_id = $user;
            $posts->category_id = $request->category;
            $posts->shop_id = $shopId;
            $posts->title = $request->title;
            $posts->slug = $slug;
            $posts->about = $request->about;
            $posts->image = $imagename;
            $posts->quantity = 1;
            $posts->price = $request->price;
            $posts->available = true;
            $posts->save();
            $posts->days()->attach($request->days);
            Toastr::success('succesfully added :)','Succes');
            return redirect()->route('author.post.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        if ($post->user_id == Auth::id()){
            return view('author.post.show',compact('post'));
        }else{
            Toastr::error('You dont have permisson to access this','Error');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if ($post->user_id == Auth::id()){
            $categories = Category::all();
            $days = Day::all();
            return view('author.post.edit',compact('post','categories','days'));
        }else{
            Toastr::error('You dont have permisson to access this','Error');
            return redirect()->back();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if ($post->user_id == Auth::id()) {
            $this->validate($request, [
                'category' => 'required|exists:categories,id',
                'title' => 'required',
                'about' => 'required',
                'image' => 'image',
                'price' => 'required|integer',
                'days' => 'required|exists:days,id',
            ]);
            //get from image
            $image = $request->file('image');
            //make current date and time
            $currentDate = Carbon::now()->toDateString();

            $slug = str_slug($request->title) . '-' . $currentDate;
            if (isset($image)) {

                $imagename = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

                //check category dir is exits
                if (!Storage::disk('public')->exists('post')) {
                    Storage::disk('public')->makeDirectory('post');
                }

                ///delete image if exist
                if($post->image != 'default.png'){
                    if (Storage::disk('public')->exists('post/' . $post->image)) {
                        Storage::disk('public')->delete('post/' . $post->image);
                    }
                }
                //resize images for category and upload
                $postImg = Image::make($image)->resize(800, 600)->save();
                Storage::disk('public')->put('post/' . $imagename, $postImg);
            } else {
                $imagename = $post->image;
            }
            //get user id
            $user = Auth::id();

            $post->user_id = $user;
            $post->category_id = $request->category;
            $post->title = $request->title;
            $post->slug = $slug;
            $post->about = $request->about;
            $post->image = $imagename;
            $post->quantity = 1;
            $post->price = $request->price;
            $post->available = true;
            $post->save();
            $post->days()->sync($request->days);
            Toastr::success('Post succesfully Updated :)', 'Succes');
            return redirect()->route('author.post.index');
        }else{
            Toastr::error('You dont have permisson to access this','Error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if ($post->user_id == Auth::id()) {
            if ($post->image != 'default.png'){
                if (Storage::disk('public')->exists('post/' . $post->image)) {
                    Storage::disk('public')->delete('post/' . $post->image);
                }
            }
            $post->delete();
            $post->days()->detach();
            Toastr::success('Delete succesfully )', 'Succes');
            return redirect()->route('author.post.index');
        }else{
            Toastr::error('You dont have permisson to access this','Error');
            return redirect()->back();
        }
    }
}
