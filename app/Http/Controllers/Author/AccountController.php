<?php

namespace App\Http\Controllers\Author;

use App\CashoutRequestShop;
use App\Shop;
use App\ShopownerAccount;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function index(){
        if(Auth::user()->role_id == 2){
            $shop = Auth::user()->shop;
            if(isset($shop->id)){
                $totalBalance = $shop->shopowneraccount->sum('amount');
                $totalCashout= $shop->cashoutshop->sum('amount');
                $totalCashoutRequest= $shop->cashoutrequestshop->sum('amount');
                $currentBalance= $totalBalance - ($totalCashout + $totalCashoutRequest);

                return view('author.accountinfo',compact('shop','totalBalance','totalCashout','totalCashoutRequest','currentBalance'));
            }
            return back();
        }return redirect()->back();
    }
    public function cashout(Request $request){
        $user = User::findOrFail(Auth::id());
        if($user){
            $shop = Auth::user()->shop;
            //dd($shop);
            if($user->role_id == '2' and $shop){
                //dd($request);
                if($request->isMethod('PUT')){

                    $this->validate($request,[
                        'reqbalance' => 'required|integer|not_in:0',
                    ]);

                    $bl1 = $shop->shopowneraccount->sum('amount'); //this is total balance
                    $bl2 = $shop->cashoutshop->sum('amount'); //this is already cashout balance
                    $bl3 = $shop->cashoutrequestshop->sum('amount'); //this is pending cashout balance
                    $bl = $bl1-($bl2+$bl3);
                    //dd($request);
                    if($request->reqbalance <= $bl){
                        $cashoutRequest = new CashoutRequestShop();
                        $cashoutRequest->shop_id = $shop->id;
                        $cashoutRequest->user_id = $shop->user_id;
                        $cashoutRequest->amount = $request->reqbalance;
                        $cashoutRequest->save();
                        $msg = "<span style='color:green;'>Success !<strong> Your cashout request is sent</strong></span>";
                        return $msg;
                    }else{
                        $msg ="<span style='color:red;'>Error! <strong>Not enoung balance to cashout</strong></span>";
                        return $msg;
                    }
                }
            }
        }

    }
}
