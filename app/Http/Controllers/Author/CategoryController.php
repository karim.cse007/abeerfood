<?php

namespace App\Http\Controllers\Author;

use App\Category;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Auth::user()->shop->categories;
        return view('author.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shop = Auth::user()->shop;
        if ($shop) {
            return view('author.category.create');
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg',
        ]);
        $exist = Category::all()->where('name',$request->name)->where('shop_id',Auth::user()->shop->id)->first();
        if ($exist){
            Toastr::error('Category already exists)','Error');
            return redirect()->back();
        }
        //get from image
        $image = $request->file('image');
        $slug = str_slug($request->name);
        if(isset($image)){
            //make current date and time
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            //check category dir is exits
            if (!Storage::disk('public')->exists('category')){
                Storage::disk('public')->makeDirectory('category');
            }

            //resize images for category and upload
            $category = Image::make($image)->resize(1600,479)->save();
            Storage::disk('public')->put('category/'.$imagename,$category);
        }else{
            $imagename = 'default.png';
        }
        $categories = new Category();
        $categories->shop_id = Auth::user()->shop->id;
        $categories->name = $request->name;
        $categories->slug = $slug;
        $categories->image = $imagename;
        $categories->save();
        Toastr::success('successfully added :)','Success');
        return redirect()->route('author.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all()->where('id',$id)->where('shop_id',Auth::user()->shop->id)->first();
        if ($category){
            return view('author.category.edit',compact('category'));
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'name' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg'
        ]);
        //dd($request);
        $category = Category::all()->where('id',$id)->where('shop_id',Auth::user()->shop->id)->first();
        if (!$category){
            Toastr::error('Permission denied','Error');
            return redirect()->back();
        }

        //get from image
        $image = $request->file('image');
        $slug = str_slug($request->name).'-'.uniqid();
        if(isset($image)){
            //make current date and time
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            //check category dir is exits
            if (!Storage::disk('public')->exists('category')){
                Storage::disk('public')->makeDirectory('category');
            }
            ///delete image if exist
            if ($category->image != 'default.png'){
                if (Storage::disk('public')->exists('category/'.$category->image)){
                    Storage::disk('public')->delete('category/'.$category->image);
                }
            }

            //resize images for category and upload
            $categoryImg = Image::make($image)->resize(1600,479)->save();
            Storage::disk('public')->put('category/'.$imagename,$categoryImg);

        }else{
            $imagename = $category->image;
        }
        $category->shop_id = Auth::user()->shop->id;
        $category->name = $request->name;
        $category->slug = $slug;
        $category->image = $imagename;
        $category->save();
        Toastr::success('Category successfully update :)','Success');
        return redirect()->route('author.category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::all()->where('id',$id)->where('shop_id',Auth::user()->shop->id)->first();
        if ($category){
            if ($category->posts->count() <1){
                if ($category->image != 'default.png'){
                    if(Storage::disk('public')->exists('category/'.$category->image)){
                        Storage::disk('public')->delete('category/'.$category->image);
                    }
                }
                $category->delete();
                Toastr::success('delete successful','Success');
                return redirect()->back();
            }else{
                Toastr::error('Cant delete this category.Because this already use.','Error');
                return redirect()->back();
            }
        }else{
            Toastr::error('Permission denied','Error');
            return redirect()->back();
        }
    }
}
