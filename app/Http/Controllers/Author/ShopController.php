<?php

namespace App\Http\Controllers\Author;

use App\Area;
use App\City;
use App\Shop;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ShopController extends Controller
{
    public function index(){
        $user = Auth::id();
        $shop = User::findOrFail($user)->shop;
        $cities = City::all();
        $areas = Area::all();
        if($shop){
            return view('author.shop',compact('shop','cities','areas'));
        }else{
            return redirect()->back();
        }

    }
    public function updateShop(Request $request){
        //dd($request);
        if(Auth::id() == User::find(Auth::id())->shop->user_id) {
            $this->validate($request, [
                'shop_name' => 'required|string|max:191',
                'phone_number' => 'required|max:20',
                'trade_license' => 'required|string',
                'city' => 'required|integer|not_in:0|exists:cities,id',
                'area' => 'required|integer|not_in:0|exists:areas,id',
                'location' => 'required|string',
                'lat' => 'required',
                'long' => 'required',
                'image' => 'mimes:jpeg,bmp,png,jpg',
                'icon' => 'mimes:jpeg,bmp,png,jpg',
            ]);

            $image = $request->file('image');
            $icon = $request->file('icon');
            $curentDate = Carbon::now()->toDateString();
            $slug = str_slug($request->shop_name). '-' . uniqid();
            $user = User::findOrFail(Auth::id())->shop;

            if (isset($image)) {
                $imageName = $slug . '-' . $curentDate .'.' . $image->getClientOriginalExtension();

                if (Storage::disk('public')->exists('shops')) {
                    Storage::disk('public')->makeDirectory('shops');
                }
                if($user->image != 'default.png'){
                    if (Storage::disk('public')->exists('shops/' . $user->image)) {
                        Storage::disk('public')->delete('shops/' . $user->image);
                    }
                }

                $profile = Image::make($image)->resize(1600, 800)->save();

                Storage::disk('public')->put('shops/' . $imageName, $profile);
            } else {
                $imageName = $user->image;
            }
            //this for icon update
            if (isset($icon)) {
                $iconName = $slug . '-' . $curentDate .'.' . $icon->getClientOriginalExtension();

                if (Storage::disk('public')->exists('shops/icons')) {
                    Storage::disk('public')->makeDirectory('shops/icons');
                }
                if($user->icon != 'default.jpg'){
                    if (Storage::disk('public')->exists('shops/icons/' . $user->icon)) {
                        Storage::disk('public')->delete('shops/icons/' . $user->icon);
                    }
                }

                $imgIcon = Image::make($icon)->resize(60, 60)->save();

                Storage::disk('public')->put('shops/icons/' . $iconName, $imgIcon);
            } else {
                $iconName = $user->icon;
            }

            $user->shop_name = $request->shop_name;
            $user->slug = $slug;
            $user->phone_number = $request->phone_number;
            $user->trade_license = $request->trade_license;
            $user->open_at = date("H:i",strtotime($request->open_at));
            $user->close_at = date("H:i",strtotime($request->close_at));
            $user->city_id = $request->city;
            $user->area_id = $request->area;
            $user->location = $request->location;
            $user->lat = $request->lat;
            $user->long = $request->long;
            $user->image = $imageName;
            $user->icon = $iconName;
            $user->save();

            Toastr::success('Update successfully', 'Success');
            return redirect()->back();
        }
    }
}
