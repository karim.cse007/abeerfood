<?php

namespace App\Http\Controllers\Author;

use App\Post;
use App\Review;
use App\Shop;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function index(){
        $shop = Auth::user()->shop;
        if ($shop){
            $reviews = Auth::user()->shop->reviews;
            if ($reviews){
                return view('author.reviews',compact('reviews'));
            }
        }
        return redirect()->back();

    }
}
