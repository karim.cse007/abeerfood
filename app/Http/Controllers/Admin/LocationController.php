<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use App\City;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LocationController extends Controller
{
    public function city(){
        $cities = City::all();
        return view('admin.location.cities',compact('cities'));
    }
    public function addcity(Request $request){

        $this->validate($request,[
                'name' =>'required|string|max:191',
            ]);
        $exists = City::where('name',$request->name)->first();
        if (isset($exists->name)){
            $msg = "<span style='color:red;'> <strong>Already exists!!!</strong></span>";
            return $msg;
        }else{
            $city = new City();
            $city->name = $request->name;
            $city->save();
            $msg = "<span style='color:green;'> <strong>Successfully added!!!</strong></span>";
            return $msg;
        }
    }
    public function updateCity(Request $request){
        //dd($request);
        $this->validate($request,[
                'update_name' =>'required|string|max:191',
                'city_id' =>'required|integer|not_in:0|exists:cities,id',
            ]);
        $city = City::findOrFail($request->city_id);
        //dd($city);
        $exists = City::where('name',$request->update_name)->where('id','!=',$request->city_id)->first();
        if (isset($exists->name)){
            $msg = "<span style='color:red;'> <strong>Already exists!!!</strong></span>";
            return $msg;
        }else{
            $city->name = $request->update_name;
            $city->save();
            $msg = "<span style='color:green;'> <strong>Successfully added!!!</strong></span>";
            return $msg;
        }
    }
    public function destroyCity($id){

            $city = City::findOrFail($id);
            if($city){
                if ($city->areas->count() >0){
                    Toastr::error('City is already use. you cant delete this.','Error');
                    return redirect()->back();
                }else{
                    $city->delete();
                    Toastr::success('City is delete successfully','Success');
                    return redirect()->back();
                }
            }
            return redirect()->back();
    }
    public function area(){
        $areas = Area::all();
        $cities = City::all();
        return view('admin.location.area',compact('areas','cities'));
    }
    public function addarea(Request $request){

        $this->validation($request->all())->validate();
        $exists = Area::where( 'city_id','=',$request->city)->where( 'name','=',$request->name)->get()->first();
        //dd($exists);
        if (isset($exists->name) ){
            $msg = "<span style='color:red;'> <strong>Already exists!!!</strong></span>";
            return $msg;
        }else {
            $area = new Area();
            $area->city_id = $request->city;
            $area->name = $request->name;
            $area->save();
            $msg = "<span style='color:green;'>Success! <strong>Successfully added!!!</strong></span>";
            return $msg;
        }
    }
    public function updateArea(Request $request){

        $this->validate($request, [
           'city'=> 'required|integer|exists:cities,id',
           'update_name'=> 'required|string',
           'area_id'=> 'required|integer|exists:areas,id',
        ]);
        $area = Area::findOrFail($request->area_id);
        $exists = Area::where('city_id',$request->city)->where('name',$request->update_name)
            ->where('id','!=',$request->area_id)->first();
        //dd(isset($exists->name));
        if (isset($exists->name)){
            $msg = "<span style='color:red;'> <strong>Already exists!!!</strong></span>";
            return $msg;
        }else {
            $area->city_id = $request->city;
            $area->name = $request->update_name;
            $area->save();
            $msg = "<span style='color:green;'>Success! <strong>Successfully Updated!!!</strong></span>";
            return $msg;
        }
    }
    public function destroyArea($id){

        $area = Area::findOrFail($id);
        if($area){
            if ($area->shops->count() + $area->drivers->count()>0){
                Toastr::error('State is already use. you cant delete this.','Error');
                return redirect()->back();
            }else{
                $area->delete();
                Toastr::success('State is delete successfully','Success');
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
    protected function validation(array $data){
        return Validator::make($data,[
            'name' => ['required','string','max:191'],
            'city' => ['required','integer','exists:cities,id'],
        ]);
    }
}
