<?php

namespace App\Http\Controllers\Admin;

use App\CashoutRequestDriver;
use App\CashoutRequestShop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CashoutController extends Controller
{
    public function shop(){
        $shops = CashoutRequestShop::all();
        return view('admin.cashout.shop',compact('shops'));
    }
    public function driver(){
        $drivers = CashoutRequestDriver::all();
        return view('admin.cashout.driver',compact('drivers'));
    }
}
