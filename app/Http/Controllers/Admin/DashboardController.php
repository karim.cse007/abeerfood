<?php

namespace App\Http\Controllers\Admin;

use App\AdminAccount;
use App\Shop;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index()
    {

        if (!Session::get('notify')){
            Session::put('notify',0);
        }
        $adminacc = AdminAccount::where('created_at','>=',Carbon::today())->sum('amount');
        return view('admin.dashboard',compact('adminacc'));
    }
}
