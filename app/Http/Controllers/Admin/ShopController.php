<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ShopApproveJob;
use App\Jobs\ShopCancelJob;
use App\Percentagefromshop;
use App\Shop;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    public function pendingshops(){
        if (Auth::user()->role->id == 1){
            $shops = Shop::where('is_approved',false)->get();
            return view('admin.shop.pendingshop',compact('shops'));
        }
    }

    public function shopapprove(Request $request, $id){
        if(Auth::user()->role->id == 1){
            $shop = Shop::findOrFail($id);
            $shop->is_approved = true;
            $shop->save();
            ShopApproveJob::dispatch($shop->user)->delay(now()->addSeconds(30));
            Toastr::success('Successful Approved','Success');
            return redirect()->back();
        }
        return redirect()->back();
    }

    public function shopCancel($id){
        $shop = Shop::findOrFail($id);
        if ($shop){
            if ($shop->is_approved == false){
                $user = $shop->user;
                $email = $shop->user->email;
                $shop->delete();
                $user->delete();
                ShopCancelJob::dispatch($email)->delay(now()->addSeconds(30));
                Toastr::success('Successful Cancel','Success');
                return redirect()->back();
            }else{
                Toastr::error('Invalid credential','Error');
                return redirect()->back();
            }
        }
        return redirect()->back();
    }

    public function index(){
        if (Auth::check() and Auth::user()->role->id == 1){
            $shops = Shop::where('is_approved',1)->get();
            return view('admin.shop.shop',compact('shops'));
        }
    }
    public function block(Request $request, $id){
        if ($request->isMethod('POST')) {
            if (Auth::check() and Auth::user()->role->id == 1) {
                $shop = Shop::findOrFail($id);
                if($shop and  $shop->is_blocked == false){
                    $shop->is_blocked = true;
                    $shop->save();
                    return redirect()->back();
                }else{
                    Toastr::error('you dont have permission to do this','Access denied');
                    return redirect()->back();
                }
            }
        }
        return redirect()->back();
    }
    public function unblock(Request $request, $id){
        if ($request->isMethod('POST')) {
            if (Auth::check() and Auth::user()->role->id == 1) {
                $shop = Shop::findOrFail($id);
                if($shop and  $shop->is_blocked == true){
                    $shop->is_blocked = false;
                    $shop->save();
                    return redirect()->back();
                }else{
                    Toastr::error('you dont have permission to do this','Access denied');
                    return redirect()->back();
                }
            }
        }
        return redirect()->back();
    }

    public function taxRate(){
        $shops = Shop::where('is_approved',1)->get();
        return view('admin.shop.tax',compact('shops'));
    }
    public function taxUpdate(Request $request){

        $this->validate($request,[
            'tax' => 'required|not_in:0',
            'shopid' => 'required|exists:shops,id',
        ]);

        $shop = Shop::findOrFail($request->shopid);

        if ($shop){
            $tax = Percentagefromshop::where('shop_id',$request->shopid)->first();
            //dd($tax);
            if ($tax !=null){
                $tax->percentage = $request->tax;
                $tax->save();
                $msg = "<span style='color:green'><strong>Successfully change Tax Rate</strong></span>";
                echo $msg;
            }else{
                $tax = new Percentagefromshop();
                $tax->shop_id = $request->shopid;
                $tax->percentage = $request->tax;
                $tax->save();
                $msg = "<span style='color:green'><strong>Successfully change Tax Rate</strong></span>";
                echo $msg;
            }
            //return redirect()->back();
        }
        //return back();
    }

}
