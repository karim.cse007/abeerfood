<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{
    public function index(){
        $user = User::findOrFail(Auth::id());
        //dd($user->image);
        return view('admin.settings',compact('user'));
    }

    public function updateProfile(Request $request){

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|max:191|email|unique:users,email,'.Auth::id(),
            'phone_number' => 'required|unique:users,phone_number,'.Auth::id(),
            'addressl1' =>'required',
            'city' =>'required',
            'country' =>'required',
            'zip' =>'required',
            'image' => 'image',
        ]);

        $image = $request->file('image');
        $curentDate = Carbon::now()->toDateString();
        $slug = str_slug($request->name).'-'.$curentDate.'-'.uniqid();
        $user = User::findOrFail(Auth::id());

        if (isset($image)){
            $imageName = $slug.'.'.$image->getClientOriginalExtension();

            if (Storage::disk('public')->exists('profile')){
                Storage::disk('public')->makeDirectory('profile');
            }

            if ($user->image != 'default.png'){
                if (Storage::disk('public')->exists('profile/'.$user->image)){
                    Storage::disk('public')->delete('profile/'.$user->image);
                }
            }

            $profile = Image::make($image)->resize(500,500)->save();

            Storage::disk('public')->put('profile/'.$imageName,$profile);
        }else{
            $imageName = $user->image;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->addressl1 = $request->addressl1;
        $user->addressl2 = $request->addressl2;
        $user->city = $request->city;
        $user->post_code = $request->zip;
        $user->country = $request->country;
        $user->image = $imageName;
        $user->save();

        Toastr::success('Update successfully','Success');
        return redirect()->back();
    }
    public function updatePassword(Request $request){

        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        $hashpassword = Auth::user()->password;

        if(Hash::check($request->old_password, $hashpassword)){
            if (!Hash::check($request->password,$hashpassword)){
                $user = User::find(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();

                Toastr::success('Password change successfully', 'Success');
                Auth::logout();
                return redirect()->back();
            }else{
                Toastr::error('New password cannot be the same as old password', 'Error');
                return redirect()->back();
            }
        }else{
            Toastr::error('Current password cannot match!!', 'Error');
            return redirect()->back();
        }

    }
}
