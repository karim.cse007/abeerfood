<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index(){
        if (Auth::check() and Auth::user()->role->id == 1){
            $admins = User::where('role_id',1)->get();
            return view('admin.admin',compact('admins'));
        }
    }
    public function block(Request $request, $id){
        if ($request->isMethod('POST')) {
            if (Auth::check() and Auth::user()->role->id == 1) {
                $admin = User::findOrFail($id);
                if($admin and $admin->id != Auth::id() and $admin->role_id == 1  and  $admin->is_blocked == false){
                    $admin->is_blocked = true;
                    $admin->save();
                    return redirect()->back();
                }else{
                    Toastr::error('you dont have permission to do this','Access denied');
                    return redirect()->back();
                }
            }
        }
    }
    public function unblock(Request $request, $id){
        if ($request->isMethod('POST')) {
            if (Auth::check() and Auth::user()->role->id == 1) {
                $admin = User::findOrFail($id);
                if($admin  and $admin->id != Auth::id() and $admin->role_id == 1 and  $admin->is_blocked == true){
                    $admin->is_blocked = false;
                    $admin->save();
                    return redirect()->back();
                }else{
                    Toastr::error('you dont have permission to do this','Access denied');
                    return redirect()->back();
                }
            }
        }
    }
}
