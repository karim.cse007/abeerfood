<?php

namespace App\Http\Controllers\Admin;

use App\AdminAccount;
use App\DriverAccount;
use App\Order;
use App\RequestDriver;
use App\Shop;
use App\ShopownerAccount;
use App\Transaction;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function index(){
        if (Auth::user()->role->id == '1'){
            $orders = Order::where('is_delivered',0)->orderBy('id','desc')->get();
            return view('admin.order.order',compact('orders'));
        }
        Auth::logout();
        return redirect()->back();

    }

    public function approveorder($id){

        $order = Order::findOrFail($id);
        if($order){
            $order->is_approved = 1;
            $order->save();
        }
        return redirect()->back();
    }
    public function transactionpage(){
        $transactions = Transaction::all();
        return view('admin.order.transaction',compact('transactions'));
    }
    public function requestDriver(Request $request){
        $this->validation($request->all())->validate();
        $check = RequestDriver::find($request->order_id);
        if(!$check){
            $requestDriver = new RequestDriver();
            $requestDriver->order_id=$request->order_id;
            $requestDriver->shop_id=$request->shop_id;
            $requestDriver->save();
        }
        return back();
    }
    protected function validation(array $data){
        return Validator::make($data,[
            'order_id' => ['required','integer','not_in:0','exists:orders,id'],
            'shop_id' => ['required','integer','not_in:0','exists:shops,id'],
        ]);
    }

    public function ordercomplete(){
        $orders = Order::where('is_delivered',1)->where('is_approved',1)->where('is_cooked',2)->get();
        return view('admin.order.complete',compact('orders'));
    }
    public function transaction(Request $request){
        $this->validation($request->all())->validate();
        $order = Order::findOrFail($request->order_id);
        $shop = Shop::findOrFail($request->shop_id);
        //dd(isset($order->shop->tax->percentage));
        if ($order and $shop){
            if ($order->user_id != null and $order->is_delivered == true and $order->is_approved ==1 and $order->is_cooked == 2){
                if (isset($order->shop->tax->percentage)){
                    $tax = $order->shop->tax->percentage;
                }else $tax =30;
                $amountAdmin = $order->total_price* $tax/100;
                $amountShop = $order->total_price - $amountAdmin;

                $transaction = new Transaction();
                $transaction->user_id=$order->user_id;
                $transaction->shop_id=$order->shop_id;
                $transaction->tax=$tax;
                $transaction->item_list=$order->item_list;
                $transaction->total_quantity=$order->total_quantity;
                $transaction->total_price=$order->total_price;
                $transaction->delivery_charge=$order->delivery_charge;
                $transaction->payment_id=$order->payment_id;
                $transaction->pay_id=$order->pay_id;
                $transaction->transaction_or_payer_id=$order->transaction_or_payer_id;
                $transaction->receipt_url=$order->receipt_url;
                $transaction->shippingaddress_id=$order->shippingaddress_id;
                $transaction->driver_id=$order->driver_id;
                $transaction->save();

                $transaction = Transaction::where('pay_id',$order->pay_id)->first();
                if (isset($transaction->pay_id)){
                    $shop = new ShopownerAccount();
                    $shop->shop_id=$order->shop_id;
                    $shop->transaction_id=$transaction->id;
                    $shop->total_price=$order->total_price;
                    $shop->tax=$tax;
                    $shop->amount=$amountShop;
                    $shop->save();

                    $admin = new AdminAccount();
                    $admin->shop_id=$order->shop_id;
                    $admin->transaction_id=$transaction->id;
                    $admin->total_price=$order->total_price;
                    $admin->tax=$tax;
                    $admin->amount=$amountAdmin;
                    $admin->save();

                    $driver = new DriverAccount();
                    $driver->driver_id=$order->driver_id;
                    $driver->transaction_id=$transaction->id;
                    $driver->delivery_rate=50;
                    $driver->amount=50;
                    $driver->save();

                    $order->delete();
                    return back();
                }
            }else{
                Toastr::error('Invalid credential','Error');
                return redirect()->back();
            }
            Toastr::error('Invalid credential','Error');
            return redirect()->back();
        }
        return back();
    }
}
