<?php

namespace App\Http\Controllers\Admin;

use App\Driver;
use App\Jobs\RiderApprovalEmail;
use App\Jobs\RiderCancelJob;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    public function pendingdrivers(){
        if (Auth::user()->role->id == 1){
            $drives = Driver::where('is_approved',false)->get();
            return view('admin.driver.pendingdriver',compact('drives'));
        }
    }

    public function driverApprove(Request $request, $id){
        if(Auth::user()->role->id == 1){
            $driver = Driver::findOrFail($id);
            $driver->is_approved = true;
            $driver->save();
            RiderApprovalEmail::dispatch($driver->user)->delay(now()->addSeconds(30));
            Toastr::success('Approved successfully','Success');
            return redirect()->back();
        }
        return redirect()->back();
    }
    public function driverCancel($id)
    {
        $driver = Driver::findOrFail($id);
        if($driver){
            if ($driver->is_approved == false){
                $email = $driver->user->email;
                $user = $driver->user;
                $driver->delete();
                $user->delete();
                RiderCancelJob::dispatch($email)->delay(now()->addSeconds(30));
                Toastr::success('Cancel successfully','Success');
                return redirect()->back();
            }
            Toastr::error('Invalid request','Error');
        }
        return back();
    }

    public function index(){
        if (Auth::check() and Auth::user()->role->id == 1){
            $drivers = Driver::all();
            return view('admin.driver.driver',compact('drivers'));
        }
    }
    public function block(Request $request, $id){
        if ($request->isMethod('POST')) {
            if (Auth::check() and Auth::user()->role->id == 1) {
                $driver = Driver::findOrFail($id);
                $user = User::findOrFail($driver->user->id);
                if($driver and $user and  $user->is_blocked == false){
                    $user->is_blocked = true;
                    $user->save();
                    return redirect()->back();
                }else{
                    Toastr::error('you dont have permission to do this','Access denied');
                    return redirect()->back();
                }
            }
        }
        return redirect()->back();
    }
    public function unblock(Request $request, $id){
        if (Auth::check() and Auth::user()->role->id == 1) {
            $driver = Driver::findOrFail($id);
            $user = User::findOrFail($driver->user->id);
            if($driver and $user and  $user->is_blocked == true){
                $user->is_blocked = false;
                $user->save();
                return redirect()->back();
            }else{
                Toastr::error('you dont have permission to do this','Access denied');
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
}
