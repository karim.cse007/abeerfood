<?php

namespace App\Http\Controllers\Admin;

use App\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function blockeduser(){
        if(Auth::user()->role->id == 1){
            $users = User::where('is_blocked',true)->get();
            return view('admin.block',compact('users'));
        }else{
            Auth::logout();
            return redirect()->back();
        }
    }
    public function unblockeduser(Request $request,$id){
        if ($request->isMethod('POST')){
            if(Auth::user()->role->id == 1){
                $user = User::findOrFail($id);

                $user->is_blocked = false;
                $user->save();
                return redirect()->back();
            }else{
                Auth::logout();
                return redirect()->back();
            }
        }else{
            return redirect()->back();
        }
    }

}
