<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function makeSeenOrder(Request $request){
        $orders = Order::latest('created_at')->where('created_at','>=',Carbon::today())->where('is_view_admin',0)->get();
        if ($orders){
            foreach ($orders as $order){
                $order->is_view_admin=true;
                $order->save();
            }
        }
    }
}
