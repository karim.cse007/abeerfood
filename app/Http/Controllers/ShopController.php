<?php

namespace App\Http\Controllers;

use App\Category;
use App\Review;
use App\Shop;
use App\Area;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShopController extends Controller
{
    public function postByShop($slug){
        $shop = Shop::where('slug',$slug)->IsApproved()->IsBlocked()->first();
        $cartCollections = \Cart::getContent();
        $total = \Cart::getTotal();
        session()->put('shipping',false);
        $reviews = Review::latest()->where('shop_id',$shop->id)->take(5)->get();
        if($shop){
            $categories = $shop->categories;
            return view('postsbyshop',compact('categories','shop','cartCollections','total','reviews'));
        }else{
            return redirect()->back();
        }
    }

    public function shopByArea(Request $request){

        $this->validate($request,[
            'searchinput' => 'required|string|max:191',
            'lat' => 'required|max:191',
            'long' => 'required|max:191',
        ]);
        $shops = Shop::all()->where('lat','>=',$request->lat-0.03)
                        ->where('lat','<=',$request->lat + 0.03)
                        ->where('long','>=',$request->long-0.03)
                        ->where('long','<=',$request->long+0.03)
                        ->IsApproved()->IsBlocked()->get();
        //dd($shops);
        if($shops->count() >0){
            //dd($shops);
            return view('searchbyarea',compact('shops'));
        }
        return view('searchbyarea');
    }
}
