<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function sentContact(Request $request){
        $this->validation($request->all())->validate();
        $ip = $request->getClientIp();
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'subject' => 'Contact form',
            'email' => $request->email,
            'name' => $request->name,
            'dateTime' => $dateTime,
            'ip' => $ip,
            'msg' => $request->message,
        );
        Mail::send('email.contact',$data, function ($message) use ($data) {
            $message->from($data['email']);
            $message->to('info@abeer.se');
            $message->subject($data['subject']);
        });
        $msg ="<div class='alert alert-success'>".
            "<b>Success</b> Thank you for contacting with us".
            "</div>";
        return $msg;
    }
    protected function validation(array $data){
        return Validator::make($data,[
            'name' => ['required','string','max:191'],
            'email' => ['required','email','max:50'],
            'message' => ['required','string'],
        ]);
    }
}
