<?php

namespace App\Http\Controllers\Customer;

use App\Post;
use App\Shippingaddress;
use App\Shop;
use App\Tempdistance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function addcart($id){

        $customerId = Auth::id();
        $post = Post::findOrFail($id);
        if($post and $post->available == 1){
            $name = $post->title;
            $price = $post->price;
            $quantity= $post->quantity;
            $cartCollections = \Cart::getContent();

            foreach ($cartCollections as $cart){
                if($cart->attributes->shop_id != $post->shop_id){//if shop other shop than clean before shop history
                    \Cart::clear();
                    Session::put('d_charge','30');
                }
                break;
            }
            \Cart::add(array(
                'id' => $post->id,
                'name' => $name,
                'price' => $price,
                'quantity' => $quantity,
                'attributes' => array(
                    'customer_id' => $customerId,
                    'shop_id' => $post->shop_id,
                    'image' => $post->image,
                ),
            ));
        }
        return redirect(url()->previous().'#pills-order-online');
    }
    public function distance(Request $request){
        $user = Auth::user();
        $distance = round($request->dist,0,PHP_ROUND_HALF_UP);
        $exist = $user->tempDistance;
        if ($distance > 5){
            $ch = $distance - 5;
            $chh = $ch * 15;
            $charge = $chh + 30;
        }else{
            $charge = 30;
        }
        if ($exist){
            $exist->distance = $distance;
            $exist->charge = $charge;
            $exist->save();
        }else{
            $tempDist = new Tempdistance();
            $tempDist->user_id = $user->id;
            $tempDist->distance = $distance;
            $tempDist->charge = $charge;
            $tempDist->save();
        }

    }
    public function show(){
        $cartCollections = \Cart::getContent();
        $total = \Cart::getTotal();
        $shop_id ='';
        foreach ($cartCollections->take(1) as $cart){
            $shop_id = $cart->attributes->shop_id;
        }
        $charge = Auth::user()->tempDistance->charge;
        $shop = Shop::findOrFail($shop_id);
        return view('auth.cart',compact('cartCollections','total','shop','charge'));

    }
    public function remove($id){

        \Cart::remove($id);
        return redirect(url()->previous().'#cart-info');
    }

    public function update(Request $request){
        $this->validate($request,[
            'pid' => 'required|integer|not_in:0',
            'cartId' => 'required|integer|not_in:0',
        ]);

        $cartCollections = \Cart::get($request->cartId);
        //dd($cartCollections,$request->cartId);
        if ($cartCollections){
            if($request->pid == 2 && $cartCollections->quantity +1 >0){
                \Cart::update($request->cartId, array(
                    'quantity' => 1,
                ));
            }elseif ($request->pid == 1 && $cartCollections->quantity -1 >0){
                \Cart::update($request->cartId, array(
                    'quantity' => -1,
                ));
            }else \Cart::remove($request->cartId);
        }
        return redirect(url()->previous().'#cart-info');
    }

    public function checkout(){
        $cartCollections = \Cart::getContent();
        $shop_id ='';
        foreach ($cartCollections->take(1) as $cart){
            $shop_id = $cart->attributes->shop_id;
        }
        if($shop_id != ''){
            $total = \Cart::getTotal();
            $shop = Shop::findOrFail($shop_id);
            if($shop){
                $shipaddress = Shippingaddress::where('user_id',Auth::id())->latest('created_at')->first();

                //dd($shipaddress);
                return view('auth.checkout',compact('cartCollections','total','shop','shipaddress'));
            }
            return redirect()->back();
        }
        return redirect()->back();

    }
    public function destroy(){
        \Session::flush();
        \Cart::clear();
        return redirect()->back();
    }

    public function thanks(){
        return view('thanks');
    }
}
