<?php

namespace App\Http\Controllers\Customer;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{
    public function updateProfile(Request $request){
        if(Auth::user()->role->id == 4){
            $user = User::findOrFail(Auth::id());
            if($user){
                if($request->isMethod('POST')){
                   // dd($request);
                    $this->validate($request,[
                        'name' => 'required|string|max:191',
                        'phone_number' => 'required|string|max:20',
                        'addressl1' => 'required|string|max:191',
                        'city' => 'required|string|max:100',
                        'post_code' => 'required|string|max:100',
                        'image' => 'image',
                    ]);
                    //dd($request);
                    //get from image
                    $image = $request->file('image');
                    //make current date and time
                    $currentDate = Carbon::now()->toDateString();

                    $slug = str_slug($request->name) . '-' . $currentDate;
                    if (isset($image)) {

                        $imagename = $slug.'-' . uniqid() . '.' . $image->getClientOriginalExtension();

                        //check category dir is exits
                        if (!Storage::disk('public')->exists('profile')) {
                            Storage::disk('public')->makeDirectory('profile');
                        }

                        ///delete image if exist
                        if($user->image != 'default.png'){
                            if (Storage::disk('public')->exists('profile/' . $user->image)) {
                                Storage::disk('public')->delete('profile/' . $user->image);

                            }
                        }
                        //resize images for category and upload
                        $postImg = Image::make($image)->resize(200, 200)->save();
                        Storage::disk('public')->put('profile/' . $imagename, $postImg);
                    } else {
                        $imagename = $user->image;
                    }
                    $user->name = $request->name;
                    $user->phone_number = $request->phone_number;
                    $user->image = $imagename;
                    $user->addressl1 = $request->addressl1;
                    $user->addressl2 = $request->addressl2;
                    $user->city = $request->city;
                    $user->post_code = $request->post_code;
                    $user->save();
                    $msg = "<span style='color:green;'>Success !<strong> Succesfuly change Information</strong></span>";
                    return $msg;
                }
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->back();
        }
    }

    public function updatePassword( Request $request){
        if(Auth::user()->role->id == 4){
            $user = User::findOrFail(Auth::id());
            if($user){
                if($request->isMethod('POST')){
                    //dd($request);
                    $this->validate($request,[
                        'old_password' => 'required',
                        'password' => 'required|confirmed|min:6',
                    ]);
                    $hashpassword = $user->password;

                    if(Hash::check($request->old_password, $hashpassword)){
                        if (!Hash::check($request->password,$hashpassword)){

                            $user->password = Hash::make($request->password);
                            $user->save();
                            $msg = "<span style='color:green;'>Success !<strong> Succesfuly change the password</strong></span>";
                            return $msg;

                        }else{
                            $msg ="<span style='color:red;'>Error! <strong>New password cannot be the same as old password</strong></span>";
                            return $msg;
                        }
                    }else{
                        $msg ="<span style='color:red;'>Error! <strong>old password not match!!</strong></span>";
                        return $msg;
                    }

                }else{
                    return redirect()->back();
                }
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->back();
        }
    }


}
