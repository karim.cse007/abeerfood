<?php

namespace App\Http\Controllers\Customer;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
   public function index(){
        if(Auth::user()->role_id == 4){
            $user = Auth::user();
            $cities = City::all();
            return view('customer.profile',compact('user','cities'));
        }else{
            return redirect()->back();
        }
   }

}
