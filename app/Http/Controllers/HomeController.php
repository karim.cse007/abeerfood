<?php

namespace App\Http\Controllers;

use App\Area;
use App\Shop;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth' => 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $shops = Shop::where('is_approved',1)->where('is_blocked',0)->get();
        if ($shops->count() >7) {
            $shops = Shop::latest(8)->IsApproved()->IsBlocked()->get();
        }
        $areas = Area::all();
        //dd($shops);
        return view('welcome',compact('shops','areas'));
    }
    public function thanks(){
        return view('register.thanks');
    }
    public function contact(){
        return view('contact');
    }

}
