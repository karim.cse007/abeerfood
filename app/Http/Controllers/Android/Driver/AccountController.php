<?php

namespace App\Http\Controllers\Android\Driver;

use App\CashoutDriver;
use App\CashoutRequestDriver;
use App\Driver;
use App\DriverAccount;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function cashoutInfo(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        if (isset($driver->id) and $user->id == $driver->user_id){
            $totalEarn = DriverAccount::where('driver_id',$driver->id)->sum('amount');
            $totalCashout = CashoutDriver::where('driver_id',$driver->id)->sum('amount');
            $pendingCashout = CashoutRequestDriver::where('driver_id',$driver->id)->sum('amount');
            $availAbleBalance = $totalEarn-$totalCashout-$pendingCashout;
            $info=([
                'availAbleBalance'=>$availAbleBalance,
            ]);
            return response()->json($info,200);
        }

        return response()->json('Access denied',500);
    }
    public function cashout(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
            'reqbalance' => ['required','integer','not_in:0'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        if (isset($driver->id) and $user->id == $driver->user_id){
            $bl1 = $user->driver->account->sum('amount'); //this is total balance
            $bl2 = $user->driver->cashoutdriver->sum('amount'); //this is already cashout balance
            $bl3 = $user->driver->cashoutrequestdriver->sum('amount'); //this is pending cashout balance
            $bl = $bl1-($bl2+$bl3);
            //dd($request);
            if($request->reqbalance <= $bl){
                $cashoutRequest = new CashoutRequestDriver();
                $cashoutRequest->driver_id = $driver->id;
                $cashoutRequest->user_id = $user->id;
                $cashoutRequest->amount = $request->reqbalance;
                $cashoutRequest->save();
               return response()->json('Your cashout request is sentcashout',200);
            }else{
                return response()->json('Not enough balance to cashout',500);
            }
        }
        return response()->json('Access denied',500);
    }
}
