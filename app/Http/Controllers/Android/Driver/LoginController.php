<?php

namespace App\Http\Controllers\Android\Driver;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request){
        $this->validate($request,[
            'email'=>'email|required',
            'password'=>'required|string',
        ]);
        $user = User::where('email',$request->email)
            ->where('role_id',3)
            ->first();
        if (Hash::check($request->password,$user->password)){
            $info=([
                'userInfo'=>[
                    'user_id'=>$user->id,
                    'driving_id'=>$user->driver->id,
                    'name'=>$user->name,
                    'email'=>$user->email,
                    'phone_no'=>$user->phone_number,
                    'adrress'=>$user->addressl1.$user->addressl2,
                    'city'=>$user->city,
                ],
            ]);
            return response()->json($info,200);
        }
        return response()->json(false,500);
        
    }
}
