<?php

namespace App\Http\Controllers\Android\Driver;

use App\Jobs\ResetPasswordJob;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ResetController extends Controller
{
    public function resetRequest(Request $request){
        $this->validate($request,[
           'email'=>'required|email',
        ]);
        $user = User::where('email',$request->email)->first();
        if (isset($user->id)){
            $rand =rand(100000,999999);
            $exists = DB::select('select * from password_resets where email= ? limit 1',[$request->email]);
            if ($exists){
                DB::update('update password_resets set token = ? where email = ?', [$rand,$request->email]);
                $msg =[
                    'success'=>'Sent you a verification code',
                ];
                ResetPasswordJob::dispatch($user,$rand)->delay(now()->addSeconds(20));
                return response()->json($msg,200);
            }else{
                DB::table('password_resets')->insert([
                    'email'=>$request->email,
                    'token'=>$rand,
                    'created_at'=>Carbon::now(),
                ]);
                $msg =[
                    'success'=>'Sent you a verification code',
                ];
                ResetPasswordJob::dispatch($user,$rand)->delay(now()->addSeconds(20));
                return response()->json($msg,200);
            }
        }else{
            $msg =[
              'error'=>'Email cant find',
            ];
            return response()->json($msg,500);
        }
    }
    public function codeMatch(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'token'=>'required|int'
        ]);
        $user = DB::select('select * from password_resets where email=? and token =?',[$request->email,$request->token]);
        if ($user){
            $msg =[
                'success'=>'token match',
            ];
            return response()->json($msg,200);
        }
        $msg =[
            'error'=>'token cant match',
        ];
        return response()->json($msg,500);
    }
    public function passUpdate(Request $request){
        $this->validate($request,[
            'email'=>'required|email|exists:users,email',
            'password'=>'required|string|min:6'
        ]);
        $user = User::where('email',$request->email)->first();
        if (isset($user->id)){
            $password = Hash::make($request->password);
            $user->password = $password;
            $user->save();
            DB::delete('delete from password_resets where email= ?',[$request->email]);
            $msg =[
                'success'=>'successfully change password',
            ];
            return response()->json($msg,200);
        }
        $msg =[
            'error'=>'cant change password',
        ];
        return response()->json($msg,500);
    }
}
