<?php

namespace App\Http\Controllers\Android\Driver;

use App\Area;
use App\CashoutDriver;
use App\CashoutRequestDriver;
use App\City;
use App\Driver;
use App\DriverAccount;
use App\Transaction;
use App\User;
use App\Smartphone;
use App\Vehicle;
use App\Workinghour;
use App\WorkingType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function profile(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0'],
        ]);
        $user = User::findOrFail($request->user_id);
        if (isset($user->id)){
            return response()->json($user,200);
        }
        return response()->json(false,500);
    }

    public function personalEdit(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0'],
        ]);
        $user = User::findOrFail($request->user_id);
        if (isset($user->id)){
            return response()->json($user,200);
        }
        return response()->json(false,500);
    }
    public function personalUpdate(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0'],
            'name' => 'required|string|max:191',
            'phone_number' => 'required|string|max:20',
            'addressl1' => 'required|string|max:191',
            'addressl2' => 'string|max:191',
            'city' => 'required|string|max:100',
            'post_code' => 'required|string|max:100',
        ]);
        $user = User::findOrFail($request->user_id);
        if (isset($user->id)){
            if($request->isMethod('POST')){
                $user->name = $request->name;
                $user->phone_number = $request->phone_number;
                $user->addressl1 = $request->addressl1;
                $user->addressl2 = $request->addressl2;
                $user->city = $request->city;
                $user->post_code = $request->post_code;
                $user->save();
                return response()->json($user,200);
            }
        }
        return response()->json(false,500);
    }
    public function drivingInfo(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        if (isset($driver->id) and $user->id == $driver->user_id){
            $driving_info=([
                'social_security' =>$driver->social_security,
                'city' =>$driver->city->name,
                'area' =>$driver->area->name,
                'vehicle' =>$driver->vehicle->name,
                'working_type' =>$driver->workingType->name,
                'working_hour' =>$driver->workinghour->name,
                'smartphone' =>$driver->smartphone->name,
            ]);
            return response()->json($driving_info,200);
        }
        return response()->json(false,500);

    }
    public function drivingEdit(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        if (isset($driver->id) and $user->id == $driver->user_id){

            $cities = City::all();
            $areas = Area::all();
            $vehicles = Vehicle::all();
            $working_types = WorkingType::all();
            $working_hours = Workinghour::all();
            $smartphones = Smartphone::all();
            $info = [
                'cities' =>$cities,
                'areas' =>$areas,
                'vehicles' =>$vehicles,
                'working_types' =>$working_types,
                'working_hours' =>$working_hours,
                'smartphones' =>$smartphones,
                'driving_info'=>$driver
            ];
            return response()->json($info,200);
        }
        return response()->json('Access denied!',500);
    }
    public function drivingUpdate(Request $request){
        $this->validation($request->all())->validate();
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        if(isset($driver->id) and $user->id == $driver->user_id){

            $driver->city_id = $request->city;
            $driver->area_id = $request->area;
            $driver->vehicle_id = $request->vehicle;
            $driver->working_type_id = $request->w_during;
            $driver->workinghour_id = $request->w_hours;
            $driver->smartphone_id = $request->phone_type;
            $driver->save();
            return response()->json('Successfully change',200);
        }
        return response()->json(false,500);
    }

    public function driverStatusUpdate(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
            'gcm_token' => ['required'],
            'lat' => ['required'],
            'long' => ['required'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        if(isset($driver->id) and $user->id == $driver->user_id){

            $driver->lat = $request->lat;
            $driver->long = $request->long;
            $driver->gcm_token = $request->gcm_token;
            $driver->save();
            return response()->json(true,200);
        }
        return response()->json(false,500);
    }
    protected function validation(array $data){
        return Validator::make($data,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
            'city' => ['required','integer','not_in:0','exists:cities,id'],
            'area' => ['required','integer','not_in:0','exists:areas,id'],
            'vehicle' => ['required','integer','not_in:0','exists:vehicles,id'],
            'w_during' => ['required','integer','not_in:0','exists:working_types,id'],
            'w_hours' => ['required','integer','not_in:0','exists:workinghours,id'],
            'phone_type' => ['required','integer','not_in:0','exists:smartphones,id'],
        ]);
    }
    public function passwordUpdate(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'old_password' => 'required',
            'password' => 'required|min:6',
        ]);
        $user = User::findOrFail($request->user_id);
        if(isset($user->id)){
            $hashpassword = $user->password;

                if(Hash::check($request->old_password, $hashpassword)){
                    if (!Hash::check($request->password,$hashpassword)){
                        $user->password = Hash::make($request->password);
                        $user->save();
                        $msg =([
                            'message'=>'Password successfully change'
                        ]);
                        return response()->json($msg,200);

                    }else{
                        $msg =([
                            'message'=>'Password cant be same as old password',
                        ]);
                        return response()->json($msg,500);
                    }
                }else{
                    $msg =([
                            'message'=>'Old password not match!!',
                        ]);
                        return response()->json($msg,500);
                }
        }
        $msg =([
            'message'=>'Access denied',
        ]);
        return response()->json($msg,500);
    }

    public function ridingHistory(Request $request){

        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        if (isset($driver->id) and $user->id == $driver->user_id){
            $totalRide = Transaction::where('driver_id',$driver->id)->count();
            $totalEarn = DriverAccount::where('driver_id',$driver->id)->sum('amount');
            $totalCashout = CashoutDriver::where('driver_id',$driver->id)->sum('amount');
            $pendingCashout = CashoutRequestDriver::where('driver_id',$driver->id)->sum('amount');
            $availAbleBalance = $totalEarn-$totalCashout-$pendingCashout;
            $info=([
                'totalRide'=>$totalRide,
                'totalEarn'=>$totalEarn,
                'totalCashout'=>$totalCashout,
                'pendingCashout'=>$pendingCashout,
                'availAbleBalance'=>$availAbleBalance,
            ]);
            return response()->json($info,200);
        }

        return response()->json('Access denied',500);
    }

    public function deliveryHistory(Request $request){
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        if (isset($driver->id) and $user->id == $driver->user_id){
            $histories = Transaction::where('driver_id',$request->driver_id)->get();
            $data = array();
            foreach ($histories as $history){
                ($data[]=[
                    'shop_name'=>$history->shop->shop_name,
                    'items'=>$history->item_list,
                    'quantity'=>$history->total_quantity,
                    'address'=>$history->shippingaddress->location,
                    'date'=>$history->created_at,
                ]);
            }
            return response()->json($data,200);
        }
    }

    public function onlineStatus(Request $request)
    {
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);
        $status =$driver->is_active;

        if (isset($driver->id) and $user->id == $driver->user_id){

            if($status==1)
            {
                $result =[
                   'status' =>'Online',
                        ];
            }else
            {
                $result =[
                   'status' =>'Offline',
                        ];
            }
            return response()->json($result,200);
        }
        $status =[
            'status' =>'access denied',
        ];
        return response()->json($status,500);
    }
    public function onlineStatusUpdate(Request $request)
    {
        $this->validate($request,[
            'user_id' => ['required','integer','not_in:0','exists:users,id'],
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
            'status' => ['required','integer'],
        ]);
        $user = User::findOrFail($request->user_id);
        $driver = Driver::findOrFail($request->driver_id);

        if (isset($driver->id) and $user->id == $driver->user_id){

            if ($request->status == 1) {
                if ($driver->is_blocked == true) {
                    $status =[
                        'status' =>'blocked',
                    ];
                    $driver->is_active=false;
                    $driver->save();
                    return response()->json($status,500);
                }else{
                    $status =[
                        'status' =>'Online',
                    ];
                    $driver->is_active=true;
                    $driver->save();
                    return response()->json($status,200);
                }
            }else if ($request->status == 0){
                $status =[
                    'status' =>'Offline',
                ];
                $driver->is_active=false;
                $driver->save();
                return response()->json($status,200);
            }
        }
        $status =[
            'status' =>'access denied',
        ];
        return response()->json($status,500);
    }
}
