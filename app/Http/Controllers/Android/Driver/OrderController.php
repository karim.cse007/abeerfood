<?php

namespace App\Http\Controllers\Android\Driver;
use DB;
use App\Driver;
use App\DriverManage;
use App\Order;
use App\RequestDriver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function requestOrders(){
        $orders = RequestDriver::all();
        return response()->json($orders,200);
    }
    public function shop(Request $request){
        $this->validate($request,[
            'order_id' => ['required','integer','not_in:0','exists:orders,id'],
        ]);
        $order = Order::all()->where('id',$request->order_id)
            ->where('is_approved',1)
            ->where('is_delivered',0)
            ->where('driver_id',null)
            ->first();
        if (isset($order->id)){
            $shop = $order->shop;
        }
        if (isset($shop->id)){
            $shop_info =[
                'shop_name'=>$shop->shop_name,
                'location'=>$shop->location,
                'lat'=>$shop->lat,
                'long'=>$shop->long,
            ];
            return response()->json($shop_info,200);
        }
        return response()->json(false,500);
    }
    public function shippingAddress(Request $request){
        $this->validate($request,[
            'order_id' => ['required','integer','not_in:0','exists:orders,id'],
        ]);
        $order = Order::findOrFail($request->order_id);
        $shippingaddress = $order->shippingaddress;
        if (isset($shippingaddress->id)){
            return response()->json($shippingaddress,200);
        }
        return response()->json(false,500);
    }

    public function accept(Request $request){
        $this->validate($request,[
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
            'order_id' => ['required','integer','not_in:0','exists:orders,id'],
        ]);
        $driver = Driver::all()->where('id',$request->driver_id)
            ->where('is_busy',0)
            ->first();
        if(isset($driver->id)){
            $order = Order::all()->where('id',$request->order_id)
                ->where('is_approved',1)
                ->where('is_delivered',0)
                ->where('driver_id',null)
                ->first();
            if (isset($order->id)){
                $order->driver_id = $driver->id;
                $order->save();
                $driver->is_busy =true;
                $driver->save();
                // $reqOrder = $order->request_driver;
                // if (isset($reqOrder->id)){
                //     $reqOrder->delete();
                // }
                $reqOrder =  RequestDriver::where('order_id','=', $request->order_id)
                           ->first();
                           if (isset($reqOrder->id)){
                            $reqOrder->delete();
                }
                return response()->json($order,200);
            }
        }
        return response()->json(false,500);
    }
    
    public function nearbyRiders(Request $request)
    {
        $reject_id = array();
        
        $users = DriverManage::where('order_id',$request->order_id)->get();
                 foreach ($users as $user) {
                $reject_id[] = $user->driver_id;
            }
            
         $radius = 99999999999;

  $riders = Driver::select('drivers.*')
 ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                         cos( radians( drivers.lat ) )
                         * cos( radians(drivers.long) - radians(?)
                         ) + sin( radians(?) ) *
                         sin( radians( drivers.lat ) ) )
                       ) AS distance', [$request->latitude, $request->longitude, $request->latitude])
 ->havingRaw("distance < ?", [$radius])
 ->where('is_busy','0')
 ->where('is_active','1')
 ->whereNotIn('id', $reject_id)
 ->first();
 
  //getting shop information from order_id
            $order = Order::all()->where('id',$request->order_id)
            ->where('is_approved',1)
            ->where('is_delivered',0)
            ->where('driver_id',null)
            ->first();
            $shop = $order->shop;
            $lat = $shop->lat;
            $long = $shop->long;
            $shop_name = $shop->shop_name;
            $shop_location = $shop->location;
            
            $data = array(
                'order_id' => $request->order_id,
                'shop_id' => $shop->id,
                'shop_latitude' => $lat,
                'shop_longitude' => $long,
                'shop_name' => $shop_name,
                'shop_location' => $shop_location
                );
            
            $driver_manage = new DriverManage();
            $driver_manage->order_id = $request->order_id;
            $driver_manage->driver_id = $riders['id'];
            $driver_manage-> save();
            
            $gcm_token = $riders['gcm_token'];
            
            $response = $this->sendRequestToRider($gcm_token,$data);
        
        return  Response()->json($response,200);
    }
    
      // sending push message to single user by firebase reg id
    public function sendRequestToRider($to,array $json = []) {
        
      $fields = [
        'to' => $to,
        'data' => $json
      ];
      return $this->sendPushNotification($fields);
    }
    
    public function sendPushNotification(array $data=[])
    {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $firebase_api_key=\Config::get('firebase.api_key');
        
        $headers = array(
            'Authorization: key=' . $firebase_api_key,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;

    }
    
    public function shopDeliver(Request $request){
        $this->validate($request,[
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
            'order_id' => ['required','integer','not_in:0','exists:orders,id'],
        ]);
        $driver = Driver::findOrFail($request->driver_id);
        $order = Order::all()->where('id',$request->order_id)
            ->where('is_approved',1)
            ->where('driver_id',$driver->id)
            ->where('is_cooked','>=',2)
            ->where('is_delivered',0)
            ->first();
        if (isset($order->id)){
            $result = ([
                "status" => true,
                ]);
            return response()->json($result,200);
        }
        $result = ([
                "status" => false,
                ]);
        return response()->json($result,500);
    }
    public function driverDeliverComplete(Request $request){
        $this->validate($request,[
            'driver_id' => ['required','integer','not_in:0','exists:drivers,id'],
            'order_id' => ['required','integer','not_in:0','exists:orders,id'],
        ]);
        $driver = Driver::findOrFail($request->driver_id);
        $order = Order::all()->where('id',$request->order_id)
            ->where('is_approved',1)
            ->where('driver_id',$driver->id)
            ->where('is_cooked','>=',2)
            ->where('is_delivered',0)
            ->first();
        if (isset($order->id)){
            $order->is_delivered = true;
            $order->save();
            $driver->is_busy=false;
            $driver->save();
            $result = ([
                "status" => true,
                ]);
            return response()->json($result,200);
        }
        $result = ([
                "status" => false,
                ]);
        return response()->json($result,500);
    }
}
