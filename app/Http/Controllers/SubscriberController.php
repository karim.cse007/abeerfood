<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    public function store(Request $request){
        $this->validate($request,[
            's_email' => 'required|email'
        ]);
        $subs = Subscriber::where('email',$request->s_email)->first();
        //dd($subs);
        if($subs != null){
            $msg = "<span style='color:red;'>Error! <strong>You are already subscriber!!!</strong></span>";
            return $msg;
        }else{
            $subscriber = new Subscriber();
            $subscriber->email =$request->s_email;
            $subscriber->save();
            $msg = "<span style='color:green;'>Success! <strong>You are now subscriber!!!</strong></span>";
            return $msg;
        }
        //return back();

    }
}
