<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public function drivers(){
        return $this->hasMany('App\Driver');
    }
}
