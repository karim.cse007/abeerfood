<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public function city(){
        return $this->belongsTo('App\City');
    }
    public function shops(){
        return $this->hasMany('App\Shop');
    }
    public function drivers(){
        return $this->hasMany('App\Driver');
    }
}
