<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone_number','is_notify',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }
    public function favorite_shops(){
        return $this->belongsToMany('App\Shop')->withTimestamps();
    }
    public function rating(){
        return $this->hasMany('App\Rating');
    }
    public function review(){
        return $this->hasMany('App\Review');
    }
    public function shop(){
        return $this->hasOne('App\Shop');
    }
    public function driver(){
        return $this->hasOne('App\Driver');
    }
    public function orders(){
        return $this->hasMany('App\Order');
    }
    public function transaction(){
        return $this->hasMany('App\Transaction');
    }
    public function shippingaddress(){
        return $this->hasMany('App\Shippingaddress');
    }
    public function city(){
        return $this->belongsTo('App\City');
    }

    public function tempDistance(){
        return $this->hasOne('App\Tempdistance');
    }
}
