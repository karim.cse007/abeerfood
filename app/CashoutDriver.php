<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashoutDriver extends Model
{
    public function driver(){
        return $this->belongsTo('App\Driver');
    }
}
