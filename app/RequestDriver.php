<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDriver extends Model
{
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
    public function order(){
        return $this->belongsTo('App\Order');
    }
}
