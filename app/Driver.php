<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    public function city(){
        return $this->belongsTo('App\City');
    }
    public function area(){
        return $this->belongsTo('App\Area');
    }
    public function vehicle(){
        return $this->belongsTo('App\Vehicle');
    }
    public function smartphone(){
        return $this->belongsTo('App\Smartphone');
    }
    public function workingType(){
        return $this->belongsTo('App\WorkingType');
    }
    public function workinghour(){
        return $this->belongsTo('App\Workinghour');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function orders(){
        return $this->hasMany('App\Order');
    }
    public function reviews(){
        return $this->hasMany('App\ReviewDriver');
    }
    public function account(){
        return $this->hasMany('App\DriverAccount');
    }
    public function cashoutdriver(){
        return $this->hasMany('App\CashoutDriver');
    }
    public function cashoutrequestdriver(){
        return $this->hasMany('App\CashoutRequestDriver');
    }
}
