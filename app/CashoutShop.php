<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashoutShop extends Model
{
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
}
