<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function posts(){
        return $this->hasMany('App\Post');
    }
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
    public function scopeCategoryCount($query)
    {
        return $query->count();
    }

}
