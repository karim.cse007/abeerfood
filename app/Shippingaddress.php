<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shippingaddress extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function order(){
        return $this->hasOne('App\Order');
    }
    public function transaction(){
        return $this->hasMany('App\Transaction');
    }
    public function city(){
        return $this->belongsTo('App\City');
    }
}
