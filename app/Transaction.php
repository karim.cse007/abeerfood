<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function driver(){
        return $this->belongsTo('App\Driver');
    }
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
    public function payment(){
        return $this->belongsTo('App\PaymentRole');
    }
    public function shippingaddress(){
        return $this->belongsTo('App\Shippingaddress');
    }
}
