<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopownerAccount extends Model
{
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
}
