<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function posts(){
        return $this->hasMany('App\Post');
    }
    public function reviews(){
        return $this->hasMany('App\Review');
    }
    public function categories(){
        return $this->hasMany('App\Category');
    }
    public function ratings(){
        return $this->hasMany('App\Rating');
    }
    public function favorites_to_users(){
        return $this->belongsToMany('App\User')->withTimestamps();
    }
    public function city(){
        return $this->belongsTo('App\City');
    }
    public function area(){
        return $this->belongsTo('App\Area');
    }
    public function orders(){
        return $this->hasMany('App\Order');
    }
    public function transactions(){
        return $this->hasMany('App\Transaction');
    }
    public function shopowneraccount(){
        return $this->hasMany('App\ShopownerAccount');
    }
    public function cashoutshop(){
        return $this->hasMany('App\CashoutShop');
    }
    public function cashoutrequestshop(){
        return $this->hasMany('App\CashoutRequestShop');
    }
    //this is tax form shop owner
    public function percentage(){
        return $this->hasOne('App\Percentagefromshop');
    }

    //this is for home page
    public function scopeIsApproved($query)
    {
        return $query->where('is_approved',1);
    }
    //this is for home page
    public function scopeIsBlocked($query)
    {
        return $query->where('is_blocked',0);
    }
    public function isOpen($id)
    {
        $shop = Shop::findOrFail($id);
        $s = $shop->open_at;
        $start = date('His',strtotime($s));
        $e = $shop->close_at;
        $end = date('His',strtotime($e));
        if ($shop->is_open == 1 and $start <=Carbon::now()->format('His') and $end >=Carbon::now()->format('His')){
            return true;
        }else
            return false;
    }
}
