<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function driver(){
        return $this->belongsTo('App\Driver');
    }

    public function payment(){
        return $this->belongsTo('App\PaymentRole');
    }
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
    public function shippingaddress(){
        return $this->belongsTo('App\Shippingaddress');
    }
    public function request_driver(){
        return $this->hasOne('App\RequestDriver');
    }
    //for driver order find

    public function scopeIsDeliverPending($query){

        return $query->where('is_delivered', 0);

    }
    public function scopeIsDelivered($query){
            return $query->where('is_delivered', 1);
    }
    //this is for customer pannel review check
    public function scopeIsReview($query){
        return $query->where('is_reviewed', 1);
    }

}
