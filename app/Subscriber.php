<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public function scopeSubscriberCount($query)
    {
        return $query->count();
    }
}
