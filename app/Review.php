<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Review extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
    public function scopeReviewCount($query)
    {
        return $query->count();
    }
    public function scopeReviewCountbyShop($query)
    {
        return $query->count();
    }
}
