<?php

namespace App\Mail;

use Composer\DependencyResolver\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RiderApprovalMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user->name;
    }

    /**
     * Build the message.
     *
     * @param Request $request
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome on board')->markdown('email.riderapprove',['user'=>$this->user]);
    }
}
