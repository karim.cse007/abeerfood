<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    public function shop(){
        return $this->belongsTo('App\Shop');
    }

    public function user(){
        $this->belongsTo('App\User');
    }

    public function scopeRatingbyUser($query, $user_id, $shop_id){
        $rat = $query->where('user_id',$user_id and 'shop_id',$shop_id)->ratings->get();
        if($rat){
            return $rat;
        }else{

            return 0;
        }
    }

    public function scopeRatingShop($query,$shopid){
        $numberOfrate = $query->where('shop_id',$shopid)->count();
        $sum = $query->where('shop_id',$shopid)->sum('rating');
        if($numberOfrate > 0){
            $rate = $sum/$numberOfrate;
            return $rate;
        }else{
            return 0;
        }

    }
}
