<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'rider','namespace'=>'Android\Driver'],function (){
    Route::post('/login','LoginController@login');
    Route::get('/register','RegisterController@register');//for driver register page
    Route::post('/register/store','RegisterController@store');//for driver register

    Route::post('/reset/request','ResetController@resetRequest');//request sent by email
    Route::post('/reset/code/match','ResetController@codeMatch');//by email and token
    Route::post('/reset/password/update','ResetController@passUpdate');//by email and password

    Route::post('/profile/personal','ProfileController@profile');//for user basic info get need to sent user id
    Route::get('/profile/personal/info/edit','ProfileController@personalEdit');
    Route::post('/profile/personal/info/update','ProfileController@personalUpdate');

    Route::post('/profile/rider/info','ProfileController@drivingInfo');
    Route::post('/profile/rider/info/edit','ProfileController@drivingEdit');
    Route::post('/profile/rider/info/update','ProfileController@drivingUpdate');
    Route::post('/profile/password/update','ProfileController@passwordUpdate');

    Route::post('/riding/history','ProfileController@ridingHistory');
    Route::post('/delivery/history','ProfileController@deliveryHistory');

    Route::post('/nearby/riders','OrderController@nearbyRiders');//for which request are place for driver

    Route::get('/requested/orders','OrderController@requestOrders');//for which request are place for driver
    Route::post('/shop/info','OrderController@shop');//for get shop info by order id
    Route::post('/shipping/info','OrderController@shippingAddress');//for get customer info by order id

    Route::post('/accept/request','OrderController@accept');//for rider accept request by order id
    Route::post('/shop/deliver/status','OrderController@shopDeliver');//for shop is delivered to rider or not by order id

    Route::post('/deliver/complete','OrderController@driverDeliverComplete');//for make deliver complete by order id and driver id

    ///cashout maintain
    Route::post('/cashout/info','AccountController@cashoutInfo');//this is for cashout page
    Route::post('/cashout/request','AccountController@cashout');//for cashout request

    Route::post('/location/status/update','ProfileController@driverStatusUpdate');//for driver location update passing cgm_token and lat ,long by driver id
    Route::post('/online/status','ProfileController@onlineStatus');//Rider is online or not check
    Route::post('/online/status/update','ProfileController@onlineStatusUpdate');//Rider online status update for offline sent false for online sent true


});
