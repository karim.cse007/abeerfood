<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Order;
use Carbon\Carbon;

Route::get('/', 'HomeController@index')->name('home');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::post('contact/info', 'ContactController@sentContact')->name('contact.sent');
Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cleared!";
});
Route::get('/access/karim007/job/start', function() {
    chdir('..');
    exec('php artisan queue:work --tries=2 &');
    return "queue is working background!";
});
Route::get('/access/karim007/live', function() {
    @unlink(storage_path().'/framework/down');
    return "Server in Live mode!";
});
Route::get('/karim007/access/maintain', function() {
    Artisan::call('down');
    return "Server in maintain mode!";
});

Route::get('/shop/{slug}','ShopController@postByShop')->name('shop.posts');
Route::post('/shop/search/area','ShopController@shopByArea')->name('shop.search');


/*map system all route*/
Route::post('location','LocationController@index')->name('getLocation');

Route::get('testing/map','LocationController@maps')->name('google.map');

//driver and shop owner registration

Route::get('rider/info', 'Driver\RegisterController@driverbasic')->name('driver.basic');
Route::get('rider/register', 'Driver\RegisterController@driveradd')->name('add.driver');
Route::post('store/rider', 'Driver\RegisterController@store')->name('store.driver');
Route::get('restaurant/info', 'Author\RegisterController@restaurantbasic')->name('restaurant.basic');
Route::get('restaurant/register', 'Author\RegisterController@restaurantadd')->name('add.restaurant');
Route::post('restaurant/store', 'Author\RegisterController@store')->name('store.restaurant');

//thanks page
Route::get('thanks/', 'HomeController@thanks')->name('registration.thanks');

Route::post('subscriber','SubscriberController@store')->name('subscriber.store');

Auth::routes();
Route::get('login/{facebook}', 'Auth\LoginController@redirectToProvider')->name('login.facebook');
Route::get('login/{facebook}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('login/{google}', 'Auth\LoginController@redirectToProvider')->name('login.google');
Route::get('login/{google}/callback', 'Auth\LoginController@handleProviderCallback');

//for shop all reviews load
Route::get('reviews/load/{id}', 'Auth\ReviewController@loadReviewPage')->name('load.reviews');

Route::group(['middleware'=>['history','auth']], function () {
    Route::post('favorite/{shop}/add','FavoriteController@add')->name('shop.favorite');
    Route::post('reviews/{post}','Auth\ReviewController@store')->name('review.store');
    Route::post('rating/shop','Auth\RatingsController@addnewrate')->name('rating.store');

    Route::post('/shipping', 'Auth\ShippingAddressController@store')->name('shipping.address.store');
    Route::post('/shipping/edit', 'Auth\ShippingAddressController@edit')->name('shipping.address.edit');

    Route::post('/charge', 'Auth\CartController@store')->name('order.store');//stripe

    Route::post('/paypal', 'Auth\CartController@paypal')->name('paypal.checkout');//paypal
    Route::get('/process', 'Auth\CartController@process')->name('paypal.process');//paypal
    Route::get('/cancel', 'Auth\CartController@cancal')->name('paypal.cancel');//paypal
});

Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth','admin','history']], function ()
{
    Route::get('dashboard','DashboardController@index')->name('dashboard');

    Route::get('settings','SettingsController@index')->name('settings');
    Route::put('profile-update','SettingsController@updateProfile')->name('profile.update');
    Route::put('password-update','SettingsController@updatePassword')->name('password.update');

    Route::get('admin-manage','AdminController@index')->name('admin.manage');
    Route::post('admin-block/{id}','AdminController@block')->name('admin.block');
    Route::post('admin-unblock/{id}','AdminController@unblock')->name('admin.unblock');

    //Location
    Route::get('location/city','LocationController@city')->name('city');
    Route::post('city/add','LocationController@addcity')->name('city.add');
    Route::put('city/update','LocationController@updateCity')->name('city.update');
    Route::delete('city/delete{id}','LocationController@destroyCity')->name('city.destroy');
    Route::get('location/state','LocationController@area')->name('areas');
    Route::post('state/add','LocationController@addarea')->name('area.add');
    Route::put('state/update','LocationController@updateArea')->name('area.update');
    Route::delete('state/delete{id}','LocationController@destroyArea')->name('area.destroy');

    Route::resource('category','CategoryController');
    Route::get('food-item','PostController@index')->name('post');

    Route::get('order-manage','OrderController@index')->name('order.driver.manage');
    Route::get('order-complete','OrderController@ordercomplete')->name('order.complete');
    Route::post('order/transaction','OrderController@transaction')->name('order.transaction');
    Route::get('order/transactions','OrderController@transactionpage')->name('page.transaction');
    //assign driver for order
    Route::post('request/driver','OrderController@requestDriver')->name('request.driver');
    Route::put('order/approve/{id}','OrderController@approveorder')->name('order.approve');


    Route::get('shops-manage','ShopController@index')->name('shop.manage');
    Route::post('shop-block/{id}','ShopController@block')->name('shop.block');
    Route::post('shop-unblock/{id}','ShopController@unblock')->name('shop.unblock');
    Route::get('shops/pending','ShopController@pendingshops')->name('pending.shops');
    Route::post('pending/shops/approve{id}','ShopController@shopapprove')->name('approve.shops');
    Route::delete('pending/shops/cancel{id}','ShopController@shopCancel')->name('cancel.shop');
    Route::get('shops/tax','ShopController@taxRate')->name('shops.tax');
    Route::post('shops/tax/update','ShopController@taxUpdate')->name('shops.tax.update');

    Route::get('driver-manage','DriverController@index')->name('driver.manage');
    Route::post('driver-block/{id}','DriverController@block')->name('driver.block');
    Route::post('driver-unblock/{id}','DriverController@unblock')->name('driver.unblock');
    Route::get('pending/drivers','DriverController@pendingdrivers')->name('pending.drivers');
    Route::put('pending/driver/approve{id}','DriverController@driverApprove')->name('approve.driver');
    Route::delete('pending/driver/cancel{id}','DriverController@driverCancel')->name('cancel.driver');

    //cash out controller
    Route::get('cashout/shop/request','CashoutController@shop')->name('cashout.request.shop');
    Route::get('cashout/driver/request','CashoutController@driver')->name('cashout.request.driver');

    Route::get('blocked/user','UserController@blockeduser')->name('user.blocked');
    Route::post('blocked/user/{id}','UserController@unblockeduser')->name('user.unblocked');


    Route::get('/subscriber','SubscriberController@index')->name('subscriber.index');
    Route::delete('/subscriber/{subscriber}','SubscriberController@destroy')->name('subscriber.destroy');

    Route::post('notification/seen','NotificationController@makeSeenOrder')->name('notification.seen');
    Route::view('notify','layouts.backend.inc.notifyadmin',[
        'orders' => Order::latest('created_at')->where('created_at','>=',Carbon::today())->get()
    ]);

});

Route::group(['as'=>'author.','prefix'=>'author','namespace'=>'Author','middleware'=>['auth','author','history']], function ()
{
    Route::get('dashboard','DashboardController@index')->name('dashboard');

    Route::get('account','AccountController@index')->name('account.info');
    Route::put('cashout/request','AccountController@cashout')->name('cashout.request');

    Route::get('settings','SettingsController@index')->name('settings');
    Route::put('profile-update','SettingsController@updateProfile')->name('profile.update');
    Route::post('password-update','SettingsController@updatePassword')->name('password.update');

    Route::get('shop','ShopController@index')->name('shopinfo');
    Route::put('shop-update','ShopController@updateShop')->name('shop.update');

    Route::resource('post','PostController');
    Route::resource('category','CategoryController');

    Route::get('order-manage','OrderController@index')->name('order.manage');
    Route::put('cooking/ready','OrderController@cookingReady')->name('cooking.ready');
    Route::put('shop/cooking/deliver','OrderController@cookingDeliver')->name('shop.cooking.deliver');
    Route::get('transactions','TransactionController@index')->name('transaction.history');

    Route::post('notification/seen','NotificationController@makeSeenOrder')->name('notification.seen');
    Route::view('notify','layouts.backend.inc.notifyshop',[
        'orders' => Order::latest('created_at')->where('created_at','>=',Carbon::today())->where('is_approved',1)->get()
    ]);

    Route::get('reviews','ReviewController@index')->name('review.index');

});

Route::group(['as'=>'driver.','prefix'=>'driver','namespace'=>'Driver','middleware'=>['auth','driver','history']], function ()
{
    Route::get('profile','DashboardController@index')->name('dashboard');
    Route::post('/profile/update','SettingsController@updateProfile')->name('update.profile');
    Route::post('/password/update','SettingsController@updatePassword')->name('update.password');

    Route::put('order/deliver','OrderController@deliver')->name('order.deliver');

    Route::put('cashout/request','AccountController@cashout')->name('cashout.request');


});

Route::group(['as'=>'customer.','prefix'=>'customer','namespace'=>'Customer','middleware'=>['auth','customer','history']], function ()
{
    Route::get('profile','DashboardController@index')->name('dashboard');
    Route::post('profile-update','SettingsController@updateProfile')->name('profile.update');
    Route::post('password-update','SettingsController@updatePassword')->name('password.update');

    Route::post('review','ReviewController@reviewDriver')->name('review.driver');

    Route::post('addtocart/{post}','CartController@addcart')->name('cart.add');
    Route::get('/cart/{cartId}','CartController@remove')->name('cart.remove');
    Route::get('show/cart','CartController@show')->name('cart.show');///for checkout page
    Route::post('distance/meter','CartController@distance')->name('distance.calculate');
    Route::post('update/cart/','CartController@update')->name('cart.update');
    Route::get('checkout/','CartController@checkout')->name('checkout');
    Route::post('destroy/cart/','CartController@destroy')->name('cart.destroy');

    Route::get('thanks','CartController@thanks')->name('checkout.thanks');

});
